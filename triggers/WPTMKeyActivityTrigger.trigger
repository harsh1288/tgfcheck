/**********************************************
Trigger Name : WPTMKeyActivityTrigger
Date         : 01 Mar 2015
Author       : Ayush Pradhan
Description : This Trigger handles the LFA user access level on Key Activity

**********************************************/

trigger WPTMKeyActivityTrigger on Key_Activity__c (before insert, before update) {

    if(Trigger.isBefore){
        if(Trigger.isInsert){
           WPTMKeyActivityHandler.restrictLFAUser(Trigger.new) ;  
        }
        if(Trigger.isUpdate){
            WPTMKeyActivityHandler.restrictLFAUser(Trigger.new) ;
        }
    }
}