trigger UpdateRegionalTotal on LFA_Work_Plan__c (After Insert,After Update,After Delete,After Undelete) {
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete || Trigger.isUndelete)){
        UpdateRegionalTotalHandler.aiuUpdateRegionalTotalForRegionalBudgets(Trigger.New,Trigger.oldMap,
                            Trigger.isInsert,Trigger.isUpdate,Trigger.isDelete,Trigger.isUndelete);
    }
}