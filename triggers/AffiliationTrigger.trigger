trigger AffiliationTrigger on npe5__Affiliation__c (before insert,before update,after Update,after insert) {
    
    /* 
Purpose: Create external user according to the access level defined in first Affiliation created
for the contact.
Developed Date: 2nd Feb 2015
Developed By: TCS(CRM)         

*/
    if(trigger.isBefore){
        AffiliationServices.updateAccessLevelForLFAAff(Trigger.new, Trigger.oldMap);
    }
    
    List<npe5__Affiliation__c> RevokeAffList = new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c> RemovesharingList= new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c> filteredAffiliations = new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c> lstOfPRCMAffiliations = new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c> lstOfLFAAffiliations = new List<npe5__Affiliation__c>();
    
    if(AffiliationServices.runTwice() <3){
        if(trigger.isAfter) {
            if(trigger.isInsert) { 
                AffiliationServices.doUserProvisioning(Trigger.new);
                
                
                Map<Id, npe5__Affiliation__c > emptyMap= new Map<Id, npe5__Affiliation__c >();
                filteredAffiliations = AffiliationServices.filterAffiliation(Trigger.new , emptyMap);
                
                if(!filteredAffiliations.isEmpty()){
                    for(npe5__Affiliation__c objAff: filteredAffiliations){
                        if(objAff.recordtypeid == label.PR_Affiliation_RT || objAff.recordtypeid== Label.CM_Affiliation_RT)
                            lstOfPRCMAffiliations.add(objAff);
                        if(objAff.recordtypeid == label.LFA_Affiliation_RT)
                            lstOfLFAAffiliations.add(objAff);
                    }    
                }
                
                if(lstOfPRCMAffiliations.size()>0)
                    AffiliationServices.createSharesBasedOnAffiliation(lstOfPRCMAffiliations , emptyMap); 
                if(lstOfLFAAffiliations.size()>0)
                    AffiliationServices.createSharesBasedOnCountryForLFA(lstOfLFAAffiliations , emptyMap); 
                //AffiliationServices.populateContactToUserMap(Trigger.new);
                
                AffiliationServices.doPSA(Trigger.new , emptyMap);
                if(userinfo.getUserType() != 'PowerPartner')
                    AffiliationServices.shareNewAffiliatedContact(Trigger.new); // 02-07-15 changes to share new Contact to manage user
                else
                    AffiliationServices.UpdateShareManageContactCheckbox(Trigger.new);
            }
            if(trigger.isUpdate) {
                AffiliationServices.doUserProvisioning(Trigger.new);
                filteredAffiliations = AffiliationServices.filterAffiliation(Trigger.new , Trigger.OldMap);
                if(!filteredAffiliations.isEmpty()){
                    for(npe5__Affiliation__c objAff: filteredAffiliations){
                        if(objAff.recordtypeid == label.PR_Affiliation_RT || objAff.recordtypeid== Label.CM_Affiliation_RT)
                            lstOfPRCMAffiliations.add(objAff);
                        if(objAff.recordtypeid == label.LFA_Affiliation_RT)
                            lstOfLFAAffiliations.add(objAff);
                    }  
                    
                }
                if(lstOfPRCMAffiliations.size()>0)
                    AffiliationServices.createSharesBasedOnAffiliation(lstOfPRCMAffiliations , Trigger.OldMap); 
                if(lstOfLFAAffiliations.size()>0)
                    AffiliationServices.createSharesBasedOnCountryForLFA(lstOfLFAAffiliations , Trigger.OldMap);
                
                AffiliationServices.doPSA(Trigger.new , Trigger.oldMap);
                
                for(npe5__Affiliation__c Aff1: trigger.new){
                    if((Aff1.Access_Level__c == 'No Access' && Aff1.Access_Level__c != trigger.oldmap.get(aff1.id).Access_Level__c) || (Aff1.npe5__Status__c == 'Former' && Aff1.npe5__Status__c != trigger.oldmap.get(aff1.id).npe5__Status__c)){
                        RevokeAffList.add(Aff1);
                    }
                }
                
                //list<npe5__Affiliation__c> RevokeList = [Select id,recordtypeid,npe5__Organization__c,recordtype.name,npe5__Status__c,npe5__Contact__c,Access_Level__c from npe5__Affiliation__c where id IN: RevokeAffList AND (Recordtypeid=:label.PR_Affiliation_RT OR Recordtypeid=:Label.CM_Affiliation_RT)];
                //list<npe5__Affiliation__c> RevokeListLFA = [Select id,recordtypeid,npe5__Organization__c,recordtype.name,npe5__Status__c,npe5__Contact__c,Access_Level__c from npe5__Affiliation__c where id IN: RevokeAffList AND Recordtypeid=:Label.LFA_Affiliation_RT];
                list<npe5__Affiliation__c> RevokeListofPRCM = new list<npe5__Affiliation__c>();
                list<npe5__Affiliation__c> RevokeListLFA = new list<npe5__Affiliation__c>();
                
                for(npe5__Affiliation__c objAff: RevokeAffList){
                
                    if(objAff.recordtypeid == Label.LFA_Affiliation_RT)
                        RevokeListLFA.add(objAff);
                    if(objAff.recordtypeid == label.PR_Affiliation_RT || objAff.recordtypeid== Label.CM_Affiliation_RT)
                        RevokeListofPRCM.add(objAff);
                    }
    
                if(RevokeListofPRCM.size()>0)
                    AffiliationServices.RemoveAccess(RevokeListofPRCM);   
                if(RevokeListLFA.size()>0)
                    AffiliationServices.RemoveLFAAccess(RevokeListLFA);
                               
                AffiliationServices.revokeLFA_PS(Trigger.new,Trigger.oldMap);
            } 
        }
    }
}