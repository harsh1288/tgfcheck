trigger PMToolKitWPTasksAfter on Work_Product_Task__c (after delete, after insert, after undelete, 
after update) {
    List<Work_Product_Task__c> triggerList = new List<Work_Product_Task__c>(); 

    if (Trigger.isDelete) {
        PMToolKitWorkProductActions.setWorkProductState(trigger.Old);
        PMToolKitWorkProductActions.sumTaskEstimatesToDos(trigger.Old);
    }
    else {
        for(Work_Product_Task__c wpt : Trigger.new)
        {
            if(!PMToolkitTriggerRunOnce.isCurrentlyRunning(wpt.Id)) {
                PMToolkitTriggerRunOnce.setIsCurrentlyRunning(wpt.Id);
                triggerList.add(wpt);
            }
        }
        if(triggerList.size() > 0) {
        PMToolKitWorkProductActions.setWorkProductState(triggerList);  
        PMToolKitWorkProductActions.sumTaskEstimatesToDos(triggerList); 
        }
    }
    
    
}