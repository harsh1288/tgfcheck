/**
* @author       Kim Roth        
* @date         06/15/2013
* @description  Trigger for workproduct
*
*    -----------------------------------------------------------------------------
*    Developer                  Date                Description
*    -----------------------------------------------------------------------------
*   
*    Kim Roth                   06/15/2013          Initial creation
*/

    trigger WorkProductTrigger on Work_Product__c (after delete, after insert, after undelete, after update, before update) {  
        List<Work_Product__c> triggerList = new List<Work_Product__c>(); 
        if(Trigger.isBefore) {
        //This is done now via Validation Rule
        //PMToolKitWorkProductActions.preventClosedDefectsChanging(Trigger.new, Trigger.old);
        } else if(Trigger.isAfter) {
            if(Trigger.new <> NULL) {
                for(Work_Product__c wp : Trigger.new) {
                    if(!PMToolkitTriggerRunOnce.isCurrentlyRunning(wp.Id)) {
                        PMToolkitTriggerRunOnce.setIsCurrentlyRunning(wp.Id);
                        triggerList.add(wp);
                    }
                }
            }
            if(Trigger.isDelete){
                PMToolKitWorkProductActions.updateIterationFromWorkProduct(Trigger.old);
                PMToolKitWorkProductActions.updateReleaseFromWorkProduct(Trigger.old);
                PMToolKitWorkProductActions.updateParentWorkProductFromChildWorkProduct(Trigger.old);
                PMToolKitWorkProductActions.updateRankFromWorkProduct(Trigger.old);
            } else if(Trigger.isUpdate && triggerList.size() > 0){ 
                    PMToolKitWorkProductActions.updateIterationFromWorkProduct(triggerList);
                    PMToolKitWorkProductActions.updateReleaseFromWorkProduct(triggerList);
                    PMToolKitWorkProductActions.updateParentWorkProductFromChildWorkProduct(triggerList);
                    PMToolKitWorkProductActions.updateRankFromWorkProductAfterUpdate(Trigger.old, triggerList);
            } else if((Trigger.isInsert && triggerList.size() > 0) || (Trigger.isUndelete  && triggerList.size() > 0)){
                    PMToolKitWorkProductActions.updateIterationFromWorkProduct(triggerList);                
                    PMToolKitWorkProductActions.updateReleaseFromWorkProduct(triggerList);
                    PMToolKitWorkProductActions.updateParentWorkProductFromChildWorkProduct(triggerList);
            }   
        }
        
    }