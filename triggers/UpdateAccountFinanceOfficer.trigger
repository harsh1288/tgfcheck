/*********************************************************************************
* Trigger: UpdateAccountFinanceOfficer
* {DateCreated 12/06/2013}
----------------------------------------------------------------------------------
* Purpose:
* - This Trigger is used to set Finance officer of Account.
----------------------------------------------------------------------------------
*********************************************************************************/
trigger UpdateAccountFinanceOfficer on Account (After Insert,After Update) {    
   // UpdateAccountFinanceOfficerHandler.aiuSetAccountFinanceOfficer(Trigger.New,Trigger.OldMap,Trigger.IsInsert,Trigger.IsUpdate);
}