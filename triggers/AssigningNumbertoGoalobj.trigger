trigger AssigningNumbertoGoalobj on Goals_Objectives__c (after delete, before insert) {
  List<id> pfids = new List<id>();
  List<Goals_Objectives__c> updategoals = new List<Goals_Objectives__c>();
  
  if(Trigger.isInsert){
     for(Goals_Objectives__c objgoal : trigger.new){
      pfids.add(objgoal.Performance_Framework__c);
     }
     List<Performance_Framework__c> lstpf = [select id from Performance_Framework__c where id =: pfids];
     List<Goals_Objectives__c> lstgoals = [select id from Goals_Objectives__c where Performance_Framework__c IN : pfids and Type__c =: 'Goal']; 
     List<Goals_Objectives__c> lstobjs = [select id from Goals_Objectives__c where Performance_Framework__c  IN : pfids and Type__c =: 'Objective']; 
     for(Performance_Framework__c objpf : lstpf){
         //List<Goals_Objectives__c> lstgoals = [select id from Goals_Objectives__c where Performance_Framework__c =:objpf.Id and Type__c =: 'Goal']; 
         integer i = lstgoals.size()+1;
         for(Goals_Objectives__c objgoal : trigger.new){
             if(objgoal.Performance_Framework__c == objpf.Id && objgoal.Type__c == 'Goal'){
             objgoal.Number__c = i;
             i++;
             }
         }
       //  List<Goals_Objectives__c> lstobjs = [select id from Goals_Objectives__c where Performance_Framework__c =:objpf.Id and Type__c =: 'Objective']; 
         integer i1 = lstobjs.size()+1;
         for(Goals_Objectives__c objgoal : trigger.new ){
             if(objgoal.Performance_Framework__c == objpf.Id && objgoal.Type__c == 'Objective'){
             objgoal.Number__c = i1;
             i1++;
             }
         }
     }
     
  }
  if(Trigger.isDelete){
     for(Goals_Objectives__c objgoal : trigger.old){
         pfids.add(objgoal.Performance_Framework__c);
     }
     List<Performance_Framework__c> lstpf = [select id from Performance_Framework__c where id =: pfids];
     List<Goals_Objectives__c> lstgoals = [select id from Goals_Objectives__c where Performance_Framework__c IN : pfids and Type__c =: 'Goal']; 
     List<Goals_Objectives__c> lstobjs = [select id from Goals_Objectives__c where Performance_Framework__c IN : pfids and Type__c =: 'Objective'];
     for(Performance_Framework__c objpf:lstpf){
         //List<Goals_Objectives__c> lstgoals = [select id from Goals_Objectives__c where Performance_Framework__c =:objpf.Id and Type__c =: 'Goal']; 
         integer i=1;
         for(Goals_Objectives__c objgoal:lstgoals){
             if(i <= lstgoals.size() ){
                objgoal.Number__c = i;
                i++;
             }
           updategoals.add(objgoal);  
         }
         //List<Goals_Objectives__c> lstobjs = [select id from Goals_Objectives__c where Performance_Framework__c =:objpf.Id and Type__c =: 'Objective']; 
         integer i1 = 1;
         for(Goals_Objectives__c objgoal:lstobjs){
             if(i1 <= lstgoals.size() ){
                objgoal.Number__c = i1;
                i1++;
             }
           updategoals.add(objgoal);  
         }
     } 
     update updategoals;
  }
  
  
 }