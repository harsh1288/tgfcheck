trigger ContactTrigger on Contact (after insert,after Update) {
    
    List<id> ConList = new List<id>();
    List<id> ExternalConList = new List<id>();
    List<user> updateUserList = new List<User>();
    List<npe5__Affiliation__c> updateAffList = new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c> AffliationList = new List<npe5__Affiliation__c>();
    List<id> GMtabRevokeConList= new List<id>();
    List<id> ConIDList = new List<id>();
    string RevokeGMTabName = 'Make_GM_app_visible_to_LFA_user_PermissionSet';
   
  /* if(trigger.isInsert){
       ContactTriggerHandler.shareNewContact(trigger.new);
   }*/
   
   if(Trigger.isUpdate){
        //FederationId Update
        for(Contact con: trigger.new){
            if(Con.Federation_ID__c != trigger.oldmap.get(con.id).Federation_ID__c){
                ConIDList.add(Con.id);
            }
        }
        
        if(!ConIDList.isEmpty())
            ContactTriggerHandler.updateFedID(ConIDList);    
       
       /* Code For Inactivating the Contact User*/
        for(Contact c: trigger.new){
             if(c.No_of_Active_Affiliation__c == 0 && c.No_of_Active_Affiliation__c != trigger.oldmap.get(c.id).No_of_Active_Affiliation__c){
                  ConList.add(c.id);   
             }
        }
        
        if(ConList.size()>0)
           FutureClassInactiveUser.updateContact(ConList);
           
        /* Code for revoking the GM tab */
        for(Contact cont: trigger.new){
            if(cont.LFA_Access_to_GM__c == 0 && cont.LFA_Access_to_GM__c!= trigger.oldmap.get(cont.id).LFA_Access_to_GM__c){
                  GMtabRevokeConList.add(cont.id);   
             }
        }  
        
        if(GMtabRevokeConList.size()>0)
            AffiliationServices.RevokeGrantMakingTab(GMtabRevokeConList,RevokeGMTabName);
        
       
       /* Update Access Level of Affiliation when  Access for external user becomes false in Contact*/
        for(Contact c: trigger.new){
             if(c.External_User__c == False && c.External_User__c != trigger.oldmap.get(c.id).External_User__c){
                  ExternalConList.add(c.id);   
             }
        }
        
        if(ExternalConList.size()>0){
            AffliationList = [Select id,Access_Level__c from npe5__Affiliation__c where npe5__Contact__c IN: ExternalConList AND Access_Level__c != 'No Access'];
        
            for(npe5__Affiliation__c Aff: AffliationList){
                aff.Access_Level__c = 'No Access';
                aff.Manage_Contacts__c = false;
                updateAffList.add(aff);
            }
            
            if(updateAffList.size()>0)
                update updateAffList;
        }
    }    
}