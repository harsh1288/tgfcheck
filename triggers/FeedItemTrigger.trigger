trigger FeedItemTrigger on FeedItem (Before Insert) { 
    for(FeedItem objFeed: Trigger.New){
        if(Trigger.IsInsert == true && objFeed.Type == 'ContentPost'){ 
            if(objFeed.ContentDescription == '' || objFeed.ContentDescription == null){                   
                objFeed.ContentDescription = objFeed.Body;
            }
        }
    }   
}