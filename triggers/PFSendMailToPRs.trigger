trigger PFSendMailToPRs on Performance_Framework__c (after update) {

/*********************************************************************************
* Trigger: SendMailToPRs
  Test class:   TestSendMailToPRs
*  DateCreated : 30/10/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To add PR Admin and PR Read Write Edit Contacts to send Email
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           28/10/2014      INITIAL DEVELOPMENT
*********************************************************************************/
/*    Set<Id> accIds = new Set<Id>();
    Set<Id> contactIds = new Set<Id>();
    Set<Id> ipIds = new Set<Id>();
    Set<Id> ipDetailIds = new Set<Id>();
    
    List<Performance_Framework__c> lstIPDetail = new List<Performance_Framework__c>(); 
    List<Account> lstAccount = new List<Account>();
    List<Implementation_Period__c> lstIP = new List<Implementation_Period__c>();
    List<String> lstEMailAdd = new List<String>(); 
    List<Id> lstConIds = new List<Id>(); 
    List<User> lstPRUsers = new list<user>();

    for(Performance_Framework__c ipDetail: Trigger.new){
      if(Trigger.isInsert){
          ipDetailIds.add(ipDetail.Id);
      }
      if(Trigger.isUpdate && (ipDetail.PF_Status__c != System.Trigger.oldMap.get(ipDetail.Id).PF_Status__c && (ipDetail.PF_Status__c == Label.IP_Return_back_to_PR ||ipDetail.PF_Status__c == Label.IP_Sub_to_GF))){
          ipDetailIds.add(ipDetail.Id);
      }
    }
    
 if(ipDetailIds!=null){
    /*Query for Implementation Period of the IP Detail Information*/
  /*  lstIPDetail = [SELECT Id,PF_Status__c, Implementation_Period__c, Implementation_Period__r.Principal_Recipient__c FROM Performance_Framework__c WHERE Id=:ipDetailIds];
    
    for(Performance_Framework__c ipDInfo: lstIPDetail){
        if(ipDInfo.Implementation_Period__c!=null)  
            ipIds.add(ipDInfo.Implementation_Period__c);
        if(ipDInfo.Implementation_Period__r.Principal_Recipient__c!=null)
            accIds.add(ipDInfo.Implementation_Period__r.Principal_Recipient__c);
    }

/*Query for all Contacts associated with the Principal Recipients*/
   /* List<Account> lstAccounts = [SELECT Id, (SELECT Id from Contacts) FROM Account WHERE Id=:accIds];
    for(Account acc:lstAccounts){
        for(Contact con: acc.Contacts){
            contactIds.add(con.Id);
            lstConIds.add(con.Id);
        }
    }

/*Query for Email of Users where Profile name is PR Admin or PR Read Write Edit*/
   /* if(contactIds.size()>0){
        lstPRUsers = [SELECT ID, Email, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID in: contactIds and Profile.Name in ('PR Read Write Edit', 'PR Admin') And isActive =: True];
    
     // Remove PR Read Only

    for(User objUser: lstPRUsers){
        if(objUser.Email!=null)
            lstEMailAdd.add(objUser.Email);
            System.debug('lstEMailAdd'+lstEMailAdd);
    }

    String[] toAddresses = new String[] {};
    for(String email: lstEMailAdd){ 
        toAddresses.add(email);
    }
 
    Messaging.SingleEmailMessage mailToPRs = new Messaging.SingleEmailMessage();
    //mailToPRs.setTargetObjectId(Label.Admin_Id);

    mailToPRs.setTargetObjectId(lstPRUsers[0].ContactId);
    mailToPRs.setToAddresses(toAddresses);
    mailToPRs.setUseSignature(false);
    mailToPRs.setBccSender(false);
    mailToPRs.setSaveAsActivity(false);
    mailToPRs.setOrgWideEmailAddressId(Label.OrgWideAddrss_GMP);
  
  
    for(Performance_Framework__c ipDet: lstIPDetail){
    if(ipDet.PF_Status__c == 'Sent back to PR') {
          mailToPRs.setWhatId(ipDet.Id);
          EmailTemplate et= [Select id from EmailTemplate where DeveloperName=:'Mail_to_PR_on_MEPH_Reject'];
          mailToPRs.setTemplateId(et.id);
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
    }
  
    if(ipDet.PF_Status__c == 'Submitted to MEPH Specialist') {
          mailToPRs.setWhatId(ipDet.Id);
          EmailTemplate et=[Select id from EmailTemplate where DeveloperName=:'Email_template_for_PR_confirmation'];
          mailToPRs.setTemplateId(et.id);
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
      }
   }
  }
 } */
}