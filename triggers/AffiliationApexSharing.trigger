/*
Purpose: Sharing of CN adn IP records according to the Affiliation. When Active affiliation checkbox is true and Access level is defined 
         then trigger will run.
         
Created Date: 15 Sep, 2014
Developed By: TCS
*/

trigger AffiliationApexSharing on npe5__Affiliation__c (after insert, after update) {
   

    
    Map<Id,Id> userConMap = new Map<Id,Id>(); 
    AccountShare externalAFFRecord = new AccountShare();
    Concept_Note__Share cnExtShare =  new Concept_Note__Share();
    List<Concept_Note__c> cnListShare = new List<Concept_Note__c>();
    Country__Share countryShare = new Country__Share();
    Indicator__Share indiShare = new Indicator__Share();
    List<AccountShare> externalAccShareCCM = new List<AccountShare>();
    
    
    List<User> lstUser = [Select ID,Profile.Name,ContactId from User where User.Profile.UserLicense.Name=:'Partner Community Login']; 
    System.debug('**lstUser '+lstUser);
    for(User objUser :lstUser ){
        userConMap.put(objUser.ContactId,objUser.id);
    }
    
    List<AccountShare> lstAffShare  = new List<AccountShare>();
    List<Concept_Note__Share> lstCNShare = new List<Concept_Note__Share>();
    List<Country__Share> lstCountryShare = new List<Country__Share>();
    List<Indicator__Share> lstIndicatorShare = new List<Indicator__Share>();
    
    Set<Id> accountIds = new Set<Id>();
    Set<Id> CNIds = new Set<Id>();
    Set<Id> CNtoDeleteCCM = new Set<Id>();
    Set<Id> CNtoDeletePR = new Set<Id>();
    Set<Id> AcctoDeletePR = new Set<Id>();
    Set<Id> AcctoDeleteCCM = new Set<Id>();
    
    // Loop to get the Affliation related Organization
    for (npe5__Affiliation__c objAff1 : Trigger.new)  
        accountIds.add(objAff1.npe5__Organization__c);
    
    /* Get the related Concet Note of Account related to Affliation*/
    
    List<Concept_Note__c> lstCN = [Select Id,Name,CCM_New__c from Concept_Note__c Where CCM_New__c in : accountIds];
    system.debug('**lstCN'+lstCN);
    Map<id, List<Concept_Note__c>> conceptMap = new Map<id,List<Concept_Note__c>>();
     
    /*Get the Account and its related Concept Note */
    for(Concept_Note__c c : lstCN){
        if(conceptMap.get(c.CCM_New__c) != null){
            List<Concept_Note__c> DocList =conceptMap.get(c.CCM_New__c);
            DocList.add(c);
            system.debug('**DocList'+DocList);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        else{
            List<Concept_Note__c> DocList = new List<Concept_Note__c>();
            DocList.add(c);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        system.debug('**conceptMap'+conceptMap);
    }
             
    /* For each of the affiliation records being inserted, do the following:*/
    
    for(npe5__Affiliation__c objAff : trigger.new){
        if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Admin' ||  objAff.Access_Level__c=='CCM Read Write' )){  
            
           /***********************Share concept Notes records related to CCM Organisation******************************************************/
              
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Concept_Note__c cn :cnListShare){
                    Concept_Note__Share cns = new Concept_Note__Share();
                    cns.ParentId = cn.ID;
                    cns.AccessLevel = 'Edit';
                    cns.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    System.debug('**UserOrGroupId of Concept NOte'+cns.UserOrGroupId);
                    System.debug('**Parent Id of the record'+cns.ParentId);
                    lstCNShare.add(cns);
                    CNtoDeleteCCM.add(cn.ID);
                }
                
                /******************* Share Implementation Period of Concept note which where shared for CCM organistaion**************/
                
                for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    AcctoDeleteCCM.add(ip.Principal_Recipient__c);
                }
            }
            countryShare.AccessLevel = 'Edit';
            indiShare.AccessLevel = 'Edit';
            //externalAFFRecord.RowCause = ;
            //lstAffShare.add(externalAFFRecord);
           
        }
        
         /************************* Share Implementation period of PR Organisation **********************/
             
        if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Admin' ||  objAff.Access_Level__c == 'PR Read Write')){
            
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Edit';
            externalAFFRecord.OpportunityAccessLevel = 'Edit';
            lstAffShare.add(externalAFFRecord);
            AcctoDeletePR.add(objAff.npe5__Organization__c);
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            
            /************* Share Concept Note for which IP is created **************/
            
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                CNtoDeletePR.add(cpr.ID);
            }
        }
        
        
        if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Read')){    
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Concept_Note__c cn :cnListShare){
                    Concept_Note__Share cns = new Concept_Note__Share();
                    cns.ParentId = cn.ID;
                    cns.AccessLevel = 'Read';
                    cns.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    System.debug('**UserOrGroupId of Concept NOte'+cns.UserOrGroupId);
                    System.debug('**Parent Id of the record'+cns.ParentId);
                    lstCNShare.add(cns);
                    //CNtoDeleteCCM.add(cn.ID);
                }
                for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    AcctoDeleteCCM.add(ip.Principal_Recipient__c);
                }
          }    
     }
     
     if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Read')){
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Read';
            externalAFFRecord.OpportunityAccessLevel = 'Read';
            lstAffShare.add(externalAFFRecord);
            //AcctoDeletePR.add(objAff.npe5__Organization__c);
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                //CNtoDeletePR.add(cpr.ID);
            }
        }
     
     /************************************** Conditions for revoking the sharing *********************/
     
    }
    
     Database.SaveResult[] accShareInsertResult = Database.insert(externalAccShareCCM,false);
     Database.SaveResult[] accShareInsertResult2 = Database.insert(lstAffShare,false);
     Database.SaveResult[] cnShareInsertResult = Database.insert(lstCNShare,false);

     System.debug(accShareInsertResult);
     System.debug(cnShareInsertResult);
     
     
    if(trigger.isUpdate){
    // Delete method
        list<id> Acclist = new List<id>();
        list<id> CNoteList = new List<id>();
        string orgType;
        List<Implementation_Period__c> IPList = new List<Implementation_Period__c>();
        List<AccountShare> AccshareToDelete = new List<AccountShare>();
        List<Concept_Note__Share> CNShareList = new List<Concept_Note__Share>();
        List<id> RelatedConlist = new List<id>();
        List<id> ConIdList = new List<id>();
        
        list<id> CCMAcclist = new List<id>();
        List<id> CCMRelatedConlist = new List<id>();
        List<Contact> UpdateConlist = new List<Contact>();
        List<Contact> ContactList = new List<Contact>();
        Account Acc = [Select id, Name from Account where Name=: 'Partner Organisation' Limit 1];
        
        for(npe5__Affiliation__c objAff : trigger.new){
            System.debug('**objAff.Is_Inactive_c__c '+objAff.Is_Inactive_c__c);
            System.debug('**objAff.Access_Level__c'+objAff.Access_Level__c);
            if(objAff.Is_Inactive_c__c == true && objAff.Is_Inactive_c__c!= trigger.oldmap.get(objAff.id).Is_Inactive_c__c && (objAff.Access_Level__c == 'PR Admin' ||  objAff.Access_Level__c == 'PR Read Write'  || objAff.Access_Level__c == 'PR Read')){
                Acclist.add(objAff.npe5__Organization__c);
                RelatedConlist.add(userConMap.get(objAff.npe5__Contact__c));
                System.debug('**inside If of Aff '+Acclist);
                ConIdList.add(objAff.npe5__Contact__c);
                 System.debug('**inside ConIdList Aff '+ConIdList);
                
            }
            if(objAff.Is_Inactive_c__c == true && objAff.Is_Inactive_c__c!= trigger.oldmap.get(objAff.id).Is_Inactive_c__c && (objAff.Access_Level__c == 'CCM Admin' ||  objAff.Access_Level__c == 'CCM Read Write'  || objAff.Access_Level__c == 'CCM Read')){
                CCMAcclist.add(objAff.npe5__Organization__c);
                CCMRelatedConlist.add(userConMap.get(objAff.npe5__Contact__c));
                System.debug('**inside If of Aff '+CCMAcclist);
            }
        }       
        
        // Deleting share for PR
        
        if(Acclist!=Null)
             IPList = [Select Id,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c IN: Acclist];
       
        for(Implementation_Period__c imp: IPList){
            CNoteList.add(imp.Concept_Note__c);
        }
        
        if(Acclist!=Null)
            AccshareToDelete = [SELECT Id  FROM AccountShare WHERE AccountID IN :Acclist AND UserOrGroupId IN:RelatedConlist];
            
        if(CNoteList!=Null)
             CNShareList = [SELECT Id FROM Concept_Note__Share WHERE ParentId IN :CNoteList AND UserOrGroupId IN:RelatedConlist];
             System.debug('**CNShareList '+CNShareList);
             
        if(!AccshareToDelete.isEmpty()){
            Database.Delete(AccshareToDelete, false);
        }
        
        if(!CNShareList.isEmpty()){
            Database.Delete(CNShareList, false);
        }
        
        // Delete share for CCM
        List<Concept_Note__Share> CcmCNShareList = new List<Concept_Note__Share>();
        List<Concept_Note__c> CcmCNList = new List<Concept_Note__c>();
        List<Implementation_Period__c> cCMIPList = new List<Implementation_Period__c>();
        List<id> DelCCMAccount = new List<id>();
        List<AccountShare> DelCCMAccountShareList = new List<AccountShare>();
        
        if(CCMAccList!=Null){
            CcmCNList = [Select id from Concept_Note__c where CCM_New__c in : CCMAcclist];
            CcmCNShareList = [SELECT Id FROM Concept_Note__Share WHERE ParentId IN :CcmCNList AND UserOrGroupId IN:CCMRelatedConlist];
            cCMIPList = [Select Id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c IN: CcmCNList];
            
            
            for(Implementation_Period__c ImpPeriod: cCMIPList){
                DelCCMAccount.add(ImpPeriod.Principal_Recipient__c);
            }
            
            if(DelCCMAccount!=Null)
                DelCCMAccountShareList = [Select id from Accountshare WHERE AccountID IN :DelCCMAccount AND UserOrGroupId IN:CCMRelatedConlist];    
        }   
        
        if(!CcmCNShareList.isEmpty()){
            Database.Delete(CcmCNShareList, false);
        }
        
        if(!DelCCMAccountShareList.isEmpty()){
            Database.Delete(DelCCMAccountShareList, false);
        } 
        
        if(ConIdList!=Null)
            ContactList = [Select id, Accountid from Contact where id IN: ConIdList AND Accountid IN: Acclist];            
             
           
         if(ContactList != NULL) { 
            for(Contact con: ContactList){
                con.Accountid= Acc.id;
                Updateconlist.add(con);
                system.debug(Updateconlist);
            }
        }  
        
        If(Updateconlist!=Null)
            Update UpdateConList;  
        
    }
       
}