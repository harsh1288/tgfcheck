//****************************************************************************************

// Purpose     :  This Trigger is used to delete records in Junction object between Milestone & Reporting Period
// Date        :  23-Mar-2015
// Created By  :  TCS 
// Author      :  Bhavya Mehta
// Description :  This Trigger is used to delete records in Junction object between Milestone & Reporting Period if correspoing Mileston record is also getting deleted with help of WPTM_MilestongRPTriggerHandler.

trigger WPTMMilestoneRPTrigger on Milestone_Target__c (before delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isDelete){
            WPTMMilestoneRPTriggerHandler.deleteMileStoneRPRecords(Trigger.oldMap);
            }
        }
}