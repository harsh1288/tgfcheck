trigger Share_with_CCM_CT on Concept_Note__c (after insert,after update) {

 List<Concept_Note__Share> cnShares  = new List<Concept_Note__Share>();
 map<String,String> mapAccountCTId = new map<String,String>();

  for(Concept_Note__c note : [Select Id, CCM_new__r.CT_ID__c from Concept_Note__c WHERE Id in: Trigger.New])
  {
    mapAccountCTId.put(note.Id,note.CCM_new__r.CT_ID__c);
  }
  

 for(Concept_Note__c cn : trigger.new){
        
        Concept_Note__Share ctShare = new Concept_Note__Share();
       
        ctShare.ParentId = cn.Id;
       
        ctShare.UserOrGroupId = mapAccountCTId.get(cn.Id); 
        System.debug(mapAccountCTId.get(cn.Id));
      
              
         ctShare.AccessLevel = 'edit';
            
        // Specify that the reason
        ctShare.RowCause = Schema.Concept_Note__Share.RowCause.Share_with_Country_Team__c;
            
        // Add the new Share record to the list of new Share records.
        cnShares.add(ctShare);
    }
    
    
     // Insert all of the newly created Share records and capture save result 
    Database.SaveResult[] cnShareInsertResult = Database.insert(cnShares,false);
        
    

}