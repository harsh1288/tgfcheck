/* PURPOSE: Update Remaining Amount in Detailed Budget whenever HPC Amount is Updated in HPC
            Framework
   Developed BY : TCS
   Created Date: 15-Nov-2014
   
*/            

trigger UpdateRemainingAmount on HPC_Framework__c (after update) {

             List<id> IpList = new List<id>();
             List<IP_Detail_Information__c> UpdateIPList = new List<IP_Detail_Information__c>();
             Map<id,IP_Detail_Information__c> IPMap = new Map<id,IP_Detail_Information__c>();
             Decimal HPCAmount = 0;
             
             // Get the record id of HPC Framework
             for(HPC_Framework__c HPF: Trigger.new){
                 iplist.add(HPF.Grant_Implementation_Period__c);
             }
             
             
             List<IP_Detail_Information__c> IP = [Select Id,name,Budget_Status__c,HPC_Amount__c,High_level_budget_GAC_1_EUR__c,High_level_budget_GAC_1_USD__c,Implementation_Period__c , Remaining_Budget_Amount__c ,Implementation_Period__r.Currency_of_Grant_Agreement__c ,Total_Approved_Budget__c ,Total_Budgeted_Amount__c from IP_Detail_Information__c where Implementation_Period__c IN: iplist];
           
             
             // Create a MAP of Implementation period ID and Detailed Budget id 
             For(IP_Detail_Information__c IPDI:IP){
                 IPMap.put(IPDI.Implementation_Period__c, IPDI);
             }
            system.debug(IPMap);
            // calculation for remaining amount in Detailed budget framework record
             for(HPC_Framework__c HPF: Trigger.new){
             
                  IP_Detail_Information__c IPD = IPMap.get(HPF.Grant_Implementation_Period__c);
                /* if(HPF.Grant_currency__c == 'USD'){
                     IPD.HPC_Amount__c = HPF.Total_HPC_Amount_USD__c;   
                 }
                 
                 if(HPF.Grant_currency__c == 'EUR'){
                     IPD.HPC_Amount__c = HPF.Total_HPC_Amount_EUR__c;  
                 }*/
                 system.debug(IPD);
                 if(IPD != null){
                  IPD.HPC_Amount__c = HPF.Total_Budgeted_Amount_in_HPC__c;  
                 
                 UpdateIPList.add(IPD);
                 }
             }
             
             // Update the Detailed Budget records
             If(UpdateIPList.size()>0 && UpdateIPList != NULL)
                 Update UpdateIPList;
}