trigger ShareGIPExt on Implementation_Period__c (after insert,after update) {
   /* if(!StopRecursion.hasalreadyRunShareGIPExt()){
    Set<Id> setCNId = new Set<Id>();
    Set<Id> setAccId = new Set<Id>();
    List<Concept_Note__c> cnListShare = new List<Concept_Note__c>();
    AccountShare externalAFFRecord = new AccountShare();
    Set<Id> CNIds = new Set<Id>();
    Map<Id,Id> userConMap = new Map<Id,Id>(); 
    List<Concept_Note__Share> lstCNShare = new List<Concept_Note__Share>();
    List<AccountShare> externalAccShareCCM = new List<AccountShare>();
    List<AccountShare> lstAffShare  = new List<AccountShare>();
    String conID1;
    
    for(Implementation_Period__c objIP : trigger.new){
        setCNId.add(objIP.Concept_Note__c);
    
    }
    
    if(setCNId!=null ){  
    
    List<Concept_Note__c> Conceptnote = [Select id,name,CCM_New__c from Concept_Note__c where Id in : setCNId];
    for(Concept_Note__c cn:Conceptnote){
        setAccId.add(cn.CCM_New__c);
    }
    }
    if(setAccId!=null){
    System.debug('**setAccId '+setAccId);
     List<User> lstUser = [Select ID,Profile.Name,ContactId from User where User.Profile.UserLicense.Name=:'Partner Community Login'];
     List<npe5__Affiliation__c> lstAff = [Select Id,Name,Is_Inactive_c__c,Access_Level__c,npe5__Organization__c,npe5__Contact__c from npe5__Affiliation__c  where npe5__Organization__c in : setAccId];
    System.debug('**lstUser '+lstUser);
    System.debug('**lstAff '+lstAff );
    for(User objUser :lstUser ){
        userConMap.put(objUser.ContactId,objUser.id);
    }
    List<Concept_Note__c> lstCN = [Select Id,Name,CCM_New__c from Concept_Note__c Where Id in : setCNId ];
    System.debug('**lstCN '+lstCN );
    Map<id, List<Concept_Note__c>> conceptMap = new Map<id,List<Concept_Note__c>>();
     
    /*Get the Account and its related Concept Note */
    /*for(Concept_Note__c c : lstCN){
        if(conceptMap.get(c.CCM_New__c) != null){
            List<Concept_Note__c> DocList =conceptMap.get(c.CCM_New__c);
            DocList.add(c);
            system.debug('**DocList'+DocList);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        else{
            List<Concept_Note__c> DocList = new List<Concept_Note__c>();
            DocList.add(c);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        system.debug('**conceptMap'+conceptMap);
    }
    
      for(npe5__Affiliation__c objAff : lstAff){
        if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Admin' ||  objAff.Access_Level__c=='CCM Read Write' || objAff.Access_Level__c=='CCM Read')){              
           /***********************Share concept Notes records related to CCM Organisation******************************************************/
              
         /*   if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                conID1 = (objAff.npe5__Contact__c);
               /* List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                
                /******************* Share Implementation Period of Concept note which where shared for CCM organistaion**************/
                
               /* for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    
                }
            }
            
           
        }
        
         /************************* Share Implementation period of PR Organisation **********************/
             
       /* if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Admin' ||  objAff.Access_Level__c == 'PR Read Write')){
            
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Edit';
            externalAFFRecord.OpportunityAccessLevel = 'Edit';
            lstAffShare.add(externalAFFRecord);
            
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            
            /************* Share Concept Note for which IP is created **************/
            
          /*  for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                
            }
        }
        
        
        if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Read')){    
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
               List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    
                }
          }    
     }
     
     if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Read')){
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Read';
            externalAFFRecord.OpportunityAccessLevel = 'Read';
            lstAffShare.add(externalAFFRecord);
            //AcctoDeletePR.add(objAff.npe5__Organization__c);
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
            }
        }
    
    }
    
     List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                
                /******************* Share Implementation Period of Concept note which where shared for CCM organistaion**************/
                
               /* for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(conID1);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    
                }
    
     Database.SaveResult[] accShareInsertResult = Database.insert(externalAccShareCCM,false);
     Database.SaveResult[] accShareInsertResult2 = Database.insert(lstAffShare,false);
     Database.SaveResult[] cnShareInsertResult = Database.insert(lstCNShare,false);

     System.debug(accShareInsertResult);
     System.debug(cnShareInsertResult);
   }
    StopRecursion.setalreadyRunShareGIPExt();
  }*/
}