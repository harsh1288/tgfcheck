public class NewImplementer_controller{

    
    public string backUrl{get;set;}
    public NewImplementer_controller(ApexPages.StandardController controller) {
         // Constructor for error page
         backUrl  = '';
         backUrl += ApexPages.currentPage().getParameters().get('goback');
         ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>');
         ApexPages.addMessage(errorMsg);         
    }
    

Public Implementer__c objImp{get;set;}
Public Implementer__c objImpInsert{get;set;}
Public String pageid{get;set;}
Public List<Implementer__c> lstImp;
Public List<Performance_Framework__c> lstInsertImp;
Public boolean returnvalue;
Public boolean errorvalue;
Public List<SelectOption> subrecipientoptions{get;set;}
public string strsubrecpient{get;set;}
public list<id> impid;
public List<Account> lstsub;
public Boolean readOnlyUser {get;set;}
    public NewImplementer_controller(ApexPages.StandardSetController controller) {
        objImp = (Implementer__c)Controller.getRecord();
        returnvalue = false;
        errorvalue = false;
        pageid =  ApexPages.currentPage().getParameters().get('id');        
        System.debug('@@@@@@'+pageid);
        // pageid is null when Save & New is clicked
        if(pageid !=null)
        {
            lstInsertImp = [SELECT id,Implementation_Period__c, 
                            Implementation_Period__r.Principal_Recipient__c, 
                            Implementation_Period__r.Principal_Recipient__r.Country__c, 
                            Implementation_Period__r.Principal_Recipient__r.Type__c, 
                            Implementation_Period__r.Principal_Recipient__r.Sub_Type__c, 
                            Implementation_Period__r.Principal_Recipient__r.Short_Name__c 
                            FROM Performance_Framework__c 
                            WHERE id =: pageid];
        
            for(Performance_Framework__c pf: lstInsertImp)
            {
                objImp.Performance_Framework__c = pf.id;
                objImp.Grant_Implementation_Period__c = pf.Implementation_Period__c;
                objImp.Principal_Recipient__c = pf.Implementation_Period__r.Principal_Recipient__c;
                objImp.Country__c = pf.Implementation_Period__r.Principal_Recipient__r.Country__c;
                
                system.debug('Implementer Details:'+objImp);
                readOnlyUser = false;
                User obj = [Select id, Name, ContactId from User where Id=:UserInfo.getUserId()];
                if( obj.ContactId != NULL ){
                  List<npe5__Affiliation__c> lstAff = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and npe5__Organization__c=:pf.Implementation_Period__r.Principal_Recipient__c and npe5__Status__c='Current' order by LastModifiedDate desc]; 
                  if( lstAff[0].Access_Level__c== 'Read'){
                       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You are not Allowed to Add Recipients.');
                       ApexPages.addMessage(myMsg); 
                       readOnlyUser = true;
                       //return null;                      
                  }               
               }
                //lstImp.add(objImp);
            }
        
            lstImp = new List<Implementer__C>();
            lstImp = [select id,Account__c from implementer__c where Performance_Framework__c =:pageid];
         
            impid = new List<Id>();
            if(lstImp.size()>0)
            {
              for(Implementer__c objimp :lstImp)
              {
                  impid.add(objimp.Account__c) ;
              }
            }
          
            recordtype rect = [select id from Recordtype where SobjectType =: 'Account' and name =: 'PR'];
        
            subrecipientoptions = new List<SelectOption>();
            lstsub = [SELECT id, name, Type__c, Sub_Type__c, Short_Name__c FROM Account WHERE Country__c =: lstInsertImp[0].Implementation_Period__r.Principal_Recipient__r.Country__c and Id != : lstInsertImp[0].Implementation_Period__r.Principal_Recipient__c and id not in:impid and RecordTypeId =:rect.id  ];
            if(lstsub.size()>0)
            {
                subrecipientoptions.add(new SelectOption('','--None--'));
                for(Account objacc : lstsub )
                {
                    subrecipientoptions.add(new SelectOption(objacc.Id,objacc.Name)); 
                }
            }
        }
    }
    // method called on vf page action attribute, checks whether Save & New is clicked and redirects to error page
    public pageReference saveNnewCheck()
    {
        if(ApexPages.currentPage().getParameters().get('save_new')=='1'){
            Pagereference errpg = Page.newImpRedirection;
            string txt ='';
            string userTypeStr = UserInfo.getUserType();
            if(userTypeStr == 'Standard'){
                txt +=   ApexPages.currentPage().getParameters().get('retURL');                 
            }
            else{
                if(ApexPages.currentPage().getParameters().get('retURL').indexOf('GM') > -1)
                {
                
                }
                else
                {
                    txt += +'/GM';
                }
                txt +=ApexPages.currentPage().getParameters().get('retURL');
            } 
                errpg.getParameters().put('goback',txt);    
                errpg.setRedirect(true);              
                return errpg;
            }
        else
        {
            return null;
        }        
    }
    // method is called when Save button is clicked and if no error found then redirects user to recipient detail page
    public pageReference SaveInfo(){
    
        for(Account ac: lstsub){
            if(objImp.Add_Implementer__c == 'Existing' &&  ac.id == strsubrecpient){
                objImp.PR_Type__c = ac.Type__c;
                objImp.Implementer_Sub_Type__c = ac.Sub_Type__c;
                objImp.Implementer_Short_Name__c = ac.Short_Name__c;                    
            }
        } 
            
        if(!checkError()){
            objImpInsert = new Implementer__c(Performance_Framework__c = objImp.Performance_Framework__c, 
                                              Grant_Implementation_Period__c = objImp.Grant_Implementation_Period__c,
                                              Principal_Recipient__c = objImp.Principal_Recipient__c, 
                                              Country__c = objImp.Country__c,
                                              Account__c = strsubrecpient,
                                              Implementer_Name__c = objImp.Implementer_Name__c, 
                                              Add_Implementer__c = objImp.Add_Implementer__c, 
                                              PR_Type__c= objImp.PR_Type__c,
                                              Implementer_Sub_Type__c = objImp.Implementer_Sub_Type__c,
                                              Implementer_Short_Name__c = objImp.Implementer_Short_Name__c);
            system.debug('insert list:'+objImpInsert.PR_Type__c);
                try{
                    system.debug('insert list@@@@@@@@:'+objImpInsert);
                    insert objImpInsert;
                    returnvalue = true;
                } catch(Exception e)
                {
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,e.getMessage());
                    ApexPages.addMessage(errorMsg);
                    returnvalue = false;
                }   
        }        
        
        if(returnvalue)
        {
            pageReference pr = new pageReference('/'+objImpInsert.id);            
            pr.setRedirect(true);
            return pr;
        }
        else
        {
            return null;
        }
    }
 // Method handles error scenario when creating a recipient      
    Public boolean checkError(){
    errorvalue = false;
        if(objImp.Add_Implementer__c == '' || objImp.Add_Implementer__c == null){
            objImp.Add_Implementer__c.addError('Please select a value');
            errorvalue = true;
        }
        if(objImp.Add_Implementer__c == 'New'){
            if(objImp.Implementer_Name__c == null || objImp.Implementer_Name__c == ''){
                objImp.Implementer_Name__c.addError('Please fill the Recipient Name');
                errorvalue = true;
            }
        }
        if(objImp.Add_Implementer__c == 'Existing'){
           if(strsubrecpient == '' || strsubrecpient == Null){
              objImp.Account__c.addError('Please select Existing Recipient');
              errorvalue = true;
           }
         }
        /*
        if(objImp.Add_Implementer__c == 'Existing'){
            if(objImp.Account__c == null || objImp.Account__c == ''){
                objImp.Account__c.addError('Please select Sub recipient');
                errorvalue = true;
            }
        }
        */
        return errorvalue;
    }
}