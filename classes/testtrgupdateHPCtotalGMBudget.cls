@IsTest
public with sharing class testtrgupdateHPCtotalGMBudget {
   public static testMethod void testtrgupdateHPCtotalGMBudget(){
       Account objAcc = TestClassHelper.insertAccount();
      
       Grant__c objGrant = TestClassHelper.createGrant(objAcc);
       insert objGrant;
       
       Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
       objIP.Status__c = 'Grant-Making';
       insert objIP;
       
       IP_Detail_Information__c objipdetail = TestClassHelper.createIPDetail();
       objipdetail.Implementation_Period__c = objIP.Id;
       objipdetail.Budget_Status__c = 'Not yet submitted';
       insert objipdetail;
       
       HPC_Framework__c objHPC = TestClassHelper.createHPC();
       objHPC.Grant_Implementation_Period__c = objIP.Id;
       objHPC.Budget_Framework_Status__c = 'Not yet submitted';
       insert objHPC;
       
       objipdetail.Budget_Status__c = 'Accepted';
       objipdetail.IS_FO_Updated_BL__c = true;
       update objipdetail;
       
       objHPC.Budget_Framework_Status__c = 'Accepted';
       update objHPC;
       
       Product__c objproduct = new Product__c();
       objproduct.Unit_Cost_Y1__c  = 25 ;
       objproduct.Quantity_Q1__c  = 2 ;
       objproduct.Health_Products_and_Costs__c = objHPC.Id;
       insert objproduct;
       
       
       
   }
}