public class AssigningNumbertoGoalhandler {
    public static void AssigningNumberinsert(List<Goals_Objectives__c> lstSobject){
        List<string> pfids = new List<string>();
        List<Goals_Objectives__c> updategoals = new List<Goals_Objectives__c>();
         for(Goals_Objectives__c objGobj: lstSobject){
             pfids.add(objGobj.Performance_Framework__c);
         }
         List<Performance_Framework__c> lstpf = [select id, implementation_Period__c from Performance_Framework__c where id =: pfids];
         List<Goals_Objectives__c> lstgoals = [select id from Goals_Objectives__c where Performance_Framework__c IN : pfids and Type__c =: 'Goal']; 
         List<Goals_Objectives__c> lstobjs = [select id from Goals_Objectives__c where Performance_Framework__c  IN : pfids and Type__c =: 'Objective']; 
         for(Performance_Framework__c objpf : lstpf){
             integer i = lstgoals.size()+1;
             for(Goals_Objectives__c objgoal : lstSobject){
                 if(objgoal.Performance_Framework__c == objpf.Id && objgoal.Type__c == 'Goal'){
                    objgoal.implementation_Period__c = objpf.implementation_Period__c;
                    objgoal.Number__c = i;
                    i++;
                 }
              }
             
              integer i1 = lstobjs.size()+1;
             for(Goals_Objectives__c objgoal : lstSobject){
                 if(objgoal.Performance_Framework__c == objpf.Id && objgoal.Type__c == 'Objective'){
                    objgoal.implementation_Period__c = objpf.implementation_Period__c;
                    objgoal.Number__c = i1;
                    i1++;
                 }
             }
          }
     }
     
     public static void AssigningNumberdelete(List<Goals_Objectives__c> lstSobject){
        List<string> pfids = new List<string>();
        List<Goals_Objectives__c> updategoals = new List<Goals_Objectives__c>();
         for(Goals_Objectives__c objGobj: lstSobject){
             pfids.add(objGobj.Performance_Framework__c);
         }
         List<Performance_Framework__c> lstpf = [select id from Performance_Framework__c where id =: pfids];
         List<Goals_Objectives__c> lstgoals = [select id from Goals_Objectives__c where Performance_Framework__c IN : pfids and Type__c =: 'Goal']; 
         List<Goals_Objectives__c> lstobjs = [select id from Goals_Objectives__c where Performance_Framework__c  IN : pfids and Type__c =: 'Objective']; 
         for(Performance_Framework__c objpf:lstpf){
         integer i=1;
         for(Goals_Objectives__c objgoal:lstgoals){
             if(i <= lstgoals.size() ){
                objgoal.Number__c = i;
                i++;
             }
           updategoals.add(objgoal);  
         }
         integer i1 = 1;
         for(Goals_Objectives__c objgoal:lstobjs){
             if(i1 <= lstgoals.size() ){
                objgoal.Number__c = i1;
                i1++;
             }
           updategoals.add(objgoal);  
         }
       } 
       if(updategoals.size()>0)
       update updategoals;
     }
     
         
     public static void checkIfIndicatorLinked(List<Goals_Objectives__c> goalsList){
         List<Ind_Goal_Jxn__c> indicatorList = new List<Ind_Goal_Jxn__c>();
         //indicatorList = [Select Id, Goal_Objective__c from Grant_Indicator__c where Goal_Objective__c IN: goalsList];
         indicatorList = [Select id, goal_Objective__c,Indicator__c from Ind_Goal_Jxn__c where Goal_Objective__c in: goalsList];
         if( indicatorList.size() > 0){
              if( goalsList[0].Type__c == 'Goal' ){
                 goalsList[0].addError('You can not delete this goal. There are indicators associated with this goal.');
              }else if( goalsList[0].Type__c == 'Objective' ){
                 goalsList[0].addError('You can not delete this objective. There are indicators associated with this objective.');         
              }
         }
     }
}