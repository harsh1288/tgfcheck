public class IterationTasksControllerExtension {

    //public Iteration__c theIteration {get; set;}
    public List<Work_Product_Task__c> wpTaskList {get; set;} 
    public List<Work_Product__c> wpList {get; set;} 
    public IterationTasksControllerExtension(ApexPages.StandardController stdController) {

    String theIterationID = ApexPages.currentPage().getParameters().get('id');

        wpList = [select id, name, record_type_name__c, task_to_do__c, task_estimate__c, work_product_id__c, plan_estimate__c, iteration__c, state__c, blocked__c, iteration__r.Name, ss_release__r.Name, rank__c, recordtypeid, is_defect__c,
                          (
                            select id, name, to_do__c, state__c, estimate__c, blocked__c, rank__c from Work_Product_Task__r
                          )
                          from Work_Product__c 
                          where Iteration__c = :theIterationID
                          order by rank__c asc];    
    }
    public PageReference saveEdits()
      {
    update wpList;
    return ApexPages.CurrentPage();
      }
}