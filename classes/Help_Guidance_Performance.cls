public with sharing class Help_Guidance_Performance{
    public String strLanguage {get; set;}
    public Guidance__c guidance {get; set;}
    
    public Help_Guidance_Performance(){
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        Performance_Framework();
    }
    
    public void Performance_Framework(){
        guidance = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'Performance Framework Detail'];
        
    }

}