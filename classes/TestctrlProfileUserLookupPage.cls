@isTest
Public class TestctrlProfileUserLookupPage{
    Public static testMethod Void TestctrlProfileUserLookupPage(){
    
     User objUser1 = [Select ID,name,Profile.Name from User where id=:userinfo.getUserId()];
     
     System.currentPageReference().getparameters().put('strUserName',objUser1.Profile.Name);
     System.currentPageReference().getParameters().put('ProfileName','PSM');
    ctrlProfileUserLookupPage objctrlProfileUserLookupPage = new ctrlProfileUserLookupPage();
    objctrlProfileUserLookupPage.searchString = objUser1.Name;
    
    objctrlProfileUserLookupPage.searchString = 'PSM';
    objctrlProfileUserLookupPage.getFormTag();
    objctrlProfileUserLookupPage.getTextBox();
    objctrlProfileUserLookupPage.search();
    }
    
    Public static testMethod Void TestctrlProfileUserLookupPage1(){
    
    User objUser1 = [Select ID,name,Profile.Name from User where id=:userinfo.getUserId()];
     
    System.currentPageReference().getparameters().put('strUserName',objUser1.Profile.Name);
    System.currentPageReference().getParameters().put('ProfileName','Finance');
    ctrlProfileUserLookupPage objctrlProfileUserLookupPage = new ctrlProfileUserLookupPage();
    objctrlProfileUserLookupPage.searchString = objUser1.Name;
    
    objctrlProfileUserLookupPage.searchString = 'Finance';
    objctrlProfileUserLookupPage.getFormTag();
    objctrlProfileUserLookupPage.getTextBox();
    objctrlProfileUserLookupPage.search();
    }
    
    Public static testMethod Void TestctrlProfileUserLookupPage2(){
    
    User objUser1 = [Select ID,name,Profile.Name from User where id=:userinfo.getUserId()];
     
    System.currentPageReference().getparameters().put('strUserName',objUser1.Profile.Name);
    System.currentPageReference().getParameters().put('ProfileName','M&E');
    ctrlProfileUserLookupPage objctrlProfileUserLookupPage = new ctrlProfileUserLookupPage();
    objctrlProfileUserLookupPage.searchString = objUser1.Name;
    
    objctrlProfileUserLookupPage.searchString = 'M&E';
    objctrlProfileUserLookupPage.getFormTag();
    objctrlProfileUserLookupPage.getTextBox();
    objctrlProfileUserLookupPage.search();
    }
}