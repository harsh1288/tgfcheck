/*********************************************************************************
* {Test} Class: {saveAndNewHpcControllerTest}
* Created by {Rahul Kumar},  {DateCreated 19-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of saveAndNewHpcController Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    24-Nov-2014
*********************************************************************************/
@isTest
public class saveAndNewHpcControllerTest {
            static saveAndNewHpcController ext;
            static saveAndNewHpcController extW;
            static HPC_Framework__c masterObject;
            static HPC_Framework__c masterObjectWithoutGI;
            static PageReference pref;
            static Grant__c grant;
            static User testUser = TestClassHelper.createExtUser();    
    Public static testMethod Void TestSaveAndNew(){
testUser.username = 'savehpc1@example.com';
     insert testUser;
            Test.startTest();
            grant = TestClassHelper.insertGrant(TestClassHelper.insertAccount());
            Implementation_Period__c ip = TestClassHelper.createIPWithConceptNote(grant,TestClassHelper.insertAccount());
            insert ip;
            masterObject = TestClassHelper.createHPC();
            masterObject.Grant_Implementation_Period__c = ip.id;
            masterObjectWithoutGI = TestClassHelper.createHPC();
            pref = Page.newHPCRedirection;
            Test.setCurrentPage(pref);
            ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
            ext = new saveAndNewHpcController(con);
            ext.createNewHpcUrl();
            ApexPages.StandardController conW = new ApexPages.StandardController(masterObjectWithoutGI);
            extW = new saveAndNewHpcController(conW);
            extW.createNewHpcUrl();
            System.runAs(testUser) {                                    
                ext.createNewHpcUrl();            
            }
            pref.getParameters().put('retUrl','GM%2F');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
                extW.createNewHpcUrl();            
            }
            pref.getParameters().put('retUrl','?');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
            extW.createNewHpcUrl();            
        }
            Test.stopTest();
    }
}