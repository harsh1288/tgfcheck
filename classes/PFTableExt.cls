public with sharing class PFTableExt{

    public transient List<TableWrapper> tbldata{get;set;}
    public string strPFId{get;set;}     
    public PFTableExt(ApexPages.StandardController controller) {   
    //getting Implementation Period Id
    strPFId = Apexpages.currentpage().getparameters().get('id');   
    Map<Id,List<Grant_Intervention__c>> moduleInterventionMap = new  Map<Id,List<Grant_Intervention__c>>();
    Map<Id,List<Key_Activity__c>> InterventionActivityMap = new Map<Id,List<Key_Activity__c>>();
    Map<Id,List<Milestone_Target__c>> activityMilestoneMap = new Map<Id,List<Milestone_Target__c>>();
    Map<Id,List<Period__c>> milestoneRPDateMap = new Map<Id,List<Period__c>>();
    Set<Id> kaIds = new Set<Id>();
    Set<Id> mtIds = new Set<Id>();
    tbldata = new List<TableWrapper>();
    for(Module__c m:[Select Id,Name,(Select Id,Name from Module__c.Grant_Interventions__r) from Module__c where Performance_Framework__c =:strPFId])
    {
        moduleInterventionMap.put(m.Id,m.Grant_Interventions__r);
    }    
    for(Grant_Intervention__c gi:[Select Id,Name,(Select Id,Name/*Activity_Title__c*/ from Grant_Intervention__c.Key_Activities__r) from Grant_Intervention__c where Performance_Framework__c =:strPFId])
    {
        InterventionActivityMap.put(gi.id,gi.Key_Activities__r);
    }    
    for(Key_Activity__c ka:[Select Id from Key_Activity__c where Grant_Intervention__c IN:InterventionActivityMap.keyset()])
    {
        kaIds.add(ka.Id);
    }
    if(kaIds.size()>0)
    {
        for(Key_Activity__c  ka:[Select Id,Name/*Activity_Title__c*/,(Select Id,MilestoneTitle__c from Key_Activity__c.Milestones_Targets__r) from Key_Activity__c where Id IN:kaIds])
        {
            activityMilestoneMap.put(ka.id,ka.Milestones_Targets__r);
        }
    }
    for(Milestone_Target__c mt:[Select Id from Milestone_Target__c where Key_Activity__c IN:activityMilestoneMap.keyset()])
    {
        mtIds.add(mt.Id);
    }
    if(mtIds.size()>0)
    {
       List<Milestone_RP_Junction__c> mrpJunctionRec = new List<Milestone_RP_Junction__c>
                                                     ([Select Id,Milestone_Target__c,Reporting_Period__c 
                                                       from Milestone_RP_Junction__c where Milestone_Target__c IN:mtIds]);                       
       if(mrpJunctionRec.size()>0)
       {   
           Set<Id> rpIds = new Set<Id>();
           for(Milestone_RP_Junction__c mrpJnc:mrpJunctionRec)
           {
               rpIds.add(mrpJnc.Reporting_Period__c);
           }
           if(rpIds.size()>0)
           {
               Map<Id,Period__c> rpMap = new Map<Id,Period__c>([Select Id,Start_Date__c,End_Date__c from Period__c where Id IN:rpIds]);                      
               for(Milestone_RP_Junction__c mrpJnc:mrpJunctionRec)
               {
                   if(milestoneRPDateMap.containsKey(mrpJnc.Milestone_Target__c))
                   {
                       milestoneRPDateMap.get(mrpJnc.Milestone_Target__c).add(rpMap.get(mrpJnc.Reporting_Period__c));
                   }
                   else
                   {
                       milestoneRPDateMap.put(mrpJnc.Milestone_Target__c,new List<Period__c>{rpMap.get(mrpJnc.Reporting_Period__c)});
                   }
               }
           }
       }
    }
    // Create wrapper list to be shown in table
    for(Module__c mod:[Select Id,Name from Module__c where Performance_Framework__c =:strPFId])
    {
        TableWrapper tempwrap = new TableWrapper();
        tempwrap.ModuleName = mod.Name;
        if(moduleInterventionMap.get(mod.Id).size()>0)
        {
            tempwrap.InterventionList = new List<Grant_Intervention__c>(moduleInterventionMap.get(mod.Id));
            List<List<Key_Activity__c>> tempActivitiesList = new List<List<Key_Activity__c>>();
            List<List<List<Milestone_Target__c>>>  tempMilestoneList = new List<List<List<Milestone_Target__c>>>();
            List<List<List<List<Period__c>>>> tempRPList = new List<List<List<List<Period__c>>>>(); 
            for(Grant_Intervention__c gi:tempwrap.InterventionList)
            {                             
                if(InterventionActivityMap.get(gi.Id).size()>0)
                {    
                    List<List<Milestone_Target__c>> mtList2 = new List<List<Milestone_Target__c>>();                
                    List<Key_Activity__c> Intka = new List<Key_Activity__c>(InterventionActivityMap.get(gi.Id));
                    List<List<List<Period__c>>> rplist2 = new List<List<List<Period__c>>>();
                    tempActivitiesList.add(Intka);
                    for(Key_Activity__c kact:Intka)
                    {                                                
                        if(activityMilestoneMap.get(kact.Id).size()>0)
                        {
                            List<List<Period__c>> rplist = new List<List<Period__c>>();
                            List<Milestone_Target__c> mtList = new List<Milestone_Target__c>(activityMilestoneMap.get(kact.Id));
                            mtList2.add(mtList);                            
                            for(Milestone_Target__c mt:mtList)
                            {                                
                                if(milestoneRPDateMap.get(mt.Id).size()>0)
                                {                                  
                                  List<Period__c> rps = new List<Period__c>(milestoneRPDateMap.get(mt.Id));
                                  rplist.add(rps);
                                }
                                else
                                {
                                  rplist.add(new List<Period__c>{new Period__c(Start_Date__c=null)});
                                }                                
                            }                            
                            rplist2.add(rplist);                            
                        }
                        else
                        {
                            mtList2.add(new List<Milestone_Target__c>{(new Milestone_Target__c(MilestoneTitle__c='-'))});
                            rplist2.add(new List<List<Period__c>>{new List<Period__c>{new Period__c(Start_Date__c=null)}});
                        }
                    }
                    tempMilestoneList.add(mtList2);
                    tempRPList.add(rplist2);
                }
                else
                {
                    //tempActivitiesList.add(new List<Key_Activity__c>{new Key_Activity__c(Name/*Activity_Title__c*/='-')});
                    tempMilestoneList.add(new List<List<Milestone_Target__c>>{new List<Milestone_Target__c>{new Milestone_Target__c(MilestoneTitle__c='-')}});
                    tempRPList.add(new List<List<List<Period__c>>>{new List<List<Period__c>>{new List<Period__c>{new Period__c(Start_Date__c=null)}}});
                }
            }
            tempwrap.ActivitiesList = new List<List<Key_Activity__c>>(tempActivitiesList);
            tempwrap.MilestoneList = new List<List<List<Milestone_Target__c>>>(tempMilestoneList);
            tempwrap.RPList = new List<List<List<List<Period__c>>>>(tempRPList);
        } 
        else
        {
            tempwrap.InterventionList= new List<Grant_Intervention__c>{new Grant_Intervention__c(Name='-')};
            //tempwrap.ActivitiesList= new List<List<Key_Activity__c>>{new List<Key_Activity__c>{new Key_Activity__c(Name/*Activity_Title__c*/='-')}};
            tempwrap.MilestoneList= new List<List<List<Milestone_Target__c>>>{new List<List<Milestone_Target__c>>{new List<Milestone_Target__c>{new Milestone_Target__c(MilestoneTitle__c='-')}}};
            tempwrap.RPList= new List<List<List<List<Period__c>>>>{new List<List<List<Period__c>>>{new List<List<Period__c>>{new List<Period__c>{new Period__c(Start_Date__c=null)}}}};
        }       
        tbldata.add(tempwrap);
    }
}         
    public Class TableWrapper{
        Public String ModuleName {get;set;}
        Public List<Grant_Intervention__c> InterventionList {get;set;}
        Public List<List<Key_Activity__c>> ActivitiesList {get;set;}
        Public List<List<List<Milestone_Target__c>>> MilestoneList {get;set;}
        Public List<List<List<List<Period__c>>>> RPList {get;set;}
    }
}