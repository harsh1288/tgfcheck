public class goalImpactReportController{

public List<goalIndicatorRelation> lstGoalIndi {get;set;}
public String pfId;
private Set<Id> setGIId;
private Set<ID> setGoalId = new Set<ID>();
private List<Ind_Goal_Jxn__c> lstJunction;
private List<Grant_Disaggregated__c> lstGrantDiss;
    public goalImpactReportController(ApexPages.StandardController controller) {
        pfId = ApexPages.currentPage().getParameters().get('sid');
         lstGoalIndi =  new List<goalIndicatorRelation>();
         setGIId = new Set<Id>();
     List<Goals_Objectives__c> lstGoals = [Select id,name from Goals_Objectives__c where Performance_Framework__c =: pfid and Type__c = 'Goal'];
     
         /*for(Goals_Objectives__c objGoal : lstGoals){
             setGoalId.add(objGoal.Id);
         }*/
     
     lstJunction = [Select Goal_Objective__c,Indicator__c,Goal_Objective__r.Name,Goal_Objective__r.Goal__c,
                           Indicator__r.Id,Indicator__r.Is_Disaggregated__c,Indicator__r.Name,Indicator__r.Indicator_Full_Name__c
                             FROM Ind_Goal_Jxn__c where Goal_Objective__r.Performance_Framework__c =:pfid and Goal_Objective__r.Type__c = 'Goal'];
     
     for(Ind_Goal_Jxn__c objJxn : lstJunction){
         setGIId.add(objJxn.Indicator__c);
     }
     
      lstGrantDiss = [Select Id,Name,Grant_Indicator__c,Catalog_Disaggregated__r.Disaggregation_Category__c,Catalog_Disaggregated__r.Disaggregation_Value__c
                        from Grant_Disaggregated__c where  Grant_Indicator__c IN : setGIId ];  
       system.debug('**lstGrantDiss '+lstGrantDiss );  
         for(Ind_Goal_Jxn__c objJxn : lstJunction){
             goalIndicatorRelation objgoalIndicatorRelation = new goalIndicatorRelation();
                 if(setGoalId.contains(objJxn.Goal_Objective__c)){
                     objgoalIndicatorRelation.blnHideDup = false;
                  }
             system.debug('**'+objJxn.Goal_Objective__r);
             system.debug('**'+objJxn.Indicator__r);
             objgoalIndicatorRelation.objGoal = objJxn.Goal_Objective__r;
             objgoalIndicatorRelation.objGrantIndicator = objJxn.Indicator__r;
             
             if(objJxn.Indicator__r.Is_Disaggregated__c ==  true){
                 for(Grant_Disaggregated__c objDiss : lstGrantDiss){
                     if(objDiss.Grant_Indicator__c == objJxn.Indicator__r.Id)
                         objgoalIndicatorRelation.lstGrantD.add(objDiss) ;
                 }
             }
             setGoalId.add(objJxn.Goal_Objective__c);
             lstGoalIndi.add(objgoalIndicatorRelation);
         }
        System.debug('**lstGoalIndi Size'+lstGoalIndi.size());
        System.debug('**lstGoalRecs'+lstGoalIndi);
    }




public class goalIndicatorRelation{
        public Goals_Objectives__c objGoal {get;set;}
        public Grant_Indicator__c objGrantIndicator {get;set;}
        public List<Grant_Disaggregated__c> lstGrantD {get;set;}
        public Boolean blnHideDup {get;set;}
        public goalIndicatorRelation (){
            objGoal = new Goals_Objectives__c ();
            objGrantIndicator = new Grant_Indicator__c();
            lstGrantD = new List<Grant_Disaggregated__c>();
            blnHideDup = true;
        }
     } 
}