public with sharing class NewModuleExt_Mobile {

    public NewModuleExt_Mobile(ApexPages.StandardController controller) {

    }


    public List<Module__c> lstmod = new List<Module__c>();
    public List<Catalog_Module__c> lstcatmod = new List<Catalog_Module__c>();
    public id cnid;
    public List<Concept_Note__c> lstcn = new List<Concept_Note__c>();
    public List<id> lstmodid = new List<id>();
    public List<wrapcatmod> lstwrapcatmod{get;set;}
    public string message{get;set;}
    public List<Catalog_Module__c> selectedcatmod = new List<Catalog_Module__c>();
    public List<Module__c> newlstmod = new List<Module__c>();    
    public List<RecordType> lstrec = new List<RecordType>();
    public List<RecordType> lstrecip = new List<RecordType>();
    public List<Implementation_Period__c> lstimp = new List<Implementation_Period__c>();
    public boolean display{get;set;}
    public boolean display1{get;set;}
    
    public NewModuleExt_Mobile(ApexPages.StandardSetController controller) {
            cnid=Apexpages.currentpage().getparameters().get('id');
            lstcn  = [Select id,Component__c,Name from Concept_Note__c where id =:cnid];
            system.debug('@@@@'+lstcn);
            if(lstcn.size()>0){
            lstmod = [select id,Name,Catalog_Module__c from MOdule__c where Concept_Note__c =:cnid ];
                  for(Module__c objmod:lstmod){
                       lstmodid.add(objmod.Catalog_Module__c);
                  }
            lstcatmod = [select Id,Name,Component__c, component_multi__c,French_Name__c,Spanish_Name__c,Russian_Name__c from Catalog_Module__c where component_multi__c INCLUDES (:lstcn[0].Component__c)  and 
                                        Id not in :lstmodid ];
            } 
            else if(lstcn.size() == 0){
                
               lstimp = [Select id,Component__c,Name from Implementation_Period__c where id =:cnid];
               
               lstmod = [select id,Name,Catalog_Module__c from MOdule__c where Implementation_Period__c =:cnid ];
               if(lstmod.size()>0){
                  for(Module__c objmod:lstmod){
                       lstmodid.add(objmod.Catalog_Module__c);
                  }
               }
               
               lstcatmod = [select Id,Name,Component__c, component_multi__c,French_Name__c,Spanish_Name__c,Russian_Name__c from Catalog_Module__c where component_multi__c INCLUDES (:lstimp[0].Component__c)  and 
                                        Id not in :lstmodid ];
               system.debug('@@@@'+lstimp);     
            }
            system.debug('@@@@'+lstmod);
            
            
             system.debug('@@@@'+lstcatmod);   
                                        
            
            lstwrapcatmod = new List<wrapcatmod>();
            if(lstcatmod.size()>0){
                display=false;
                display1 = true;
               for(Catalog_Module__c objcatmod:lstcatmod){
                   lstwrapcatmod.add(new wrapcatmod(objcatmod));  
               }
            }
            else{
                display= true;
                display1 = false;
              message ='No Modules Presented';
            }
             system.debug('@@@@'+lstwrapcatmod);
           
            lstrec = [Select Id,SobjectType,Name From RecordType where Name = 'Concept Note' and SobjectType = 'Module__c'  limit 1];
           
             system.debug('@@@@'+lstrec);
            
            lstrecip=[Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Module__c'  limit 1];
    
             system.debug('@@@@'+lstrecip);
    }
    
     
          /*public NewModuleExt(ApexPages.StandardController controller) {
            cnid=Apexpages.currentpage().getparameters().get('id');
            lstcn  = [Select id,Component__c,Name from Concept_Note__c where id =:cnid];
            
            lstmod = [select id,Name,Catalog_Module__c from MOdule__c where Concept_Note__c =:cnid ];
                  for(Module__c objmod:lstmod){
                       lstmodid.add(objmod.Catalog_Module__c);
                  }
            
            lstcatmod = [select Id,Name,Component__c, component_multi__c,French_Name__c,Spanish_Name__c,Russian_Name__c from Catalog_Module__c where component_multi__c INCLUDES (:lstcn[0].Component__c) and 
                                        Id not in :lstmodid ];
            
            lstwrapcatmod = new List<wrapcatmod>();
            if(lstcatmod.size()>0){
               for(Catalog_Module__c objcatmod:lstcatmod){
                   lstwrapcatmod.add(new wrapcatmod(objcatmod));  
               }
            }
            else{
              message ='No Modules Presented';
            }
            
          }*/
          
          public pageReference save(){
              for(wrapcatmod objwrap : lstwrapcatmod){
                if(objwrap.selected == true){
                    selectedcatmod.add(objwrap.wrapcatmod);
                }
              }
               if(selectedcatmod.size()==0){
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one Module');
                 ApexPages.addMessage(myMsg); 
                 return null;
               }
         else{
              if(lstcn.size() > 0){
              for(Catalog_Module__c objcatmod:selectedcatmod){
                 Module__c objmod= new Module__c();
                    objmod.Catalog_Module__c = objcatmod.id;
                    objmod.Component__c = lstcn[0].Component__c;
                    objmod.Name = objcatmod.name;
                    objmod.Russian_Name__c = objcatmod.Russian_Name__c;
                    objmod.French_Name__c = objcatmod.French_Name__c;
                    objmod.Spanish_Name__c =objcatmod.Spanish_Name__c;
                    objmod.Concept_Note__c = cnid;
                    objmod.RecordTypeId = lstrec[0].Id;
                    newlstmod.add(objmod);
              }
              insert newlstmod;
              }
              else if(lstimp.size()>0){
                   for(Catalog_Module__c objcatmod:selectedcatmod){
                    Module__c objmod= new Module__c();
                    objmod.Catalog_Module__c = objcatmod.id;
                    objmod.Component__c = lstimp[0].Component__c;
                    objmod.Name = objcatmod.name;
                    objmod.Russian_Name__c = objcatmod.Russian_Name__c;
                    objmod.French_Name__c = objcatmod.French_Name__c;
                    objmod.Spanish_Name__c =objcatmod.Spanish_Name__c;
                    objmod.Implementation_Period__c = cnid;
                    objmod.RecordTypeId = lstrecip[0].Id;
                    newlstmod.add(objmod);
              }
              insert newlstmod;
              }
              pageReference pr = new pageReference('/'+newlstmod[0].id);
              pr.setRedirect(true);
              return pr;
           }
              
              
             
          }
          
          public class wrapcatmod{
            public Catalog_Module__c wrapcatmod{get;set;}
            public boolean selected{get;set;}
                public wrapcatmod(Catalog_Module__c objcatmod){
                       wrapcatmod = objcatmod;
                       selected  = false;
                }
            
          }
}