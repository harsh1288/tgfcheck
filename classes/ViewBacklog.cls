/**
* @author       Kim Roth        
* @date         06/11/2013
* @description  This Class is the controller extension class for Visualforce Page "ViewBacklog".
*
*   -----------------------------------------------------------------------------
*   Developer               Date                Description
*   -----------------------------------------------------------------------------
*   
*   Kim Roth                06/11/2013          Initial version                                             
*/  

global with sharing class ViewBacklog {

    public List<Work_Product__c> releaseBacklogList {get;set;}
    public List<Work_Product__c> iterationBacklogList {get;set;}
    public List<Work_Product__c> productBacklogList {get;set;}
    public List<Iteration__c> iterationList {get;set;}
    public List<SS_Release__c> releaseList {get;set;}
    public List<Project_Overview__c> projectList {get;set;}
    public string selectedProjectId {get;set;}
    public Id selectedWorkstreamId {get;set;}
    public Id selectedbprocessId {get;set;}
    public Id selectedActivityId {get;set;}
    public boolean isHidePastIterations {get; set;}
    public boolean isHidePastReleases {get; set;}
    public boolean isHideClosedProjects {get; set;}
    public Map<String, planTabColorScheme__c> colorScheme {get;set;}
    private List<ss_release__c> completeReleaseList;
    private List<iteration__c> completeIterationList;
    private List<work_product__c> completeWPList;
    private List<work_product__c> completeWPIList;
    private List<work_product__c> completeWPRList;
    private RecordType parentUserStoryRecordType;
    public transient String QueryText { get; set; } // For debugging
    public transient String QueryTextiteration { get; set; } // For debugging
    public transient String QueryTextrelease { get; set; } // For debugging
    public String flag;
    public String selectionFilter {
        get {
            if (selectionFilter == null)
                selectionFilter ='All';
            return selectionFilter;
                
        }
        set;
    }
    public List<SelectOption> filterByOptions {
        get {
            SelectOption allOption = new SelectOption('All','All'); 
            SelectOption storyOption = new SelectOption('User Stories','User Stories');
            SelectOption defectOption = new SelectOption('Defects','Defects');                                   
            return new System.SelectOption[]{storyOption, defectOption, allOption};
        }
        private set;
    }
    private ProjectSelection__c projectSelection = new ProjectSelection__c();

    
    
    
    
    /**
    * @author       Kim Roth
    * @date         06/11/2013
    * @description  Method that creates lists for iteration, product and release backlog
    * @param        <none> 
    * @return       <none>
    */  
    public ViewBacklog() { 
        if(ApexPages.currentpage().getParameters().get('isProjectCloseToggle')=='true')
        {
            isHideClosedProjects = true;
        }
        else if(isHideClosedProjects == null){isHideClosedProjects = false;}
 
         //get color scheme from custom settings
        colorScheme = planTabColorScheme__c.getAll();
        
       system.debug(colorScheme);
       selectedProjectId = ApexPages.currentpage().getParameters().get('project');
       selectedbprocessId = ApexPages.currentpage().getParameters().get('bprocess');
       selectedActivityId = ApexPages.currentpage().getParameters().get('bactivity');
       selectionFilter = ApexPages.currentpage().getParameters().get('selectionFilter');
         // Retrieve the user's last project selection from the custom setting
if (ApexPages.currentpage().getParameters().get('project')== null)
 {
    ProjectSelection__c proj = ProjectSelection__c.getInstance(UserInfo.getUserId());
    selectedProjectId = proj.projectId__c;
    selectedbprocessId = proj.bprojectId__c;
             if(selectedbprocessId != null)
    getActivityItems();             
    selectedActivityId = proj.bactivity_Id__c;            
     if(selectedProjectId != null)
       getItems();
     flag='1';
  } 
   if(ApexPages.currentpage().getParameters().get('project')!= null || flag=='1')    
   { 
    
    
       
        //parentUserStoryRecordType = [SELECT id FROM RecordType WHERE name = 'Activity' AND sobjectType = 'Work_Product__c'];   
            
   
        
        
        //query for iterations
        iterationList = completeIterationList = [select id, name, Release__r.Id, state__c, start_date__c, end_date__c, plan_estimate__c,Task_Estimate__c, planned_velocity__c, Current_Iteration__c from iteration__c where Iteration_On__c=true order by end_date__c asc];
        system.debug('iterationlist'+iterationList);
        //query for releases
        releaseList = completeReleaseList = [select id , name, start_date__c, release_date__c from ss_release__c where WorkStreamId__c = :selectedProjectId order by start_date__c desc];        
        
        //query for release backlog
   
  
     QueryTextrelease = 'select id, name, work_product_id__c, plan_estimate__c, iteration__c, state__c, blocked__c, iteration__r.Name, ss_release__r.Name, rank__c, is_defect__c, Record_Type_Name__c , Project_Overview__c, Parent__c from Work_Product__c  where ss_release__c <> NULL and iteration__c = NULL '/*AND  recordtypeid !=\''+ parentUserStoryRecordType.id + '\''*/ ;
      /*    if(selectedProjectId != null)
          QueryTextrelease +=' AND WorkstreamId__c =\'' + selectedProjectId + '\'' ;
          if(selectedbprocessId != null)
          QueryTextrelease +=' AND Project_Overview__c =\'' + selectedbprocessId + '\'' ;
          if(selectedActivityId != null)
          QueryTextrelease +=' AND Activity__c =\'' + selectedActivityId + '\'' ;  */
         if(selectionFilter == 'Defects')
           QueryTextrelease +=' AND  Record_Type_Name__c=\'Defect\''  ;
          if(selectionFilter == 'User Stories')
          QueryTextrelease +=' AND  Record_Type_Name__c=\'User Story\''  ;
          QueryTextrelease +=' order by rank__c asc'  ;  
    releaseBacklogList = completeWPRList = database.query(QueryTextrelease);      
  
                              
        QueryTextiteration = 'select id, summary__c, name, work_product_id__c, plan_estimate__c, iteration__c, state__c, blocked__c, iteration__r.Name, ss_release__r.Name, rank__c, is_defect__c, Record_Type_Name__c , Project_Overview__c, Parent__c from Work_Product__c where  iteration__c <> NULL '/* AND  recordtypeid !=\''+ parentUserStoryRecordType.id + '\'' */;
     
         if(selectedProjectId != null)
          QueryTextiteration +=' AND WorkstreamId__c =\'' + selectedProjectId + '\'' ;
          if(selectedbprocessId != null)
          QueryTextiteration +=' AND Project_Overview__c =\'' + selectedbprocessId + '\'' ;
          if(selectedActivityId != null)
          QueryTextiteration +=' AND Activity__c =\'' + selectedActivityId + '\'' ;    
          if(selectionFilter == 'Defects')
          QueryTextiteration +=' AND  Record_Type_Name__c=\'Defect\''  ;
          if(selectionFilter == 'User Stories')
          QueryTextiteration +=' AND  Record_Type_Name__c=\'User Story\''  ;
          QueryTextiteration +=' order by rank__c asc'  ;  
    iterationBacklogList = completeWPIList = database.query(QueryTextiteration);
    
         if(selectedProjectId== '--Select a WorkStream--' ){
         	system.debug('Inside If loop');
         QueryTextiteration = 'select id, summary__c, name, work_product_id__c, plan_estimate__c, iteration__c, state__c, blocked__c, iteration__r.Name, ss_release__r.Name, rank__c, is_defect__c, Record_Type_Name__c , Project_Overview__c, Parent__c from Work_Product__c where  iteration__c <> NULL '/* AND  recordtypeid !=\''+ parentUserStoryRecordType.id + '\'' */;
     
       /*  if(selectedProjectId != null)
          QueryTextiteration +=' AND WorkstreamId__c =\'' + selectedProjectId + '\'' ;
          if(selectedbprocessId != null)
          QueryTextiteration +=' AND Project_Overview__c =\'' + selectedbprocessId + '\'' ;
          if(selectedActivityId != null)
          QueryTextiteration +=' AND Activity__c =\'' + selectedActivityId + '\'' ;   */ 
          if(selectionFilter == 'Defects')
          QueryTextiteration +=' AND  Record_Type_Name__c=\'Defect\''  ;
          if(selectionFilter == 'User Stories')
          QueryTextiteration +=' AND  Record_Type_Name__c=\'User Story\''  ;
          QueryTextiteration +=' order by rank__c asc'  ;  
    iterationBacklogList = completeWPIList = database.query(QueryTextiteration);
         
         }
         
    
        QueryText='select id, summary__c, name, work_product_id__c, plan_estimate__c, iteration__c, state__c, blocked__c, iteration__r.Name, ss_release__r.Name, rank__c, is_defect__c, Record_Type_Name__c , Project_Overview__c, Parent__c from Work_Product__c  where iteration__c = NULL and ss_release__c = NULL '/*and recordtypeid !=\''+ parentUserStoryRecordType.id + '\'' */;
          if(selectedProjectId != null)
          QueryText +=' AND WorkstreamId__c =\'' + selectedProjectId + '\'' ;
          if(selectedbprocessId != null)
          QueryText +=' AND Project_Overview__c =\'' + selectedbprocessId + '\'' ;
          if(selectedActivityId != null)
          QueryText +=' AND Activity__c =\'' + selectedActivityId + '\'' ;  
         if(selectionFilter == 'Defects')
           QueryText +=' AND  Record_Type_Name__c=\'Defect\''  ;
          if(selectionFilter == 'User Stories')
          QueryText +=' AND  Record_Type_Name__c=\'User Story\''  ; 
          QueryText +=' order by rank__c asc'  ;                  
         system.debug('pructbackloglist'+QueryText);     
         system.debug('pructbackloglist'+selectionFilter);     
         productBacklogList = completeWPList = database.query(QueryText);
         
       QueryText='select id, summary__c, name, work_product_id__c, plan_estimate__c, iteration__c, state__c, blocked__c, iteration__r.Name, ss_release__r.Name, rank__c, is_defect__c, Record_Type_Name__c , Project_Overview__c, Parent__c from Work_Product__c  where iteration__c = NULL and ss_release__c = NULL '/*and recordtypeid !=\''+ parentUserStoryRecordType.id + '\'' */;
       if(selectedProjectId== '--Select a WorkStream--' ){
        /*  if(selectedProjectId != null)
          QueryText +=' AND WorkstreamId__c =\'' + selectedProjectId + '\'' ;
          if(selectedbprocessId != null)
          QueryText +=' AND Project_Overview__c =\'' + selectedbprocessId + '\'' ;
          if(selectedActivityId != null)
          QueryText +=' AND Activity__c =\'' + selectedActivityId + '\'' ;  */
         if(selectionFilter == 'Defects')
           QueryText +=' AND  Record_Type_Name__c=\'Defect\''  ;
          if(selectionFilter == 'User Stories')
          QueryText +=' AND  Record_Type_Name__c=\'User Story\''  ; 
          QueryText +=' order by rank__c asc'  ;                  
         system.debug('pructbackloglist'+QueryText);     
         system.debug('pructbackloglist'+selectionFilter);     
         productBacklogList = completeWPList = database.query(QueryText);
       }
     
      }
    }
 /*   public static string getIdfromIteration(){
    List<Iteration__c> iterations= new List<Iteration__c>();	
    iterations = [select Id,Name,Release__c from Iteration__c where Id =: iterationId];
            if(iterations.size()>0){
            wp.SS_Release__c=iterations[0].Release__c;} 
            system.debug('###wp.SS_Release__c'+wp.SS_Release__c);
            return wp.SS_Release__c;
    } */

    @RemoteAction
    global static String updateWorkProductRank(List<Id> toIds,   String toRelease,   String toIteration,   Boolean toAscending, 
                                             List<Id> fromIds, String fromRelease, String fromIteration, Boolean fromAscending) {
      
        List<Work_Product__c> workProducts = new List<Work_Product__c>();
        List<Iteration__c> iterations= new List<Iteration__c>();
        List<Iteration__c> iterations1= new List<Iteration__c>();
        //List<Iteration__c> iterations= new List<Iteration__c>();
        Integer count = 0;
        toRelease     = String.isEmpty(toRelease)     ? null : toRelease;
        toIteration   = String.isEmpty(toIteration)   ? null : toIteration;
        fromRelease   = String.isEmpty(fromRelease)   ? null : fromRelease;
        fromIteration = String.isEmpty(fromIteration) ? null : fromIteration;  
        system.debug('*****toRelease'+toRelease+'toIteration'+toIteration);  
       // iterations = [select Id,Name,Release__c from Iteration__c where Id =: toIteration];

        for(Id wpId : toIds) {
            Work_Product__c wp = new Work_Product__c();
            wp.Id = wpId;
            wp.Rank__c = toAscending ? count : toIds.size() - count - 1;
            wp.Iteration__c = toIteration;
            wp.SS_Release__c = toRelease;   
            system.debug('iterations'+iterations);
           /* wp.SS_Release__c=iterations[0].Release__c; 
            system.debug('###wp.SS_Release__c'+wp.SS_Release__c);   
             system.debug('TTTtoRelease'+toRelease+'FFFfromRelease'+fromRelease+'TTTtoIteration'+toIteration); */
             if(toRelease <> NULL && toRelease != fromRelease)
              { wp.SS_Release__c =toRelease ;system.debug('###wp.SS_Release__c'+toRelease);}              
          count++;
          workProducts.add(wp);         
        } 
        
     
        if(toRelease != fromRelease || toIteration != fromIteration) {               
        system.debug('toRelease'+toRelease+'fromRelease'+fromRelease+'toIteration'+toIteration+'fromIteration'+fromIteration);
        count = 0;
        for(Id wpId : fromIds) {
                workProducts.add(new Work_Product__c(
                Id            = wpId,
                Rank__c       = fromAscending ? count : fromIds.size() - count - 1,
                Iteration__c  = fromIteration
                 ));                                             
        	
           
        }
        }
       
        try{update workProducts;} catch(DmlException e) { system.debug(e.getDmlMessage(0));
        system.debug('work product update'+workProducts);   
           return e.getDmlMessage(0);
        }

        return 'success';
      
    }
     
    /**
    * @author       Kim Roth
    * @date         06/25/2013
    * @description  Method to return select options for each project
    * @param        <none>
    * @return       options - List of select options
    */  
    public List<SelectOption> getItems() {
        
        List<SelectOption> options = new List<SelectOption>();
        List<Project_Overview__c> projectList = new List<Project_Overview__c>();
        //RecordType projectRT = new RecordType();
        
        //projectRT = [select id FROM RecordType WHERE name = 'Business Process' AND sObjectType = 'Project_Overview__c'];
       options.add(new SelectOption('--Select a Business Process --', '--Select a Business Process --')); 
       system.debug('selectedProjectId  :'+selectedProjectId );      
      if(selectedProjectId != null ) {
            if(isHideClosedProjects)
            {   
                projectList = [select id, name from Project_Overview__c where Project_Stage__c != 'Closed' /*AND recordTypeID =: projectRT.id*/ AND Program__c =: selectedProjectId ORDER BY name];
            }
            else
            {   
                projectList = [select id, name from Project_Overview__c where /*recordTypeID =: projectRT.id AND*/ Work_Stream__c =: selectedProjectId ORDER BY name];
            }
      }
      else
      {
         options.add(new SelectOption('', '--None--'));
      }
        if(projectList.size() > 0) {
            for(Project_Overview__c project : projectList){
                options.add(new SelectOption(project.id, project.name));
            }
        } 
         else
      {
         options.add(new SelectOption('', '--None--'));
      }
        return options;
    }
    
    
     /**
    * @author       Vaishali Mankar
    * @date         04/01/2014
    * @description  Method to return select options for each WorkStream
    * @param        <none>
    * @return       options - List of select options
    */  
    public List<SelectOption> getWorkStreamitems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Project_Overview__c> projectList = new List<Project_Overview__c>();
        //RecordType projectRT = new RecordType();
       
        //projectRT = [select id FROM RecordType WHERE name = 'Work Stream' AND sObjectType = 'Project_Overview__c'];
         options.add(new SelectOption('--Select a WorkStream--', '--Select a WorkStream--'));
     /* if(isHideClosedProjects)
        {
            projectList = [select id, name from Project_Overview__c where Project_Stage__c != 'Closed' AND recordTypeID =: projectRT.id ORDER BY name];
        }
        else
        {
            projectList = [select id, name from Project_Overview__c where recordTypeID =: projectRT.id ORDER BY name];
        }
        if(projectList.size() > 0) {
            for(Project_Overview__c project : projectList){
                options.add(new SelectOption(project.id, project.name));
            }
        } else {
            
            options.add(new SelectOption(projectRT.id, '--None--'));
        }*/
        
        Schema.DescribeFieldResult fieldResult = Project_Overview__c.Work_Stream__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple)
        options.add(new SelectOption(p.getValue(), p.getValue()));       
        return options;
           
    }
    
    
      /**
    * @author       Vaishali Mankar
    * @date         04/01/2014
    * @description  Method to return select options for each Activity
    * @param        <none>
    * @return       options - List of select options
    */  
    public List<SelectOption> getActivityItems() {
        List<SelectOption> options = new List<SelectOption>();
        List<Activity__c> ActivityList = new List<Activity__c>();
        RecordType projectRT = new RecordType();
       
        //projectRT = [select id FROM RecordType WHERE name = 'Activity' AND sObjectType = 'Work_Product__c'];
         options.add(new SelectOption('', '--Select a Activity--'));
      if(selectedbprocessId != null ) {
            if(isHideClosedProjects)
            {
                ActivityList = [select id, name from Activity__c where  /*recordTypeID =: projectRT.id and*/ Business_Process__c =: selectedbprocessId ORDER BY name];
            }
            else
            {
                ActivityList = [select id, name from Activity__c where /*recordTypeID =: projectRT.id and*/ Business_Process__c =: selectedbprocessId ORDER BY name];
            }
        }
        else
        {
         options.add(new SelectOption('', '--None--'));
        }
        if(ActivityList.size() > 0) {
            for(Activity__c project : ActivityList ){
                options.add(new SelectOption(project .id, project.name));
            }
        } else {
            
            options.add(new SelectOption('', '--None--'));
        }
        return options;
    }
    
    
    
    /**
    * @author       Yael Perez
    * @date         09/23/2013
    * @description  Helper method to verify if a project which was retrieved from the custom setting exists as an option in the
    *               Project selectOption picklist.
    * @param        Project ID
    * @return       true if the project exists in the picklist
    */
    private boolean isStoredProjectExistInProjectPicklist(Id projId) {
        Project_Overview__c po;
        try {
            po = [select id, name from Project_Overview__c where id =: projId];
        } catch (Exception ex) {
            return false;
        }
        for (SelectOption option : getItems()) {
            if (option.getValue().equalsIgnoreCase(po.Id)) {
                return true;
            }
        }
        return false;
    }
    
    public PageReference selectProject() {
        // Store project ID in custom setting.
        projectSelection = ProjectSelection__c.getInstance(UserInfo.getUserId());
        projectSelection.projectId__c = selectedProjectId;
        system.debug('Select Project ID : ' + selectedProjectId);
        try { upsert projectSelection; } catch (System.DmlException ex) {
            System.debug('Exception thrown while trying to upsert custom setting: ' + ex.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the selected project, please contact your administrator.﻿');
            ApexPages.addMessage(myMsg);
        }
        
        // Send the user to the backlog page for the selected project
        PageReference projectBacklogPage = Page.ViewBacklog;
        projectBacklogPage.getParameters().put('project', selectedProjectId);        
        projectBacklogPage.setRedirect(true);
        // return null;
        return projectBacklogPage;
    }
    public PageReference selectWorkStream() {
        

        return null;
    }
     public PageReference  selectBusinessprocess() {
        /* new code added */
      
        
        projectSelection = ProjectSelection__c.getInstance(UserInfo.getUserId());
        projectSelection.bprojectId__c = selectedbprocessId;
        try { upsert projectSelection; } catch (System.DmlException ex) {
            System.debug('Exception thrown while trying to upsert custom setting: ' + ex.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the selected project, please contact your administrator.﻿');
            ApexPages.addMessage(myMsg);
        }
        
        // Send the user to the backlog page for the selected project
        PageReference projectBacklogPage = Page.ViewBacklog;
        projectBacklogPage.getParameters().put('project', selectedProjectId);   
         projectBacklogPage.getParameters().put('bprocess', selectedbprocessId);      
        projectBacklogPage.setRedirect(true);
        // return null;
        return projectBacklogPage;
        
  
    }
   
     public PageReference selectUserStory() {
        

        return null;
    }
     public PageReference selectActivity() {
        
     
           projectSelection = ProjectSelection__c.getInstance(UserInfo.getUserId());
        projectSelection.bactivity_Id__c = selectedActivityId;
        try { upsert projectSelection; } catch (System.DmlException ex) {
            System.debug('Exception thrown while trying to upsert custom setting: ' + ex.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the selected project, please contact your administrator.﻿');
            ApexPages.addMessage(myMsg);
        }
        
        // Send the user to the backlog page for the selected project
        PageReference projectBacklogPage = Page.ViewBacklog;
        projectBacklogPage.getParameters().put('project', selectedProjectId);   
        projectBacklogPage.getParameters().put('bprocess', selectedbprocessId); 
        projectBacklogPage.getParameters().put('bactivity', selectedActivityId);         
        projectBacklogPage.setRedirect(true);
        // return null;
        return projectBacklogPage;
    }
    
    
    /**
    * @author       Yael Perez
    * @date         09/09/2013
    * @description  Action invoked when filtering the WP list on the plan page.
    * @param        <none>
    * @return       null
    */
    public PageReference filterWorkProducts() {
      
        // Send the user to the backlog page for the selected project
        PageReference projectBacklogPage = Page.ViewBacklog;
        projectBacklogPage.getParameters().put('project', selectedProjectId);   
        projectBacklogPage.getParameters().put('bprocess', selectedbprocessId); 
        projectBacklogPage.getParameters().put('bactivity', selectedActivityId); 
        projectBacklogPage.getParameters().put('selectionFilter', selectionFilter);      
             
        projectBacklogPage.setRedirect(true);
        // return null;
        return projectBacklogPage;
    }
        
    /**
    * @author       Yael Perez
    * @date         09/09/2013
    * @description  Action invoked when hiding old iterations.
    * @param        <none>
    * @return       null
    */    
    public PageReference hidePastIterations() {
        List<Iteration__c> iterList = new List<Iteration__c>();
        if (isHidePastIterations) {
            for (iteration__c iteration : iterationList) {
                if (iteration.current_iteration__c) {
                    iterList.add(iteration);
                }
            }
        } else if (!isHidePastIterations) {
            iterList = completeIterationList;
        }
        iterationList = iterList;
        return null;
    }
 
    /**
    * @author       Yael Perez
    * @date         09/09/2013
    * @description  Action invoked when hiding past releases.
    * @param        <none>
    * @return       null
    */       
    public PageReference hidePastReleases() {
        List<ss_release__c> relList = new List<ss_release__c>();
        if (isHidePastReleases) {
            for (ss_release__c release : releaseList) {
                if (release.release_date__c >= Date.Today()) {
                    relList.add(release);
                }
            }
        } else if (!isHidePastReleases) {
              relList = completeReleaseList;        
        }
        releaseList = relList;
        return null;
    }

    public PageReference hideClosedProjects()
    {
        //was a hidden project selected when the hide option was toggled?
        if(!isStoredProjectExistInProjectPicklist(selectedProjectId)){
            return redirectToStickyProject();
        }
        return null;
    }
    
    /**
    * @author       Tarren Anderson
    * @date         10/01/2013
    * @description  Redirect if user doesn't have a custom setting, or the project ID no longer exists.
    * @param        <none>
    * @return       null
    */    

     public PageReference redirectToStickyProject() {
         ProjectSelection__c proj = ProjectSelection__c.getInstance(UserInfo.getUserId());
         //check if custom setting exists, or if the Project ID is not an option in the Project picklist
         if (proj.projectId__c == null || !isStoredProjectExistInProjectPicklist(proj.projectId__c)) 
        {
            //get id of first project in the picklist
            selectedProjectId = getItems()[0].getValue();            
            //set the custom setting equal to the ID of the first project
            proj.projectId__c = selectedProjectId;
                //UPDATE CUSTOM SETTING FOR PROJECT SELECTION
                try { upsert proj; } catch (System.DmlException ex) {
                    System.debug('Exception thrown while trying to upsert custom setting: ' + ex.getMessage());
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An error occurred while loading the selected project, please contact your administrator.﻿');
                    ApexPages.addMessage(myMsg);
                }
            //return the page reference    
            PageReference projectBacklogPage = Page.ViewBacklog;
            projectBacklogPage.getParameters().put('project', selectedProjectId); 
            if(isHideClosedProjects)
            {
                projectBacklogPage.getParameters().put('isProjectCloseToggle', 'true'); 
            }       
            projectBacklogPage.setRedirect(true);
            return projectBacklogPage;
        }
        return null;
     
    }


    /**
    * @author       Tarren Anderson
    * @date         10/06/2013
    * @description  Return map containing color scheme from custom settings.
    * @param        <none>
    * @return       Map
    */   
        public Map<String, planTabColorScheme__c> getMyColor()
    {
        return colorScheme;
        
    }
    
}