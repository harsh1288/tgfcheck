/*********************************************************************************
* {Test} Class: {saveAndNewRPControllerTest}
* Created by {Rahul Kumar},  {DateCreated 19-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of saveAndNewRPController Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    24-Nov-2014
*********************************************************************************/
@isTest
public class saveAndNewRPControllerTest {
            static saveAndNewRPController ext;
            static saveAndNewRPController extW;
            static Period__c masterObject;
            static Period__c masterObjectWithoutPF;
            static PageReference pref;
            static Grant__c grant;
            static User testUser = TestClassHelper.createExtUser();    
    Public static testMethod Void TestSaveAndNew(){
             testUser.username = 'savenewrp1@example.com';
    insert testUser;
            Test.startTest();
            grant = TestClassHelper.insertGrant(TestClassHelper.insertAccount());
            Implementation_Period__c ip = TestClassHelper.createIPWithConceptNote(grant,TestClassHelper.insertAccount());
            insert ip;
            Performance_Framework__c pf = TestClassHelper.createPF(ip);
            insert pf;
            masterObject = TestClassHelper.createPeriod();
            masterObject.Performance_Framework__c = pf.id;
            masterObjectWithoutPF = TestClassHelper.createPeriod();
            pref = Page.newHPCRedirection;
            Test.setCurrentPage(pref);
            ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
            ext = new saveAndNewRPController(con);
            ext.createNewRPUrl();
            ApexPages.StandardController conW = new ApexPages.StandardController(masterObjectWithoutPF);
            extW = new saveAndNewRPController(conW);
            extW.createNewRPUrl();
            System.runAs(testUser) {                                    
                ext.createNewRPUrl();            
            }
            pref.getParameters().put('retUrl','GM%2F');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
                extW.createNewRPUrl();            
            }
            pref.getParameters().put('retUrl','?');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
            extW.createNewRPUrl();            
        }
            Test.stopTest();
    }
}