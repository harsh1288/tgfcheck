/*********************************************************************************
* Test class:   Test_IndGoaljxn_preventdelete
  Trigger: Indicator_Goal_junction_prevent_delete 
*  DateCreated : 26/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - 
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           26/11/2014      INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
public class Test_IndGoaljxn_preventdelete {

    public static testMethod void Test_IndGoaljxn_preventdelete() {
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();
        insert objGoal;
        
        Goals_Objectives__c objGo = TestClassHelper.createGoalsObjectives();
        insert objGo;
        
        Grant_Indicator__c objGI = TestClassHelper.createGrantIndicator();
        objGI.Goal_Objective__c = objGoal.Id; 
        insert objGI;
        
        Ind_Goal_Jxn__c objJxn = new Ind_Goal_Jxn__c();
        objJxn.Goal_Objective__c = objGoal.Id;
        objJxn.Indicator__c = objGI.Id;
        insert objJxn;
        
        Ind_Goal_Jxn__c objJxnO = new Ind_Goal_Jxn__c();
        objJxnO.Goal_Objective__c = objGo.Id;
        objJxnO.Indicator__c = objGI.Id;
        insert objJxnO;
        
        
        delete objJxn;
         
    }
}