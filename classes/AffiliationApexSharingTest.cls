@isTest(seealldata=false)
public class AffiliationApexSharingTest{

  /*      
public static testMethod void AffiliationApexSharingTest(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Account a = new Account();
        a.Name = 'Partner Organisation';
        a.Short_Name__c = 'Name';
        insert a;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        insert objIP;
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        insert con;
        
         User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;
        Test.StartTest();
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = false;
        objAff.Access_Level__c = 'Admin';
        objAff.npe5__Organization__c  = objAcc.Id;
        objAff.npe5__Contact__c  = con.Id;
        objAff.RecordTypeId  = label.PR_Affiliation_RT;
        objAff.npe5__Status__c  = 'Current';
        insert objAff;
        
        Concept_Note__c objCN1 = TestClassHelper.createCN();
        objCN1.CCM_New__c = objAcc.id;
        insert objCN1;
        Implementation_Period__c objIP1 = TestClassHelper.createIP(objGrant,objAcc);
        objIP1.Concept_Note__c = objCN1.Id;
        insert objIP1;
        Test.stopTest();
        
}
public static testMethod void AffiliationApexSharingTest2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Account a = new Account();
        a.Name = 'Partner Organisation';
        a.Short_Name__c = 'Name'; 
        insert a;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        insert objIP;
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        insert con;
        
         User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;
      Test.StartTest();
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = False;
        objAff.Access_Level__c = 'Admin';
        objAff.npe5__Organization__c  = objAcc.Id;
        objAff.npe5__Contact__c  = con.Id;
        objAff.RecordTypeId  = label.PR_Affiliation_RT;
        objAff.npe5__Status__c  = 'Current';
        insert objAff;
        
        Concept_Note__c objCN1 = TestClassHelper.createCN();
        objCN1.CCM_New__c = objAcc.id;
        insert objCN1;
        Implementation_Period__c objIP1 = TestClassHelper.createIP(objGrant,objAcc);
        objIP1.Concept_Note__c = objCN1.Id;
        insert objIP1;
        Test.stopTest();
}
public static testMethod void AffiliationApexSharingTest3(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
        
        Account objAcc = TestClassHelper.insertAccount();
        
        
        Account a = new Account();
        a.Name = 'Partner Organisation';
        a.Short_Name__c = 'Name';
        
        insert a;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        insert objIP;
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        insert con;
        
         User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;
      Test.StartTest();
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = False;
        objAff.Access_Level__c = 'Read';
        objAff.npe5__Organization__c  = objAcc.Id;
        objAff.npe5__Contact__c  = con.Id;
        objAff.RecordTypeId  = label.CM_Affiliation_RT;
        objAff.npe5__Status__c  = 'Current';
        insert objAff;
        
        Concept_Note__c objCN1 = TestClassHelper.createCN();
        objCN1.CCM_New__c = objAcc.id;
        insert objCN1;
        Implementation_Period__c objIP1 = TestClassHelper.createIP(objGrant,objAcc);
        objIP1.Concept_Note__c = objCN1.Id;
        insert objIP1;
        
        objAff.Is_Inactive_c__c = false;
        update objAff;
        Test.stopTest();
        
        
}
public static testMethod void AffiliationApexSharingTest4(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Account a = new Account();
        a.Name = 'Partner Organisation';
        a.Short_Name__c = 'Name';
        insert a;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        insert objIP;
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        insert con;
        
         User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;
        
        Test.StartTest();
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = False;
        objAff.Access_Level__c = 'Read';
        objAff.npe5__Organization__c  = objAcc.Id;
        objAff.npe5__Contact__c  = con.Id;
        objAff.RecordTypeId  = label.PR_Affiliation_RT;
        objAff.npe5__Status__c  = 'Current';
        insert objAff;
        
        Concept_Note__c objCN1 = TestClassHelper.createCN();
        objCN1.CCM_New__c = objAcc.id;
        insert objCN1;
        Implementation_Period__c objIP1 = TestClassHelper.createIP(objGrant,objAcc);
        objIP1.Concept_Note__c = objCN1.Id;
        insert objIP1;
        
        objAff.Is_Inactive_c__c = true;
        //update objAff;
        Test.stopTest();
}
*/
}