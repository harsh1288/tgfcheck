public without sharing class ContactTriggerHandler{
    
    public static void shareNewContact(List<Contact> ContactList){
        
       Map<id,Contact> AccountContactMap = new Map<id,Contact>();
       Map<id,id> ManageContactUserMap = new Map<id,id>();
       Map<id,id> AccountManageContactMap = new map<id,id>();
       List<ContactShare> ContactShareList = new List<ContactShare>();
       List<id> ManageContactList = new List<id>();
       
        for(contact Con:ContactList){
            AccountContactMap.put(Con.Accountid,Con);
        }
        
        List<npe5__Affiliation__c> AffiliationForManageContact = [select id,npe5__contact__c,npe5__organization__c,npe5__status__c FROM npe5__Affiliation__c WHERE npe5__Organization__c IN: AccountContactMap.keyset() AND Manage_Contacts__c =:true];
    
    
        for(npe5__Affiliation__c Aff: AffiliationForManageContact){
            ManageContactList.add(Aff.npe5__contact__c);
            AccountManageContactMap.put(Aff.npe5__organization__c,Aff.npe5__contact__c);   
        }
        
        List<user> ManageContactUser = [Select id,Name,contactid from user where ContactID IN: ManageContactList AND isActive=:True];
        
        if(!ManageContactUser.isEmpty()){
            For(User u: ManageContactUser){
                ManageContactUserMap.put(u.contactid,u.id);
            }
            
            for(contact Cont :ContactList){
                contactShare CS = new ContactShare();
                CS.ContactId = cont.id;
                CS.ContactAccessLevel = 'Edit';
                id ContactId = AccountManageContactMap.get(cont.Accountid);
                if(Contactid!=NULL){
                    CS.UserOrGroupId = ManageContactUserMap.get(contactid);
                    ContactShareList.add(CS); 
                }     
            }
        }
        
        if(!ContactShareList.isEmpty())
            insert ContactShareList;    
    }
  // Method for updating FedID on User  
    public static void updateFedID(List<id> ConIdList){
        List<User> UpdateUserList = new List<User>();
        List<User> Us = [Select id,isActive,FederationIdentifier, ContactID, Contact.Federation_ID__c FROM User WHERE ContactID IN: ConIDList AND isActive = True AND (User.Profile.UserLicense.Name='Partner Community Login' OR User.Profile.UserLicense.Name='Overage Customer Portal Manager Custom')];
   
        if(Us.size()>0){
            for(User U: Us){
               if(U.FederationIdentifier != U.Contact.Federation_ID__c){ 
               U.FederationIdentifier = U.Contact.Federation_ID__c;
               UpdateUserList.add(u);
               }
            }
           
            if(UpdateUserList.size()>0)
               update UpdateUserList;
         }       
    }   
}