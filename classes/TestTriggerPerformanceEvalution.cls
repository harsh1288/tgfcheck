/*********************************************************************************
* Class: TestTriggerPerformanceEvalution
* Created by {DeveloperName}, {DateCreated 11/22/2013}
----------------------------------------------------------------------------------
* Purpose: Test class for Trigger.
----------------------------------------------------------------------------------
*********************************************************************************/
@isTest
Private Class TestTriggerPerformanceEvalution {
    static testMethod void TestTriggerPerformanceEvalution() {
        
        List<RecordType> lstRecordTypeAccount = [Select Id,DeveloperName from RecordType Where SObjectType = 'Account' AND DeveloperName= 'LFA' limit 1];
        List<Account> accountsToInsert = new List<Account>();
        Account objAccount = new Account();
        objAccount.RecordTypeId = lstRecordTypeAccount[0].Id;
        objAccount.Name = 'Test1';
        //insert objAccount;
        accountsToInsert.add(objAccount);
        Account objAccount2= new Account();
        objAccount2.RecordTypeId = lstRecordTypeAccount[0].Id;
        objAccount2.Name = 'Test2';
        objAccount2.ParentID = objAccount.ID;
        //insert objAccount2;
        accountsToInsert.add(objAccount2);
        insert accountsToInsert;
        
        List<Contact> contactsToInsert = new List<Contact>();
        Contact objContact = new Contact();
        objContact.LastName = 'TestLast';
        objContact.AccountID = objAccount2.ID;
        objContact.Receive_PET_Alerts__c = true;
        //insert objContact;
        contactsToInsert.add(objContact);
        Contact objContact1 = new Contact();
        objContact1.LastName = 'TestLast';
        objContact1.AccountID = objAccount.ID;
        objContact1.Receive_PET_Alerts__c = true;
        //insert objContact1;
        contactsToInsert.add(objContact1);
        insert contactsToInsert;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',ContactID = objContact.ID);
        
        
        
        Country__c objCountry = new Country__c();
        objCountry.Name ='India';
        objCountry.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry ;
        
        Country__c objCountry1 = new Country__c();
        objCountry1.Name ='India1';
        //objCountry1.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry1 ;
        
        LFA_Work_Plan__c lfwp = new LFA_Work_Plan__c();
        lfwp.Name = 'Test';
        lfwp.LFA_Access__c = 'Read';
        lfwp.LFA__c = objAccount2.Id;
        lfwp.Country__c = objCountry.ID;
        
        insert lfwp;
        
        LFA_Work_Plan__c lfwp1 = new LFA_Work_Plan__c();
        lfwp1.Name = 'Test';
        lfwp1.LFA_Access__c = 'Read';
        lfwp1.LFA__c = objAccount2.Id;
        lfwp1.Country__c = objCountry1.ID;
        insert lfwp1;       
        
        PET_Access__c objPETAccess1 = new PET_Access__c();
        objPETAccess1.LFA_Work_Plan__c = lfwp1.id;
        objPETAccess1.PET_Access__c = 'Read';
        objPETAccess1.User__c = u.id;
        objPETAccess1.Receive_PET_Alerts__c = true;
        insert objPETAccess1 ; 
        Performance_Evaluation__c objPE1 = new Performance_Evaluation__c();
        objPE1.Name = 'PE1';
        objPE1.Start_Date__c = system.today();
        objPE1.Start_Date__c = system.today()+1;
        objPE1.LFA_Work_Plan__c = lfwp1.Id;
        objPE1.begin_alerts__c =true;
        objPE1.Status__C ='LFA Response Complete';
        
        insert objPE1;
        Test.startTest();
        objPE1.begin_alerts__c =true;
        objPE1.Status__C ='Submitted to LFA';
        update objPE1;
        
        PET_Access__c objPETAccess = new PET_Access__c();
        objPETAccess.LFA_Work_Plan__c = lfwp.id;
        objPETAccess.PET_Access__c = 'Read';
        objPETAccess.User__c = u.id;
        objPETAccess.Receive_PET_Alerts__c = true;
        insert objPETAccess ;
        
        Performance_Evaluation__c objPE = new Performance_Evaluation__c();
        objPE.Name = 'PE';
        objPE.Start_Date__c = system.today();
        objPE.Start_Date__c = system.today()+1;
        objPE.LFA_Work_Plan__c = lfwp.Id;
        objPE.begin_alerts__c =true;
        objPE.Status__C ='LFA Response Complete';
        
        insert objPE; 
        
        PET_Response__c objPETResponse = new PET_Response__c();
        objPETResponse.Performance_Evaluation__c=objPE .id;
        objPETResponse.Type__c = 'LFA PSM';
        objPETResponse.Status__c = 'Incomplete';
        objPE.begin_alerts__c =false;
        
        insert objPETResponse ;
        objPETResponse.pet_status__c ='Begin Alerts';
        objPE.Status__C ='Begin Alerts';
        
        List<PET_Response__c> lstPETResponse = new List<PET_Response__c>();
        lstPETResponse.add(objPETResponse);
        update objPE; 
        objPE.Status__C ='Submitted to LFA';
        update objPE; 
        Test.stopTest();
        
        
    }
}