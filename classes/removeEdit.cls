public with sharing class removeEdit {
 
   Public id recID {get;set;}
   IP_Detail_Information__c IP;
   Public boolean check;
  
   public removeEdit(ApexPages.StandardController stdController) {
       recId =  ApexPages.currentpage().getparameters().get('id');
       IP = [Select id,Implementation_period__c from IP_Detail_Information__c where id=: recid];
       check = false;
   }
   
   Public pageReference returnDetailPage(){
        pageReference pr= new pageReference('/'+recId ); 
        pr.setRedirect(true);
        return pr;
   }
   
   Public pageReference checkprof(){
       Id profileId=userinfo.getProfileId();
       Profile pro = [SELECT Id,Name from Profile where id =:profileId];
       system.debug(pro);
       If(Pro.name == 'System Administrator' || Pro.name == 'Super User'){
           pageReference pr1= new pageReference('/'+recId +'/e?nooverride=1&retURL='+IP.Implementation_period__c ); 
           pr1.setRedirect(true);
           return pr1;
       } 
       else{
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Update record from Detail View of record');
           ApexPages.addMessage(myMsg); 
           system.debug('Test1');
           return null;
       }
       
   } 
}