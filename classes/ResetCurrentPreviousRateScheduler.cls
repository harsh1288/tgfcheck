/*********************************************************************************
*  Class: ResetCurrentPreviousRateScheduler
* Created by {DeveloperName}, {DateCreated 07/10/2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - This class is use for populated all rate records from the rate object.
- It is run on 1 january every year mid night.
----------------------------------------------------------------------------------
* Unit Test: {TestResetCurrentPreviousRateScheduler}
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
    1.0      Name             07/10/2014     INITIAL DEVELOPMENT  
*********************************************************************************/
global class ResetCurrentPreviousRateScheduler implements Schedulable{
    global void execute(SchedulableContext sc) {
        ResetCurrentPreviousRateScheduler objRCPRS = new ResetCurrentPreviousRateScheduler();
            String sch = '0 0 0 1 1 ? *';
        if(test.isrunningtest() == false){
            system.schedule('Reset Current and Previous Rate', sch, objRCPRS);
        }
        String strSOQL = 'Select Id,Name,Difference__c,Approval_Process_Type__c,Contact__c,Country__c,Active__c,Rate__c,GF_Approved__c,LFA_Location__c,Last_Year_Active__c,Last_Year_Daily_Rate__c,Last_Year_GF_Approved__c,Last_Year_LFA_Location__c,LFA_of_contact__c,LFA_Role__c,Next_Year_Active__c,Next_Year_Daily_Rate__c,Next_Year_GF_Approved__c,Next_Year_LFA_Location__c,Other_LFA_Role__c From Rate__c';
        ResetCurrentPreviousRateBatch objBatch = new ResetCurrentPreviousRateBatch(strSOQL);
        ID BatchProcessId = Database.executeBatch(objBatch, 100);
    }
}