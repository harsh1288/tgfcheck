/*********************************************************************************
* {Test} Class: {TargetForIndicatorExt_Test}
* Purpose/Methods:
    - Used to cover all methods of TargetForIndicatorExt.

*********************************************************************************/

@isTest(seeAlldata = false)
public class TargetForIndicatorExt_Test{
    
    Public static testMethod void TestTargetInd4(){
        //test.startTest();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1]; 
        
        User user1 = new User(Alias = 'standt1', Email='standarduser@testorg1.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg1.com');
        
        User user2 = new User(Alias = 'standt2', Email='standarduser@testorg2.com', 
        EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='fr', 
        LocaleSidKey='fr', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg2.com');
        
        User user3 = new User(Alias = 'standt3', Email='standarduser@testorg3.com', 
        EmailEncodingKey='UTF-8', LastName='Testing3', LanguageLocaleKey='ru', 
        LocaleSidKey='ru', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg3.com');
        
        User user4 = new User(Alias = 'standt4', Email='standarduser@testorg4.com', 
        EmailEncodingKey='UTF-8', LastName='Testing4', LanguageLocaleKey='es', 
        LocaleSidKey='es', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg4.com');
        
        Account objAcc= TestClassHelper.createAccount();
        insert objAcc;
        
        Grant__c objGrant = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_new__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP= TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.Status__c = 'Concept-note';
        insert objIP;
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();
        objGoal.Concept_note__c = objCN.Id;
        insert objGoal;
        
        Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
        insert objCM;
        
        Catalog_Module__c objCM1 = TestClassHelper.createCatalogModule();
        objCM1.Name = 'Test CM1';
        objCM1.Component__c = 'Malaria';
        objCM1.component_multi__c = 'Malaria';
        insert objCM1;
        
        Module__c objModule = TestClassHelper.createModule();
        //objModule.Implementation_Period__c = objIP.Id;
        objModule.Catalog_Module__c = objCM.id;
        objModule.Component__c = 'Malaria';
        insert objModule;
        
        Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention();
        objCI.Catalog_Module__c  = objCM.Id;
        insert objCI; 
        
        Indicator__c objCatInd = TestClassHelper.createCatalogIndicator();
        objCatInd.Indicator_Type__c = 'Coverage/Output';
        insert objCatInd;
        
        Indicator__c objCatInd2 = TestClassHelper.createCatalogIndicator();
        objCatInd2.Indicator_Type__c = 'Impact';
        insert objCatInd2;
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Concept_note__c = objCN.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Standard_or_Custom__c = 'Standard';
        objInd.Indicator__c = objCatInd2.Id;
        objInd.Goal_Objective__c = objGoal.Id;
        objInd.Data_Type__c = 'Number';
        objInd.Reporting_Frequency__c = '12 Months';
        insert objInd;
        
        Module__c objIPModule = TestClassHelper.createModule();
        objIPModule.Implementation_Period__c = objIP.Id;
        //objIPModule.Catalog_Module__c = objCM.id;
        objIPModule.Component__c = 'Malaria';
        insert objIPModule;
        
        Grant_Indicator__c objInd1 = TestClassHelper.createGrantIndicator();
        objInd1.Grant_Implementation_Period__c = objIP.Id;
        objInd1.Indicator_Type__c = 'Coverage/Output';
        objInd1.Standard_or_Custom__c = 'Standard';
        objInd1.Parent_Module__c = objIPModule.Id;
        objInd1.Indicator__c = objCatInd.Id;
        insert objInd1;
        
        Grant_Intervention__c objInt = TestClassHelper.createGrantIntervention(objIP);
        objInt.Name = 'TestIntervention';
        objInt.Module__c = objIPModule.Id;
        insert objInt;
        Ind_Goal_Jxn__c objJxn = TestClassHelper.insertIndicatorGoalJxn(objGoal,objInd);
        
        test.startTest();
        
        Period__c objPeriod = TestClassHelper.createPeriod();
        objPeriod.Implementation_Period__c = objIP.Id;
        objPeriod.Base_Frequency__c = 'Yearly';
        objPeriod.Start_Date__c = Date.today();
        objPeriod.EFR__c = true;
        objPeriod.EFR_Due_Date__c = system.today();
        objPeriod.PU__c=true;
        objPeriod.PU_Due_Date__c = system.today();
        objPeriod.Audit_Report__c = true;
        objPeriod.AR_Due_Date__c=system.today();
        objPeriod.DR__c=true;
        objPeriod.Type__c = 'Reporting';
        objPeriod.Due_Date__c =system.today();
        objPeriod.Flow_to_GrantIndicator__c = true;
        insert objPeriod;
        
        Result__c objResult = new Result__c();
        objResult.Target__c = 67;
        objResult.Target_Numerator__c = 98;
        objResult.Target_Denominator__c = 100;
        objResult.Indicator__c = objInd.id;
        objResult.Period__c = objPeriod.Id;
        objResult.Targetnew__c = 'Per 100k';
        insert objResult;
        
        System.runAs(user1) {
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI1 = new TargetForIndicatorExt(sc1);
            //clsTFI1.fillPeriods();
        }
        
        System.runAs(user2) {
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI2 = new TargetForIndicatorExt(sc2);
        }
        
        System.runAs(user3) {
            ApexPages.Standardcontroller sc3 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI3 = new TargetForIndicatorExt(sc3);
        }
        
        System.runAs(user4) {
            ApexPages.Standardcontroller sc4 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI4 = new TargetForIndicatorExt(sc4);
        }
        
        test.stopTest();
    }
    
    
    Public static testMethod void TestTargetInd5(){
        //test.startTest();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1]; 
        
        User user1 = new User(Alias = 'standt1', Email='standarduser@testorg1.com', 
        EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg1.com');
        
        User user2 = new User(Alias = 'standt2', Email='standarduser@testorg2.com', 
        EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='fr', 
        LocaleSidKey='fr', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg2.com');
        
        User user3 = new User(Alias = 'standt3', Email='standarduser@testorg3.com', 
        EmailEncodingKey='UTF-8', LastName='Testing3', LanguageLocaleKey='ru', 
        LocaleSidKey='ru', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg3.com');
        
        User user4 = new User(Alias = 'standt4', Email='standarduser@testorg4.com', 
        EmailEncodingKey='UTF-8', LastName='Testing4', LanguageLocaleKey='es', 
        LocaleSidKey='es', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@testorg4.com');
        
        Account objAcc= TestClassHelper.createAccount();
        insert objAcc;
        
        Grant__c objGrant = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_new__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP= TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.Status__c = 'Concept-note';
        insert objIP;
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();
        objGoal.Concept_note__c = objCN.Id;
        insert objGoal;
        
        Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
        insert objCM;
        
        Catalog_Module__c objCM1 = TestClassHelper.createCatalogModule();
        objCM1.Name = 'Test CM1';
        objCM1.Component__c = 'Malaria';
        objCM1.component_multi__c = 'Malaria';
        insert objCM1;
        
        Module__c objModule = TestClassHelper.createModule();
        //objModule.Implementation_Period__c = objIP.Id;
        objModule.Catalog_Module__c = objCM.id;
        objModule.Component__c = 'Malaria';
        insert objModule;
        
        Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention();
        objCI.Catalog_Module__c  = objCM.Id;
        insert objCI; 
        
        Indicator__c objCatInd = TestClassHelper.createCatalogIndicator();
        objCatInd.Indicator_Type__c = 'Coverage/Output';
        insert objCatInd;
        
        Indicator__c objCatInd2 = TestClassHelper.createCatalogIndicator();
        objCatInd2.Indicator_Type__c = 'Impact';
        insert objCatInd2;
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Concept_note__c = objCN.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Standard_or_Custom__c = 'Standard';
        objInd.Indicator__c = objCatInd2.Id;
        objInd.Goal_Objective__c = objGoal.Id;
        objInd.Data_Type__c = 'Percent';
        objInd.Reporting_Frequency__c = 'Based on Reporting Frequency';
        insert objInd;
        
        Module__c objIPModule = TestClassHelper.createModule();
        objIPModule.Implementation_Period__c = objIP.Id;
        //objIPModule.Catalog_Module__c = objCM.id;
        objIPModule.Component__c = 'Malaria';
        insert objIPModule;
        
        Grant_Indicator__c objInd1 = TestClassHelper.createGrantIndicator();
        objInd1.Grant_Implementation_Period__c = objIP.Id;
        objInd1.Indicator_Type__c = 'Coverage/Output';
        objInd1.Standard_or_Custom__c = 'Standard';
        objInd1.Parent_Module__c = objIPModule.Id;
        objInd1.Indicator__c = objCatInd.Id;
        insert objInd1;
        
        Grant_Intervention__c objInt = TestClassHelper.createGrantIntervention(objIP);
        objInt.Name = 'TestIntervention';
        objInt.Module__c = objIPModule.Id;
        insert objInt;
        Ind_Goal_Jxn__c objJxn = TestClassHelper.insertIndicatorGoalJxn(objGoal,objInd);
        
        test.startTest();
        
        Period__c objPeriod = TestClassHelper.createPeriod();
        objPeriod.Implementation_Period__c = objIP.Id;
        objPeriod.Start_Date__c = Date.today();
        objPeriod.EFR__c = true;
        objPeriod.EFR_Due_Date__c = system.today();
        objPeriod.PU__c=true;
        objPeriod.PU_Due_Date__c = system.today();
        objPeriod.Audit_Report__c = true;
        objPeriod.AR_Due_Date__c=system.today();
        objPeriod.DR__c=true;
        objPeriod.Type__c = 'Reporting';
        objPeriod.Due_Date__c =system.today();
        objPeriod.Flow_to_GrantIndicator__c = false;
        insert objPeriod;
        /*
        Result__c objResult = new Result__c();
        objResult.Target__c = 67;
        objResult.Target_Numerator__c = 98;
        objResult.Target_Denominator__c = 100;
        objResult.Indicator__c = objInd.id;
        objResult.Period__c = objPeriod.Id;
        objResult.Targetnew__c = '%';
        insert objResult;
        */
        System.runAs(user1) {
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI1 = new TargetForIndicatorExt(sc1);
            clsTFI1.ShowHistoryPopup();
            clsTFI1.HidePopupHistory();
            //clsTFI1.fillPeriods();
        }
        
        System.runAs(user2) {
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI2 = new TargetForIndicatorExt(sc2);
            clsTFI2.ShowHistoryPopup();
            clsTFI2.HidePopupHistory();
        }
        
        System.runAs(user3) {
            ApexPages.Standardcontroller sc3 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI3 = new TargetForIndicatorExt(sc3);
            clsTFI3.ShowHistoryPopup();
            clsTFI3.HidePopupHistory();
        }
        
        System.runAs(user4) {
            ApexPages.Standardcontroller sc4 = new ApexPages.Standardcontroller(objInd);
            ApexPages.currentPage().getParameters().put('Id',objInd.id);
            TargetForIndicatorExt clsTFI4 = new TargetForIndicatorExt(sc4);
            clsTFI4.ShowHistoryPopup();
            clsTFI4.HidePopupHistory();
        }
        
        test.stopTest();
    }
    
}