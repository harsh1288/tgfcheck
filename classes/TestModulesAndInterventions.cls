/*********************************************************************************
* Test class:   TestModulesAndInterventions
  Class: ModulesAndInterventions
*  DateModified : 06/04/2015
* History:
* - VERSION  DEVELOPER NAME             DATE          DETAIL FEATURES
     1.1     TCS(Jaideep Khare - JK01)  06/04/2015    Modified DEVELOPMENT
*********************************************************************************/ 
@isTest
Public Class TestModulesAndInterventions{
    Public static testMethod void TestModulesAndInterventions(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c = 'MOH';
        objAcc.Boolean_Duplicate__c = true;  // JK01
        insert objAcc;
        
        Template__c objTemplate = new Template__c(Name = 'Test Template');
        insert objTemplate;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.Name = 'Test CN';
        objCN.Component__c = 'Malaria';
        objCN.Language__c = 'ENGLISH';
        objCN.CCM_new__c = objAcc.Id;
        objCN.Number_of_Years__c = '4';
        objCN.Page_Template__c = objTemplate.Id;
        objCN.Indicative_Amount__c = 10;
        insert objCN;
                        
        Page__c objPage = new Page__c();
        objPage.Concept_note__c = objCN.Id;
        insert objPage;
        
        Catalog_Module__c objCM = new Catalog_Module__c();
        objCM.Name = 'Test CM';
        insert objCM;
        
        Indicator__c objGi = new Indicator__c();   
        objGi.Indicator_Type__c = 'Coverage/Output';
        objGi.Catalog_Module__c = objCM.id;
        insert objGi;
        
        Catalog_Module__c objCM1 = new Catalog_Module__c();
        objCM1.Name = 'Test CM1';
        objCM1.Component__c = 'Malaria';
        objCM1.component_multi__c = objCN.Component__c;
        insert objCM1;
        
        Module__c objModule = new Module__c();
        objmodule.Name = 'Test';
        objModule.Component__c = 'Malaria';
        objModule.Concept_note__c = objCN.Id;
        objModule.Catalog_Module__c = objCM.id;
        insert objModule;
        test.startTest();
        Module__c objModule1 = new Module__c();
        objmodule1.Name = 'Test';
        objModule1.Component__c = 'Malaria';
        objModule1.Concept_note__c = objCN.Id;
        insert objModule1;
        
        Intervention__c objInt = new Intervention__c();
        objInt.Name = 'Test';
        objInt.Module_rel__c = objModule.Id;
        objInt.Concept_note__c = objCN.Id;
        insert objInt;
        
        Catalog_Intervention__c objCI = new Catalog_Intervention__c();
        objCI.Name = 'Test CI';
        objCI.Catalog_Module__c  = objCM.Id;
        insert objCI;
        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;
        objIP.Concept_Note__c = objCN.id;
        insert objIP;
        
        
        Grant_Intervention__c objGrant_Intervention = TestClassHelper.createGrantIntervention(objIP);
        objGrant_Intervention.Implementation_Period__c = objIP.id;
        objGrant_Intervention.CN_Intervention__c = objInt.id;
        objGrant_Intervention.Standard_or_Custom__c = 'Custom';
        insert objGrant_Intervention;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user1 = new User(Alias = 'standt', Email='standarduser122@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser122@testorg.com');
        test.stopTest();
        
         User user2 = new User(Alias = 'standta', Email='standarduser122@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testingd', LanguageLocaleKey='ru', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12231@testorg.com');
        
        User user3 = new User(Alias = 'stan', Email='standarduser122@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Tes', LanguageLocaleKey='es', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standardus12231@testorg.com');
        
        PageReference pageRef = Page.ModulesAndInterventions;
        pageRef.getParameters().put('Id',objPage.Id);
        Test.setCurrentPageReference(pageRef);
            
        ApexPages.StandardController sc = new ApexPages.StandardController(objPage);
        ModulesAndInterventions objCls = new ModulesAndInterventions(sc);
        
        objCls.strConceptNoteId = objCN.id;
        
        objCls.LoadModules();
        
        objCls.lstCatModules.get(0).isSelected = true;
        
        objCls.AddSelectedModule();
        
        Apexpages.currentpage().getparameters().put('ModuleIndex','0');
        Apexpages.currentpage().getparameters().put('interventionIndex','0');
        Apexpages.currentpage().getparameters().put('PRIndex','0');
        objCls.lstModules[0].childInterventions[0].WrapPrShortNameList[0].EditPRName = objAcc.Id;
        objCls.SaveImplementationPeriod();
        objCls.EditImplementationPeriod();
        objCls.CancelImplementationPeriod();
        objCls.DeleteImplementationPeriod();
        
        Apexpages.currentpage().getparameters().put('parentRowIndex','0');
        objCls.lstModules[0].lstinterventions[0].isSelected = true;
        objCls.AddSelectedIntervention();
        
        Apexpages.currentpage().getparameters().put('ChildEditIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        
        objCls.lstModules[0].childInterventions[0].implementationPeriods[0].isSelected = true;        
        objCls.AddSelectedPR();
        
        Apexpages.currentpage().getparameters().put('ChildCancelIndex','0');
        Apexpages.currentpage().getparameters().put('CancelIndex','0');
        objCls.CancelInterventionEdit();
         
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        objCls.EditModule();
        
        Apexpages.currentpage().getparameters().put('ChildEditIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        objCls.EditGrantIntervention();
        
        Apexpages.currentpage().getparameters().put('SaveIndex','0');
        objCls.SaveModule(); 
        
        String indexModule = String.valueof(objCls.lstModules.size()+1);
        Apexpages.currentpage().getparameters().put('SaveIndex',indexModule);
        objCls.SaveModule();
        
        Apexpages.currentpage().getparameters().put('ChildSaveIndex','0');
        Apexpages.currentpage().getparameters().put('SaveIndex','0');
        objCls.SaveGrantIntervention();
        
        String indexInt = String.valueof(objCls.lstModules[0].childInterventions.size()+1);
        Apexpages.currentpage().getparameters().put('ChildSaveIndex',indexInt);
        Apexpages.currentpage().getparameters().put('SaveIndex','0');
        objCls.SaveGrantIntervention();
        
        
        Apexpages.currentpage().getparameters().put('CancelIndex','0');
        objCls.CancelModule();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','0');
        Apexpages.currentpage().getparameters().put('ChildDeleteIndex','1');       
        objCls.DeleteIntervention();
        
        Apexpages.currentpage().getparameters().put('deleteModuleIndex','1');
        objCls.DeleteModule();
        
        Profile_Access_Setting_CN__c checkProfile = TestClassHelper.createProfileSettingCN();
        checkProfile.Page_Name__c ='ModulesAndInterventions';
        checkProfile.Salesforce_Item__c = 'External Profile';
        checkProfile.Status__c = 'Not yet submitted'; 
        insert checkProfile;
        objCls.checkProfile(); 
    }
    
}