@isTest
public class CrmTestUtil {  
    
    public static RecordType getRecordType(String name, String type) {
        return [SELECT Id FROM RecordType WHERE Name = :name AND SobjectType = :type];
    }
    public static User ADMIN_USER {
        get{
            if ( null == ADMIN_USER ){
                ADMIN_USER = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive=true LIMIT 1 ];
            }
            return ADMIN_USER;
        }
        private set;
    }
    
    public static Affiliation_to_User_Profile_Mapping__c createAffiliationtoUserProfileMapping(String Name, String Profile_ID, String Access_Level, String Record_Type_Name, Integer profilePrecedence, Boolean doInsert )
    {
        Affiliation_to_User_Profile_Mapping__c setting = new Affiliation_to_User_Profile_Mapping__c();
        setting.Name = Name;
        setting.Profile_ID__c = Profile_ID;
        setting.Access_Level__c= Access_Level;
        setting.Profile_Precedence__c = profilePrecedence;
        setting.Record_Type_Name__c= Record_Type_Name;
        if( doInsert )
        {
            System.runAs( ADMIN_USER )
            {
                insert setting;
            }
        }
        return setting;
    }
    public static AffiliationBasedSharingMatrix__c createAffiliationBasedSharingMatrix(String Name, String Access_Level, String Record_Type_Name,String ConceptNoteAccessLevel, String AccountAccessLevel, String ContactAccessLevel_MngContact_False, String ContactAccessLevel_MngContact_True, Boolean doInsert )
    {
        AffiliationBasedSharingMatrix__c setting = new AffiliationBasedSharingMatrix__c();
        setting.Name = Name;
        setting.Access_Level__c = Access_Level;
        setting.Record_Type_Name__c = Record_Type_Name;
        setting.ConceptNoteAccessLevel__c = ConceptNoteAccessLevel;
        setting.AccountAccessLevel__c = AccountAccessLevel;
        setting.ContactAccessLevel_MngContact_False__c = ContactAccessLevel_MngContact_False;
        setting.ContactAccessLevel_MngContact_True__c = ContactAccessLevel_MngContact_True;
        if( doInsert )
        {
            System.runAs( ADMIN_USER )
            {
                insert setting;
            }
        }
        return setting;
    }
    public static Contact insertContact(Id accountId,Boolean doInsert){        
        
        Contact Con = new Contact();
            con.FirstName = 'Test Contact';
            con.External_User__c=true;
            con.LastName = 'Testing';
            Con.email = 'standarduser@testorg.com';
            con.accountid= accountId;
          
        if(doInsert)
            insert con;        
        return con;
    }
    public static Account insertAccount(String name,Boolean doInsert){        
        
        Country__c objCountry = TestClassHelper.insertCountry();
        Account objAcc = new Account();
        objAcc.Name = name;
        objAcc.Short_Name__c = 'MOH';
        objAcc.Country__c = objCountry.Id ;
        if(doInsert)
            insert objAcc;        
        return objAcc;
    }
    public static npe5__Affiliation__c insertAffiliation(Id accountId,Id contactId,String recordTypeId ,String accessLevel,Boolean doInsert){        
        
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = False;
        objAff.Access_Level__c = accessLevel;
        objAff.Recordtypeid = (Id)recordTypeId;
        objAff.npe5__Organization__c  = accountId;
        objAff.npe5__Contact__c  = contactId;
        if(doInsert)
            insert objAff;        
        return objAff;
    }
}