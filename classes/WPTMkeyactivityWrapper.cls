// Purpose     :  This class is used to sort Key Activity records used in WPTMKeyActivityReport Page                    
// Date        :  7 April 2015
// Created By  :  TCS 
// Author      :  Gaurav Guleria
// Description :  This class is used to sort Key Activity records used in WPTMKeyActivityReport Page
// Revision:   :  

global class WPTMkeyactivityWrapper implements Comparable {

    public Key_Activity__c keyactivity;
    // Constructor
    public WPTMkeyactivityWrapper (Key_Activity__c keyact) {
        keyactivity= keyact;
    }
    global Integer compareTo(Object compareTo) {
        WPTMkeyactivityWrapper compareToactvty = (WPTMkeyactivityWrapper )compareTo;
        Integer returnValue = 0;
        if (keyactivity.Grant_Intervention__r.Name > compareToactvty.keyactivity.Grant_Intervention__r.Name) {
            
            returnValue = 1;//keyactivity.Activity_Description__c
        } else if (keyactivity.Grant_Intervention__r.Name < compareToactvty.keyactivity.Grant_Intervention__r.Name) {
           
            returnValue = -1;
        }
        return returnValue;       
    }
}