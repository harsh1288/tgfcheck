@istest
public class testBudgetLineTrigger {
    public static testmethod void testBudgetLineTrigger(){
         Account objact = TestClassHelper.createAccount();
         insert objact;
        
         Grant__c objgrant = TestClassHelper.createGrant(objact);
         insert objgrant;
        
         Implementation_Period__c objimp = TestClassHelper.createIPWithConceptNote(objgrant, objact);
         insert objimp;
        
        Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
        insert objpf;
        
        Module__c objmod = TestClassHelper.createModule();
        objmod.Performance_Framework__c = objpf.id;
        objmod.Implementation_Period__c = objimp.Id;
        insert objmod;
        
        grant_intervention__c objgi = TestClassHelper.createGrantIntervention(objimp);
        objgi.Module__c = objmod.Id;
        objgi.Performance_Framework__c = objpf.Id;
        objgi.Implementation_Period__c = objimp.Id;
        insert objgi;
        
        IP_Detail_Information__c objdb = TestClassHelper.createIPDetail();
        objdb.Implementation_Period__c = objimp.id;
        objdb.Budget_Status__c = 'Submitted to Global Fund';
        objdb.IS_FO_Updated_BL__c = false;
        insert objdb;
        
        Budget_Line__c objbl = TestClassHelper.createBudgetLine();
        objbl.Grant_Intervention__c = objgi.Id;
        objbl.Implementation_Period__c = objimp.Id;
        objbl.trackField__c = Date.today();
        objbl.Budget_Line_ETL_ID__c = '123';
        objbl.Detailed_Budget_Framework__c = objdb.Id;
        insert objbl;
        
        delete objbl;
        
    }
}