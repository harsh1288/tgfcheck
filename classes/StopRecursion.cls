public class StopRecursion{

public static boolean alreadyCreatedTasks = false;
public static boolean alreadyCreatedAccFeed = false;
public static boolean alreadyCreatedBankAcc_LFA_Rev = false;
public static boolean alreadyCreatedImpApproval = false;
public static boolean alreadyCreatedBankApproval = false;
public static boolean alreadyRunAfterInsertCN = false;
public static boolean alreadyRunBeforeInsertCN = false;
public static boolean alreadyRunBeforeUpdateIP = false;
public static boolean alreadyRunAfterUpdateIP = false;
public static boolean alreadyRunRPAfterUpdateIP = false;
public static boolean alreadyRunCPFAfterInsert = false;
public static boolean alreadyRunShareGIPExt = false;

private static boolean alreadyRunBeforeAcc = false;
private static boolean alreadyRunAfterAcc = false; 
private static boolean alreadyRunBeforeInsertAcc = false;
private static boolean alreadyRunBeforeUpdateAcc = false;
private static boolean alreadyRunAfterInsertAcc = false;
private static boolean alreadyRunAfterUpdateAcc = false; 
private static boolean alreadyRunAfterUpdateDB  = false;
private static boolean alreadyRunAfterUpdateDBSubToGF  = false;
private static boolean alreadyRunAfterUpdateDBAccepted  = false;
private static boolean alreadyRunAfterUpdateBL  = false;
private static boolean alreadyRunAfterUpdateAssump  = false;
/*****************************************************
 * adding booleans for trigger recursion control
 * ***************************************************/
  /*****************************************************
     To Stop recursion of Account Trigger
 *****************************************************/
    public static boolean hasAlreadyRunBeforeAcc() 
    {
      return alreadyRunBeforeAcc ;
    }
    public static void setAlreadyRunBeforeAcc() 
    {
        alreadyRunBeforeAcc = true;
    }
    public static boolean hasAlreadyRunAfterAcc() 
    {
      return alreadyRunAfterAcc ;
    }
    public static void setAlreadyRunAfterAcc() 
    {
        alreadyRunAfterAcc = true;
    }
    public static boolean hasAlreadyRunBeforeInsertAcc() 
    {
      return alreadyRunBeforeInsertAcc ;
    }
    public static void setAlreadyRunBeforeInsertAcc() 
    {
        alreadyRunBeforeInsertAcc = true;
    }
    public static boolean hasAlreadyRunBeforeUpdateAcc() 
    {
      return alreadyRunBeforeUpdateAcc ;
    }
    public static void setAlreadyRunBeforeUpdateAcc() 
    {
        alreadyRunBeforeUpdateAcc = true;
    }
    public static boolean hasAlreadyRunAfterInsertAcc() 
    {
      return alreadyRunAfterInsertAcc ;
    }
    public static void setAlreadyRunAfterInsertAcc() 
    {
        alreadyRunAfterInsertAcc = true;
    }
    public static boolean hasAlreadyRunAfterUpdateAcc() 
    {
      return alreadyRunAfterUpdateAcc ;
    }
    public static void setAlreadyRunAfterUpdateAcc() 
    {
        alreadyRunAfterUpdateAcc = true;
    }
 
 /*****************************************************
     To Stop recursion of shareGIPEXt Trigger
 *****************************************************/
     public static boolean hasalreadyRunShareGIPExt() 
    {
      return alreadyRunShareGIPExt ;
    }
    public static void setalreadyRunShareGIPExt() 
    {
        alreadyRunShareGIPExt = true;
    }
    //------------------for Concept Note insert-----
    public static boolean hasalreadyRunAfterInsertCN() 
    {
      return alreadyRunAfterInsertCN;
    }
    public static void setalreadyRunAfterInsertCN() 
    {
        alreadyRunAfterInsertCN = true;
    }
    public static boolean hasalreadyRunBeforeInsertCN() 
    {
      return alreadyRunBeforeInsertCN;
    }
    public static void setalreadyRunBeforeInsertCN() 
    {
        alreadyRunBeforeInsertCN = true;
    }
//------------------for Implementation Period Update-------------------------
    public static boolean hasalreadyRunBeforeUpdateIP() 
    {
      return alreadyRunBeforeUpdateIP;
    }
    public static void setalreadyRunBeforeUpdateIP() 
    {
        alreadyRunBeforeUpdateIP = true;
    }
    public static boolean hasalreadyRunAfterUpdateIP() 
    {
      return alreadyRunAfterUpdateIP;
    }
    public static void setalreadyRunAfterUpdateIP() 
    {
        alreadyRunAfterUpdateIP = true;
    }
    public static boolean hasalreadyRunRPAfterUpdateIP() 
    {
      return alreadyRunRPAfterUpdateIP;
    }
    public static void setalreadyRunRPAfterUpdateIP() 
    {
        alreadyRunRPAfterUpdateIP = true;
    }
//---------------------------------------------------    
    public static boolean hasAlreadyCreatedFollowUpTasks() 
    {
      return alreadyCreatedTasks;
    }
 
    public static void setAlreadyCreatedFollowUpTasks() 
    {
        alreadyCreatedTasks = true;
    }
    
    //--------------------For Account Approval Status Explanation-----------------
    public static boolean hasAlreadyCreatedAccFeed() 
    {
        return alreadyCreatedAccFeed;
    }
    public static void setAlreadyCreatedAccFeed() 
    {
        alreadyCreatedAccFeed = true;
    }
    
    //---------------------For CPF Report Insert----------------------------------------------------------------------
    
    public static boolean hasalreadyRunCPFAfterInsert() 
    {
        return alreadyRunCPFAfterInsert;
    }
    public static void setalreadyRunCPFAfterInsert() 
    {
        alreadyRunCPFAfterInsert = true;
    }
    //-----------------------------------------------------------------
    public static boolean hasAlreadyCreatedBankAcc_LFA_Rev() 
    {
        return alreadyCreatedBankAcc_LFA_Rev;
    }
    public static void setAlreadyCreatedBankAcc_LFA_Rev() 
    {
        alreadyCreatedBankAcc_LFA_Rev = true;
    }
    //--------------------For Implementation Approval Status Explanantion--------------------
     public static boolean hasAlreadyCreatedImpApproval() 
    {
        return alreadyCreatedImpApproval;
    }
    public static void setAlreadyCreatedImpApproval() 
    {
        alreadyCreatedImpApproval = true;
    }
 //------------------------------------------------------------------------
 
  //--------------------For Bank Account Approval Status Explanantion--------------------
     public static boolean hasAlreadyCreatedBankApproval() 
    {
        return alreadyCreatedBankApproval;
    }
    public static void setAlreadyCreatedBankApproval() 
    {
        alreadyCreatedBankApproval = true;
    }
 //-------------------------------------------------------------------------
 //-----------------------------To Stop Recursion for SendMailToPr Trigger---------------------
   public static boolean hasalreadyRunAfterUpdateDB() 
    {
      return alreadyRunAfterUpdateDB;
    }
    public static void setalreadyRunAfterUpdateDB() 
    {
        alreadyRunAfterUpdateDB = true;
        
    }
    //--------------------------------------------------------------------------------------------------
    //-----------------------------To Stop Recursion for SendMailToPr Trigger---------------------
   public static boolean hasalreadyRunAfterUpdateDBSubToGF() 
    {
      return alreadyRunAfterUpdateDBSubToGF;
    }
    public static void setalreadyRunAfterUpdateDBSubToGF() 
    {
        alreadyRunAfterUpdateDBSubToGF = true;
        
    }
    //--------------------------------------------------------------------------------------------------
    //-----------------------------To Stop Recursion for SendMailToPr Trigger---------------------
   public static boolean hasalreadyRunAfterUpdateDBAccepted() 
    {
      return alreadyRunAfterUpdateDBAccepted;
    }
    public static void setalreadyRunAfterUpdateDBAccepted() 
    {
        alreadyRunAfterUpdateDBAccepted = true;
        
    }
    //---------------------------------------For reporting Period-----------------------------------------------------------
    public static boolean alreadyRunBeforeDeleteRP = false;
    public static boolean alreadyRunBeforeInsertRP = false;
    public static boolean alreadyRunBeforeUpdateRP = false;
    
    public static boolean hasalreadyRunBeforeDeleteP() 
    {
      return alreadyRunBeforeDeleteRP;
    }
    public static void setalreadyRunBeforeDeleteRP() 
    {
        alreadyRunBeforeDeleteRP = true;
        
    }
    
    public static boolean hasalreadyRunBeforeUpdateRP() 
    {
      return alreadyRunBeforeUpdateRP;
    }
    public static void setalreadyRunBeforeUpdateRP() 
    {
        alreadyRunBeforeUpdateRP= true;
        
    }
    
    public static boolean hasalreadyRunBeforeInsertRP() 
    {
      return alreadyRunBeforeInsertRP;
    }
    public static void setalreadyRunBeforeInsertRP() 
    {
        alreadyRunBeforeInsertRP= true;
        
    }
    
   //--------------------------------------------------------------------------------------------------
    //-----------------------------To Stop Recursion for BL and Assumption ETL Trigger---------------------
   public static boolean hasalreadyRunAfterUpdateBL() 
    {
      return alreadyRunAfterUpdateBL;
    }
    public static void setalreadyRunAfterUpdateBL() 
    {
        alreadyRunAfterUpdateBL = true;        
    }
    public static boolean hasalreadyRunAfterUpdateAssump() 
    {
      return alreadyRunAfterUpdateAssump;
    }
    public static void setalreadyRunAfterUpdateAssump() 
    {
        alreadyRunAfterUpdateAssump = true;
        
    }
}