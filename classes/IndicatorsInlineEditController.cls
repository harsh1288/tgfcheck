public with sharing class IndicatorsInlineEditController
{
    public List<Grant_Indicator__c> shwIndicatorsList {get;set;}
    public boolean shwmsg{get;set;}
    public boolean saved{get;set;}
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Boolean displayFiscal {get; set;}
    // for switching panels b/w indicator and disaggregation
    public Boolean indEdit{get; set;}
    // for component attribute
    public string targetRecord{get;set;}
    public string pfStatus{get;set;}
    public string dataType{get;set;}
    public string decimalPlaces{get;set;}
    public String indName{get;set;}
    public String pfId;
    public String indicatortype;
    public IndicatorsInlineEditController(ApexPages.StandardSetController standardController)
    {
        indicatortype = 'Impact/Outcome';
        shwmsg = false;
        saved = false;
        indEdit = true;
        pfId = ApexPages.currentPage().getParameters().get('id');        
        shwIndicatorsList = new List<Grant_Indicator__c>([Select Id,Name,Indicator_Full_Name__c,Standard_or_Custom__c,Indicator_Type__c,
                                                          Data_Type__c,Baseline_numerator__c,Baseline_Denominator__c,Baseline_Value1__c,Baseline_Year__c,
                                                          Baseline_Sources__c,Component__c, Target_Value_Y12__c, Target_Value_Y22__c, Target_Value_Y32__c, 
                                                          Target_Value_Y42__c, Decimal_Places__c,Target_Numerator_Y1__c,Target_Numerator_Y2__c,Target_Numerator_Y3__c,
                                                          Target_Numerator_Y4__c,Target_Denominator_Y1__c,Target_Denominator_Y2__c,Target_Denominator_Y3__c,
                                                          Target_Denominator_Y4__c,Y1_Report_Due__c,Y2_Report_Due__c,Y3_Report_Due__c,Y4_Report_Due__c,
                                                          Grant_Implementation_Period__r.Start_Date__c, Grant_Implementation_Period__r.End_Date__c, 
                                                          Grant_Implementation_Period__r.Number_of_fiscal_cycles__c,Performance_Framework__r.Grant_Status__c,(Select Id from Grant_Indicator__c.Grant_Indicator__r) 
                                                          from Grant_Indicator__c 
                                                          where Performance_Framework__c =: pfId and Indicator_Type__c !='Coverage/Output' limit 10000 ]);
        if(!(shwIndicatorsList.size() >0))
        {
            shwmsg= true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No Indicators are available to show.')); 
        }
        else if(shwIndicatorsList.size() > 0){
            startDate = shwIndicatorsList [0].Grant_Implementation_Period__r.Start_Date__c;
            endDate = shwIndicatorsList [0].Grant_Implementation_Period__r.End_Date__c;
            if(shwIndicatorsList [0].Grant_Implementation_Period__r.Number_of_fiscal_cycles__c== '3'){
                displayFiscal = false;
            }
            else{
                displayFiscal = true;
            }
        }
           
    }
    public void saveList()
    {
        List<Grant_Indicator__c> updatedIndicatorsList=new List<Grant_Indicator__c>();
        
        Integer flag = 0;
        Boolean stopSave = false;
        Decimal num;
        Decimal den;
        Integer idx=0;
        for (Grant_Indicator__c validateNumDenFieldsOnIndicatorRecords : shwIndicatorsList )
        {
            idx++;
            num = validateNumDenFieldsOnIndicatorRecords.Baseline_numerator__c;
            den = validateNumDenFieldsOnIndicatorRecords.Baseline_Denominator__c;
            //Check if numerator has proper value, then check corresponding denominator also has proper value, if not set flag and break out of for loop
            if(validateNumDenFieldsOnIndicatorRecords.Data_Type__c == 'Number and Percent')
            {
                if(num!=null)
                {    
                    if (den ==0 || den == null)
                    {
                        flag = 1;
                        stopSave = true;
                        saved = false;
                        shwmsg= true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Line ' + idx + ' - Please fill a non zero value in Baseline Denominator')); 
                        validateNumDenFieldsOnIndicatorRecords.Baseline_Denominator__c.addError('Please fill a non zero value in Baseline Denominator field');
                        //  break;
                    }
                }
                //Check if den has proper value, then check corresponding num also has proper value, if not set flag and break out of for loop
                if(!(den == 0 || den == null))
                {
                    if( num == null )
                    {                 
                        flag = 1;
                        stopSave = true;
                        saved = false;
                        shwmsg= true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Line ' + idx + ' - Please fill a value in Baseline Numerator field')); 
                        validateNumDenFieldsOnIndicatorRecords.Baseline_Numerator__c.addError('Please fill a value in Baseline Numerator field');    
                        // break;
                    }
                }
            }
        }
        
        if(!stopSave)
        {
            for (Grant_Indicator__c addPercentSymbolToIndFields : shwIndicatorsList )
            {
                if(addPercentSymbolToIndFields.Data_Type__c == Label.Percent_DT || addPercentSymbolToIndFields.Data_Type__c == Label.NaP_DT) {
                    if(addPercentSymbolToIndFields.Baseline_Value1__c != null && addPercentSymbolToIndFields.Baseline_Value1__c != '' && (!addPercentSymbolToIndFields.Baseline_Value1__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.Baseline_Value1__c += Label.Percent_symb; 
                    if(addPercentSymbolToIndFields.Target_Value_Y12__c != null && addPercentSymbolToIndFields.Target_Value_Y12__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y12__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.Target_Value_Y12__c += Label.Percent_symb;
                    if(addPercentSymbolToIndFields.Target_Value_Y22__c != null && addPercentSymbolToIndFields.Target_Value_Y22__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y22__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.Target_Value_Y22__c+=  Label.Percent_symb;  
                    if(addPercentSymbolToIndFields.Target_Value_Y32__c != null && addPercentSymbolToIndFields.Target_Value_Y32__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y32__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.Target_Value_Y32__c +=  Label.Percent_symb;
                    if(addPercentSymbolToIndFields.Target_Value_Y42__c != null && addPercentSymbolToIndFields.Target_Value_Y42__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y42__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.Target_Value_Y42__c += Label.Percent_symb;    
                }
                if(addPercentSymbolToIndFields.Data_Type__c == 'Ratio') {
                    if(addPercentSymbolToIndFields.Baseline_Value1__c != null && addPercentSymbolToIndFields.Baseline_Value1__c != '' && (!addPercentSymbolToIndFields.Baseline_Value1__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.Baseline_Value1__c =  addPercentSymbolToIndFields.Baseline_Value1__c+' ' +Label.Ratio_label; 
                    if(addPercentSymbolToIndFields.Target_Value_Y12__c != null && addPercentSymbolToIndFields.Target_Value_Y12__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y12__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.Target_Value_Y12__c = addPercentSymbolToIndFields.Target_Value_Y12__c+' '+  Label.Ratio_label;
                    if(addPercentSymbolToIndFields.Target_Value_Y22__c !=  null && addPercentSymbolToIndFields.Target_Value_Y22__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y22__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.Target_Value_Y22__c = addPercentSymbolToIndFields.Target_Value_Y22__c +' '+Label.Ratio_label;  
                    if(addPercentSymbolToIndFields.Target_Value_Y32__c != null && addPercentSymbolToIndFields.Target_Value_Y32__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y32__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.Target_Value_Y32__c = addPercentSymbolToIndFields.Target_Value_Y32__c+' '+ Label.Ratio_label;
                    if(addPercentSymbolToIndFields.Target_Value_Y42__c != null && addPercentSymbolToIndFields.Target_Value_Y42__c != '' && (!addPercentSymbolToIndFields.Target_Value_Y42__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.Target_Value_Y42__c =addPercentSymbolToIndFields.Target_Value_Y42__c+ ' '+ Label.Ratio_label;
                } 
                updatedIndicatorsList.add(addPercentSymbolToIndFields);
            }
            
            try
            {
                update updatedIndicatorsList;
                //update shwIndicatorsList;
                saved = true;    
            }
            catch(exception ex)
            {
                saved = false;
                shwmsg = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'There was some error in saving records.')); 
            }
        } 
    }
/*  public void EditDissagregation()
    {
        indEdit = false;
    }
    public void ReturnToInd()
    {
        indEdit = true;
    }
*/    
   public PageReference EditDissagregation()
    {
        saveList();
        if(saved ==true)
        {
            indEdit = false;
            PageReference pageRef = new PageReference('/apex/DisaggregationTableEdit?par1='+targetRecord+'&par2='+pfStatus+'&par3='+dataType+'&par4='+decimalPlaces+'&par5='+pfId+'&par6='+indicatortype+'&par7='+indName+'&sourceParam=InlineInd');
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }

    public PageReference  ReturnToInd()
    {
        indEdit = true;
        PageReference pageRef = new PageReference('/apex/IndicatorsInlineEditPage?id='+pfId);
        pageRef.setRedirect(true);
        return pageRef;
    } 
      
}