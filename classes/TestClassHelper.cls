@istest
public class TestClassHelper{
//static Country__c objCountry;
//static User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
//Static Account objAcc;
public static Country__c insertCountry(){
        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Testcountry';
        objCountry.French_Name__c = 'TestFrench';
        objCountry.Russian_Name__c = 'TestRussian';
        objCountry.Spanish_Name__c = 'TestSpanish';
        
        objCountry.FPM__c = objUser.id;
        objCountry.Country_Fiscal_Cycle_Start_Date__c = Date.today();
        objCountry.Country_Fiscal_Cycle_End_Date__c = Date.today()+12;
        insert objCountry;
        return objCountry;
} 
public static Country__c createCountry(){
        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Testcountry';
        objCountry.French_Name__c = 'TestFrench';
        objCountry.Russian_Name__c = 'TestRussian';
        objCountry.Spanish_Name__c = 'TestSpanish';
        
        objCountry.FPM__c = objUser.id;
        objCountry.Country_Fiscal_Cycle_Start_Date__c = Date.today();
        objCountry.Country_Fiscal_Cycle_End_Date__c = Date.today()+12;
        
        return objCountry;
} 

public static Profile_Access_Setting__c createProfileSetting(){
        Profile_Access_Setting__c objProfCust = new Profile_Access_Setting__c();
        objProfCust.Name = 'Record 1';
        objProfCust.Profile_Name__c = 'System Administrator';
        return objProfCust;
}
public static Account insertAccount(){
        Country__c objCountry = TestClassHelper.insertCountry();
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c = 'MOH';
        objAcc.Boolean_Duplicate__c = true;
        objAcc.Country__c = objCountry.Id ;
        insert objAcc;        
        return objAcc;
}
public static Contact insertContact(){    
        Account acc = TestClassHelper.insertAccount();
        Contact con = new Contact (
        AccountId = acc.id,
        // added below required fields as per a custom validation
        External_User__c = true,
        FirstName = 'external',
        Email = 'testexternal@user.com.invalid',
        // added above required fields as per a custom validation
        Boolean_Duplicate__c = true,
        LastName = 'portalTestUser'
        );
        insert con;
        return con;
        }
public static Contact createContact(){
        Account acc = TestClassHelper.insertAccount();
        Contact con = new Contact (
        AccountId = acc.id,
        Boolean_Duplicate__c = true,
        LastName = 'portalTestUser'
        );
        return con;
    }
public static User insertIntUser(){
        Set<String> internalProfile = new Set<String> {'Country Team'};
        Profile p = [select Id from Profile where Name in :internalProfile limit 1];         
        User newUser = new User(
        profileId = p.id,    
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname'
        );
        insert newUser;
        return newUser;
    } 
public static User insertExtUser(){
        Set<String> partnerProfile = new Set<String> {'PR Admin'};
        Profile p = [select Id from Profile where Name in :partnerProfile limit 1];         
        User newUser = new User(
        profileId = p.id,    
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = TestClassHelper.insertContact().Id
        );
        insert newUser;
        return newUser;
    }    
public static User insertExtLFAUser(){
        Set<String> partnerProfile = new Set<String> {'LFA Portal User'};
        Profile p = [select Id from Profile where Name in :partnerProfile limit 1];         
        User newUser = new User(
        profileId = p.id,    
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = TestClassHelper.insertContact().Id
        );
        insert newUser;
        return newUser;
    }    
    public static User createExtUser(){
        Set<String> partnerProfile = new Set<String> {'PR Admin'};
        Profile p = [select Id,Name from Profile where Name in :partnerProfile limit 1];         
        User newUser = new User(
        profileId = p.id, 
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = TestClassHelper.insertContact().Id
        );
        
        return newUser;
    }    
public static Bank__c createBank(){
        Country__c objCountry = TestClassHelper.insertCountry();
        Bank__c bank = new Bank__c();
        bank.Country__c = objCountry.Id ;
        return bank;
}
public static Bank_Account__c createBankAcc(){
        Bank_Account__c bankAcc = new Bank_Account__c();
        return bankAcc;

}
public static Account createAccount(){
        //Country__c objCountry = TestClassHelper.insertCountry();
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c = 'MOH';
        objAcc.Boolean_Duplicate__c = true;
        //objAcc.Country__c = objCountry.Id ;          
        return objAcc;
}
public static Attachment createAttach(){
        
        Attachment attach=new Attachment();     
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        
        
        return attach;
}
public static Template__c insertTemp(){        
        Template__c objTemplate = new Template__c(Name = 'Test Template');
        insert objTemplate;
        return objTemplate;
        }
public static Concept_Note__c createCN(){
        Account a = TestClassHelper.insertAccount();
        Template__c objTemplate =  TestClassHelper.insertTemp(); 
        Program_Split__c objPrgmSplit = TestClassHelper.insertProgramSplit();    
        Concept_Note__c objCN = new Concept_Note__c();        
        objCN.Name = 'Test CN';
        objCN.Component__c = 'Malaria';
        objCN.Language__c = 'ENGLISH';        
        objCN.Number_of_Years__c = '3';
        objCN.Page_Template__c = objTemplate.Id;   
        objCN.Program_Split__c = objPrgmSplit.Id;
        objCN.CCM_New__c = a.Id;
        return objCN;
        }
public static Grant__c createGrant(Account objAcc){        
        Grant__c objGrant = new Grant__c();
        objGrant.Disease_Component__c =  'Malaria';
        objGrant.Name = 'Test';
        objGrant.Principal_Recipient__c = objAcc.id;               
        return objGrant;
}
public static Grant__c insertGrant(Account objAcc){        
        Grant__c objGrant = new Grant__c();
        objGrant.Disease_Component__c =  'Malaria';
        objGrant.Name = 'Test';
        objGrant.Principal_Recipient__c = objAcc.id;
        insert objGrant;
        return objGrant;
}

public static Implementation_Period__c createIP(Grant__c objGrant,Account objAcc){
        /*Grant__c objGrant = TestClassHelper.insertGrant();
        Account objAcc = TestClassHelper.insertAcc();*/
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Name = 'NGA-M-UNDP';
        objIP.Principal_Recipient__c = objAcc.Id;
        objIP.Grant__c = objGrant.Id;
        objIP.Start_Date__c =Date.today();
        objIP.End_Date__c =Date.today().addYears(3);
        objIP.Length_Years__c ='3';
           
        return objIP;
}
public static Implementation_Period__c createIPWithConceptNote(Grant__c objGrant,Account objAcc){
        /*Grant__c objGrant = TestClassHelper.insertGrant();
        Account objAcc = TestClassHelper.insertAcc();*/
        Concept_Note__c cn = TestClassHelper.CreateCN();
        cn.Concept_Note_Type__c = 'Regional';
        insert cn;
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Name = 'NGA-M-UNDP';
        objIP.Principal_Recipient__c = objAcc.Id;
        objIP.Grant__c = objGrant.Id;
        objIP.Start_Date__c =Date.today();
        objIP.End_Date__c =Date.today().addYears(3);
        objIP.Length_Years__c ='3';
        objIP.Concept_Note__c = cn.Id;
        return objIP;
}
public static Performance_Framework__c createPF(Implementation_Period__c objIP){
    Performance_Framework__c objPF = new Performance_Framework__c();
    objPF.Implementation_Period__c = objIP.Id;
    return objPF;
}

public static Module__c createModule(){
        Module__c ObjModule = new Module__c();
        ObjModule.Name = 'Test';
        ObjModule.CurrencyIsoCode = 'EUR';
           
        return ObjModule;
}
public static Period__c insertPeriod(){
        Period__c ObjRP = new Period__c();
        ObjRP.Country__c = 'Test';
        insert ObjRP;
        return ObjRP;

}
public static Period__c createPeriod(){
        Period__c ObjRP = new Period__c();
        ObjRP.Country__c = 'Test';
        return ObjRP;

}
public static Implementer__c insertImplementor(){
        Implementer__c ObjImplementer = new Implementer__c();
        ObjImplementer.Implementer_Name__c = 'Test';       
        insert ObjImplementer;
        return ObjImplementer;
}

public static Implementer__c createImplementor(){
        Implementer__c ObjImplementer = new Implementer__c();
        ObjImplementer.Implementer_Name__c = 'Test';       
        return ObjImplementer;
}

public static Page__c createPage(){
        //Implementation_Period__c objIP = TestClassHelper.insertIP();
        Page__c objPage = new Page__c();
        Objpage.Name = 'Test';
        Objpage.Status__c= 'Draft';
        //ObjPage.Implementation_Period__c = objIP.Id;        
        return objPage;
}
public static Catalog_Module__c createCatalogModule(){
        Catalog_Module__c objCM = new Catalog_Module__c();
        objCM.Name = 'Test CM';
        return objCM;
}
public static Grant_Intervention__c createGrantIntervention(Implementation_Period__c objIP){
        //Implementation_Period__c objIP = TestClassHelper.insertIP();
        Grant_Intervention__c ObjGrantIntervention = new Grant_Intervention__c ();
        ObjGrantIntervention.Implementation_Period__c = objIP.id;        
        return ObjGrantIntervention;
}
public static Catalog_Cost_Input__c insertCostInput(){
        Catalog_Cost_Input__c ObjCatalogCostInput = new Catalog_Cost_Input__c();
        ObjCatalogCostInput.Name = '8.2 Renovation/constructions';
        ObjCatalogCostInput.GIS_ID__c = 46;
        ObjCatalogCostInput.CurrencyIsoCode = 'EUR'; 
        ObjCatalogCostInput.PSM__c = FALSE;
        ObjCatalogCostInput.Cost_Grouping__c = '1. Human Resources (HR)';
        insert ObjCatalogCostInput;
        return ObjCatalogCostInput;
}

public static Catalog_Cost_Input__c createCostInput(){
        Catalog_Cost_Input__c ObjCatalogCostInput = new Catalog_Cost_Input__c();
        ObjCatalogCostInput.Name = '8.2 Renovation/constructions';
        ObjCatalogCostInput.GIS_ID__c = 46;
        ObjCatalogCostInput.CurrencyIsoCode = 'EUR'; 
        //ObjCatalogCostInput.Cost_Grouping__c = '1. Human Resources (HR)';
        
        return ObjCatalogCostInput;
}
public static Budget_Line__c createBudgetLine(){
        //Catalog_Cost_Input__c ObjCatalogCostInput = TestClassHelper.insertCostInput();
        //Grant_Intervention__c ObjGrantIntervention = TestClassHelper.insertGrantIntervention();
        //Implementer__c ObjImplementer = TestClassHelper.insertImplementor();
        Budget_Line__c ObjBudgetLine = new  Budget_Line__c();
        ObjBudgetLine.CurrencyIsoCode = 'EUR';                   
        return ObjBudgetLine;
}
public static Catalog_Intervention__c createCatalogIntervention(){
        Catalog_Intervention__c objCI = new Catalog_Intervention__c();
        objCI.Name = 'Test CI';
        return objCI; 
}
public static Goals_Objectives__c createGoalsObjectives() {
        Goals_Objectives__c objGoal = new Goals_Objectives__c();
        objGoal.Goal__c = 'Test Goal';        
        objGoal.Type__c = 'Goal';
        objGoal.Number__c = 1;        
        return objGoal;
}

public static Grant_Indicator__c createGrantIndicator(){        
        Grant_Indicator__c objIndicator = new Grant_Indicator__c();
        objIndicator.Indicator_Full_Name__c = 'Test Indicator';
        objIndicator.Data_Type__c = 'Percent';
        objIndicator.Baseline_numerator__c = 20;
        objIndicator.Baseline_Denominator__c = 20;
        objIndicator.Baseline_Value1__c = '100';
        objIndicator.Target_Value_Y12__c = '10';
        objIndicator.Target_Value_Y22__c = '20';
        objIndicator.Target_Value_Y32__c = '30';
        objIndicator.Target_Value_Y42__c = '40';
        objIndicator.decimal_Places__c = '2';
        
        objIndicator.decimal_Places__c = '2';
        objIndicator.Baseline_Percent__c = 2;
        objIndicator.Baseline_Value__c = 10;
        objIndicator.Target_Denominator_Y1__c = 10;
        objIndicator.Target_Denominator_Y2__c = 10;
        objIndicator.Target_Denominator_Y3__c = 10;
        objIndicator.Target_Denominator_Y4__c = 12;
        
        objIndicator.Target_Numerator_Y1__c = 10;
        objIndicator.Target_Numerator_Y2__c = 10;
        objIndicator.Target_Numerator_Y3__c = 10;
        objIndicator.Target_Numerator_Y4__c = 10;
        
        objIndicator.Target_Value_Y1__c = 10;
        objIndicator.Target_Value_Y2__c = 10;
        objIndicator.Target_Value_Y3__c = 10;
        objIndicator.Target_Value_Y4__c = 10;
        return objIndicator;
}
public static Grant_Indicator__c insertGrantIndicator(){        
        Grant_Indicator__c objIndicator = new Grant_Indicator__c();
        objIndicator.Indicator_Full_Name__c = 'Test Indicator';
        objIndicator.Data_Type__c = 'Percent';
        insert objIndicator;
        return objIndicator;
}
public static Indicator__c createCatalogIndicator(){
        Indicator__c ind = new Indicator__c();
        Catalog_Intervention__c ci = TestClassHelper.createCatalogIntervention();
        insert ci;
        Catalog_Module__c cm = TestClassHelper.createCatalogModule();
        insert cm;
        ind.Catalog_Intervention__c = ci.Id;
        ind.Catalog_Module__c = cm.Id;
        ind.Grant__c = TestClassHelper.insertGrant(TestClassHelper.insertAccount()).Id;
        ind.Component__c = 'Malaria';
        ind.Indicator_Type__c = 'Impact';
        ind.Type_of_Data__c = 'Percent';
        ind.Full_Name_En__c = 'Test Cat Indicator';
        return ind;
}
public static Indicator__c insertCatalogIndicator(){
        Indicator__c ind = new Indicator__c();
        Catalog_Intervention__c ci = TestClassHelper.createCatalogIntervention();
        insert ci;
        Catalog_Module__c cm = TestClassHelper.createCatalogModule();
        insert cm;
        ind.Catalog_Intervention__c = ci.Id;
        ind.Catalog_Module__c = cm.Id;
        ind.Grant__c = TestClassHelper.insertGrant(TestClassHelper.insertAccount()).Id;
        ind.Programme_Area__c = 'Malaria';
        ind.Indicator_Type__c = 'Impact';
        ind.Type_of_Data__c = 'Percent';
        ind.Full_Name_En__c = 'Test Cat Indicator';
        insert ind;
        return ind;
}
public static Catalog_Disaggregated__c createCatalogDisaggregated(){
        Catalog_Disaggregated__c cd = new Catalog_Disaggregated__c();
        cd.Catalog_Indicator__c = TestClassHelper.insertCatalogIndicator().Id;
        return cd;
    }
public static Catalog_Disaggregated__c insertCatalogDisaggregated(){
        Catalog_Disaggregated__c cd = new Catalog_Disaggregated__c();
        cd.Catalog_Indicator__c = TestClassHelper.insertCatalogIndicator().Id;
        insert cd;
        return cd;
    }
public static Grant_Disaggregated__c createGrantDisaggregated(){
        Grant_Disaggregated__c gd = new Grant_Disaggregated__c();
        gd.Grant_Indicator__c = TestClassHelper.insertGrantIndicator().Id;
        gd.Catalog_Disaggregated__c = TestClassHelper.insertCatalogDisaggregated().Id;
        Implementation_Period__c ip = TestClassHelper.createIPWithConceptNote(TestClassHelper.insertGrant(TestClassHelper.insertAccount()),TestClassHelper.insertAccount());
        insert ip;
        Performance_Framework__c pf = TestClassHelper.createPF(ip);
        insert pf;
        gd.Performance_Framework__c = pf.Id;
        return gd;
}
public static Grant_Disaggregated__c insertGrantDisaggregated(){
        Grant_Disaggregated__c gd = new Grant_Disaggregated__c();
        gd.Grant_Indicator__c = TestClassHelper.insertGrantIndicator().Id;
        gd.Catalog_Disaggregated__c = TestClassHelper.insertCatalogDisaggregated().Id;
        Implementation_Period__c ip = TestClassHelper.createIPWithConceptNote(TestClassHelper.insertGrant(TestClassHelper.insertAccount()),TestClassHelper.insertAccount());
        insert ip;
        Performance_Framework__c pf = TestClassHelper.createPF(ip);
        insert pf;
        gd.Performance_Framework__c = pf.Id;
        insert gd;
        return gd;
}
public static Grant_Disaggregated__c createGrantDisaggregatedWithoutGI(){
        Grant_Disaggregated__c gd = new Grant_Disaggregated__c();
        gd.Catalog_Disaggregated__c = TestClassHelper.insertCatalogDisaggregated().Id;
        Implementation_Period__c ip = TestClassHelper.createIPWithConceptNote(TestClassHelper.insertGrant(TestClassHelper.insertAccount()),TestClassHelper.insertAccount());
        insert ip;
        Performance_Framework__c pf = TestClassHelper.createPF(ip);
        insert pf;
        gd.Performance_Framework__c = pf.Id;
        return gd;
}
public static Ind_Goal_Jxn__c insertIndicatorGoalJxn(Goals_Objectives__c objGoal,Grant_Indicator__c objIndicator) { 
        Ind_Goal_Jxn__c objJun = new Ind_Goal_Jxn__c();
        objJun.Goal_Objective__c = objGoal.Id;
        objJun.Indicator__c = objIndicator.Id;
        insert objJun;
        return objJun;
}
public static Project_multi_lingual__c createProjMultiLanguage() {
        Project_multi_lingual__c objPs = new Project_multi_lingual__c();
        objPs.English_Text__c = 'Concept Notes';
        objPs.French_Text__c = 'Les notes succinctes';
        objPs.Spanish_Text__c = 'documentos de síntesis';
        objPs.Group_Name__c = 'GoalsAndImpactIndicators';
        objPs.Key__c = 'GIConceptNotes';
        return objPs;
}
public static Program_Split__c insertProgramSplit(){
        Program_Split__c objPrgmSplit = new Program_Split__c();
        Account objAcc = TestClassHelper.insertAccount();
        objPrgmSplit.CCM__c = objAcc.Id;
        insert objPrgmSplit;
        return objPrgmSplit;    
}
public static Profile_Access_Setting_CN__c createProfileSettingCN(){        
        Profile_Access_Setting_CN__c checkProfile = new Profile_Access_Setting_CN__c();
        checkProfile.Name = 'Record 1';
        checkProfile.Profile_Name__c = 'System Administrator';
        return checkProfile;
}
public static Guidance__c createGuidance(){
       Guidance__c guidance = new Guidance__c();
       guidance.English_Guidance__c = 'Test';
       return guidance;
}

public static HPC_Framework__c createHPC(){
       HPC_Framework__c HPC = new HPC_Framework__c();
       return HPC;
}
public static IP_Detail_Information__c createIPDetail(){
       IP_Detail_Information__c objIP = new IP_Detail_Information__c();
       return objIP;
}

public static MultipleDataType__c createMultipleDataType() {
    MultipleDataType__c objMDT = new MultipleDataType__c();
    objMDT.Indicator_Name__c = 'Test indicator';
    objMDT.Cumulation_for_AFD__c = 'Non cumulated';
    objMDT.Reason__c = 'Test Reason';
    return objMDT;
}
public static MultipleDataType__c createMultipleDataType(Id catalogIndicatorId) {
    MultipleDataType__c objMDT;
    objMDT= new MultipleDataType__c();
    objMDT.Indicator_Name__c = 'Test indicator';
    objMDT.Catalog_Indicator__c = catalogIndicatorId;
    objMDT.Data_Type__c = 'Percent';
    objMDT.Cumulation_for_AFD__c = 'Non cumulated';
    objMDT.Reason__c = 'Test Reason';
    return objMDT;
}
public static Goals_Objectives__c createGoalsAndObjective(String goalOrObj, id implementationId) {
    Goals_Objectives__c objGoalAndObj = new Goals_Objectives__c();
    objGoalAndObj.Implementation_Period__c = implementationId;
    objGoalAndObj.Goal__c = 'test Goal';
    objGoalAndObj.Type__c = goalOrObj;
    return objGoalAndObj;
}

public static Ind_Goal_Jxn__c createGoalsJunction(id indicatorId, Id GoalOrObjId) {
    Ind_Goal_Jxn__c objGoalJunction = new Ind_Goal_Jxn__c();
    objGoalJunction.Indicator__c = indicatorId;
    objGoalJunction.Goal_Objective__c = GoalOrObjId;
    return objGoalJunction;
}
public static Contact insertContact(Boolean doInsert){    
        Account acc = TestClassHelper.insertAccount();
        Contact con = new Contact (
        AccountId = acc.id,
        Boolean_Duplicate__c = true,
        LastName = 'portalTestUser'
        );
        if(doInsert)
        insert con;
        return con;
        }
public static Reporting_Period_Detail__c createReportingDetailPeriod(Id GIPId, Boolean status) {
    Reporting_Period_Detail__c detailObj = new Reporting_Period_Detail__c();
    detailObj.Is_Active__c = status;
    detailObj.Grant_Implementation_Period__c = GIPId;
    detailObj.PR_Reporting_Cycle__c = 'Yearly';
    detailObj.Reporting_Frequency__c=  'April-March';
    detailObj.Implementation_Period_Start_Date__c = System.today();
    detailObj.Implementation_Period_End_Date__c = System.today().addDays(45);
    
    return detailObj;
}
public static Result__c createResult(Period__c rp, id indicatorId) {
    Result__c rs = new Result__c(target__c =0, Period__c = rp.id, indicator__c =indicatorId );
    return rs;
}

public static Grant_Multi_Country__c createGrntMultiCountry(Id countryId, id GIPId) {
    Grant_Multi_Country__c GMC = new Grant_Multi_Country__c (Country__c= countryId,Grant_Implementation_Period__c = GIPId);
    return GMC ;
}
/*
public static Key_Activity__c createKeyActivity(Grant_Intervention__c  gi){
    Key_Activity__c act = new Key_Activity__c (activity_title__c ='Test Activity', Grant_Intervention__c =gi.id);
    return act;        
}

public static Milestone_Target__c createMilestone(Key_Activity__c activity){
    Milestone_Target__c milestone = new Milestone_Target__c(milestoneTitle__c = 'Test Milestone', key_Activity__c = activity.id);
    return milestone;
}*/
}