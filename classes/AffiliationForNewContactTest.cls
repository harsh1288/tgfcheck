@isTest(seealldata=false)

public class AffiliationForNewContactTest{
 
        private static RecordType rtAccountPR;
        private static RecordType rtAccountCM;
        private static RecordType rtContactPR;
        private static RecordType rtContactCM;
        private static RecordType rtAffiliationPR;
        private static RecordType rtAffiliationCM;    
        
public static testMethod void AffiliationForNewContactTest(){
        

        
        List<RecordType> lstRT = [Select id,name, developername, sobjectType from RecordType where developerName in ('Applicant_Coordinating_Mechanism', 'LFA', 'Board','CM_Affiliation', 'PR_Affiliation','PR', 'Board_Affiliation', 'LFA_Affiliation','External_Affiliation', 'External_Organization') AND SobjectType in ('Contact','Account', 'npe5__Affiliation__c')];
        
            for(RecordType objRT: lstRT){
            if(objRT.sobjectType == 'Account'){
                if(objRT.developername == 'PR'){
                    rtAccountPR = objRT;            
                }
                if(objRT.developername == 'Applicant_Coordinating_Mechanism'){
                    rtAccountCM = objRT;            
                }

            }
            if(objRT.sobjectType == 'Contact'){
                if(objRT.developername == 'PR'){
                    rtContactPR = objRT;            
                }
                if(objRT.developername == 'Applicant_Coordinating_Mechanism'){
                    rtContactCM = objRT;            
                }
            }
            if(objRT.sobjectType == 'npe5__Affiliation__c'){
                if(objRT.developername == 'PR_Affiliation'){
                    rtAffiliationPR = objRT;            
                }
                if(objRT.developername == 'CM_Affiliation'){
                    rtAffiliationCM = objRT;            
                }
            }
        }
        
        Assigning_RT_to_Affiliation__c objAssignRT = new Assigning_RT_to_Affiliation__c();
        objAssignRT.Name = rtAccountPR.Id;
        objAssignRT.Account_RT__c = 'PR';
        objAssignRT.Account_RT_Id__c = rtAccountPR.Id;
        objAssignRT.Active__c = true;
        objAssignRT.Affiliation_RT__c = 'PR Affiliation';
        objAssignRT.Affiliation_RT_Id__c = rtAffiliationPR.Id;
        insert objAssignRT;
        
        Account Acc = TestClassHelper.createAccount();
        Acc.Name = 'ANewName';
        Acc.recordtypeid  = rtAccountPR.id;
        insert Acc;

        Contact Con = TestClassHelper.createContact();
        con.LastName = 'TestingCRM';
        Con.email = 'standarduser@testorg.com';
        con.accountid= Acc.id;
        con.recordtypeid = rtContactPR.id;
        insert con;
        
        System.debug('Acc '+Acc+' con '+Con);
        
        npe5__Affiliation__c objAff = [SELECT id, RecordTypeId, npe5__Status__c from npe5__Affiliation__c WHERE npe5__Organization__r.Id =: Acc.Id AND npe5__Contact__r.Id =: Con.Id LIMIT 1];
        
        System.debug('objAff'+objAff+'  rtAffiliationPR'+ rtAffiliationPR);
        System.assertEquals(objAff.Id!=Null, True);
        //System.assertEquals(objAff.RecordTypeId == rtAffiliationPR.Id, True);
        }
  
public static testMethod void AffiliationForUpdatedContactTest(){
        
        List<RecordType> lstRT = [Select id,name, developername, sobjectType from RecordType where developerName in ('Applicant_Coordinating_Mechanism','External_Contact', 'LFA', 'PR', 'Board','CM_Affiliation', 'PR_Affiliation','Applicant_Coordinating_Mechanism','PR', 'Board_Affiliation', 'LFA_Affiliation', 'Board', 'LFA', 'External_Affiliation', 'External_Organization') AND SobjectType in ('Contact', 'Account', 'npe5__Affiliation__c')];
        //RecordType ExternalAccRT = [Select id,name from RecordType where Name=: 'External Organization' AND SobjectType=: 'Account'];
        
        for(RecordType objRT: lstRT){
            if(objRT.sobjectType == 'Account'){
                if(objRT.developername == 'PR'){
                    rtAccountPR = objRT;            
                }
                if(objRT.developername == 'Applicant_Coordinating_Mechanism'){
                    rtAccountCM = objRT;            
                }

            }
            if(objRT.sobjectType == 'Contact'){
                if(objRT.developername == 'PR'){
                    rtContactPR = objRT;            
                }
                if(objRT.developername == 'Applicant_Coordinating_Mechanism'){
                    rtContactCM = objRT;            
                }
            }
            if(objRT.sobjectType == 'npe5__Affiliation__c'){
                if(objRT.developername == 'PR_Affiliation'){
                    rtAffiliationPR = objRT;            
                }
                if(objRT.developername == 'CM_Affiliation'){
                    rtAffiliationCM = objRT;            
                }
            }
        }
        
        
        Account a = TestClassHelper.createAccount();
        a.Name = 'Partner Organisation';
        a.Short_Name__c = 'Name'; 
        insert a;

        Account accountForCRM = TestClassHelper.createAccount();
        accountForCRM.Name = 'TestAccountForCRMUpd';
        accountForCRM.Short_Name__c = 'CRM';
        accountForCRM.recordtypeid  = rtAccountCM.Id;
        insert accountForCRM;
        
        Account Acc = TestClassHelper.createAccount();
        Acc.Name = 'ANewName';
        Acc.recordtypeid  = rtAccountPR.id;
        insert Acc;

        Contact Con = TestClassHelper.createContact();
        con.LastName = 'TestingCRM';
        Con.email = 'standarduser@testorg.com';
        con.accountid= Acc.id;
        con.recordtypeid = rtContactPR.id;
        insert con;
        
        Con.accountid= accountForCRM.id;
        update Con;
        
        System.debug('Con'+Con);
        
        npe5__Affiliation__c objAffNew = [SELECT id, RecordTypeId, npe5__Status__c from npe5__Affiliation__c WHERE npe5__Organization__r.Id =: accountForCRM.id AND npe5__Contact__r.Id =: Con.Id LIMIT 1];
        npe5__Affiliation__c objAffUpd = [SELECT id, RecordTypeId, npe5__Status__c from npe5__Affiliation__c WHERE npe5__Organization__r.Id =: Acc.id AND npe5__Contact__r.Id =: Con.Id LIMIT 1];
         
        System.debug('objAffNew '+objAffNew+' objAffUpd '+objAffUpd);    
    
        System.assertEquals(objAffNew.Id!=Null, True);
        //System.assertEquals(objAffUpd.npe5__Status__c == 'Former', True);
}


}