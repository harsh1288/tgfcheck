public with sharing class GMbudgetTableext_clone {

 
 public transient List<BLModule> lstBLModule {get;set;} 
 public transient List<BLModule> lstBLModuleByModule {get;set;}
 public transient List<BLModule> lstBLHPCModuleByModule {get;set;}
 public string strGIPId{get;set;}
 public transient List<BLCost> lstBLCost {get;set;}  
 public transient List<BLPayee> lstBLPayee {get;set;}  
 private map<String, BLModule> moduleWrapMap;
 public string strRecipient{get;set;} 
  
Public Integer TotalModuleY1Cost {get;set;}
Public Integer TotalModuleY2Cost {get;set;}
Public Integer TotalModuleY3Cost {get;set;}
Public Integer TotalModuleY4Cost {get;set;}
Public Integer AllYearModuleTotalCost {get;set;}

Public Integer TotalY1Cost {get;set;}
Public Integer TotalY2Cost {get;set;}
Public Integer TotalY3Cost {get;set;}
Public Integer TotalY4Cost {get;set;}
Public Integer AllYearTotalCost {get;set;}

Public Integer TotalPayeeY1Cost {get;set;}
Public Integer TotalPayeeY2Cost {get;set;}
Public Integer TotalPayeeY3Cost {get;set;}
Public Integer TotalPayeeY4Cost {get;set;}
Public Integer AllYearPayeeTotalCost {get;set;}
public IP_Detail_Information__c budgetId {get;set;}
private HPC_Framework__c framework;
public string frameworkName{get;set;}

Public String BudgetName {get;set;}
 
    String profileName;
    public Boolean blnExternalPro {get; set;}
    
    public GMbudgetTableext_clone(ApexPages.StandardController controller) {
    //try{   
    //getting Implementation Period Id
    strGIPId = Apexpages.currentpage().getparameters().get('id');
    strRecipient = 'Recipient';
    
    BudgetId = [Select id,Name,Implementation_Period__c,Implementation_Period__r.start_date__c,Implementation_Period__r.end_date__c,Implementation_Period__r.Name,Implementation_Period__r.Year_4__c,Implementation_Period__r.Year_1__c,Implementation_Period__r.Year_2__c,Implementation_Period__r.Year_3__c from IP_Detail_Information__c where Implementation_Period__c =:strGIPId];
    BudgetName = BudgetId.Name;
    System.debug('----'+BudgetId.Id);
    
    framework = [SELECT id,Name FROM HPC_Framework__c WHERE Grant_Implementation_Period__c =: strGIPId];
    frameworkName = framework.Name;
    
    
    //System.debug('----'+BudgetId.Id+'--frameworkId-'+frameworkId);
    //getting budget by module and intervention Name
    List<AggregateResult> lstBudgetLineModule = [Select Grant_Intervention__r.Module__r.Name Modulename,Grant_Intervention__r.Name Interventionname, Sum(Y1_Grant_Amount__c) ModuleY1,Sum(Y2_Grant_Amount__c) ModuleY2,Sum(Y3_Grant_Amount__c) ModuleY3,Sum(Y4_Grant_Amount__c) ModuleY4 From Budget_Line__c WHERE Grant_Intervention__r.Implementation_Period__c =: strGIPId AND Detailed_Budget_Framework__c=: BudgetId.id Group By Grant_Intervention__r.Module__r.Name,Grant_Intervention__r.Name];
    
    List<AggregateResult> lstHPCModule = [Select Grant_Intervention__r.Module__r.Name Modulename, Grant_Intervention__r.Name Interventionname, Sum(Cost_Y1__c) ModuleY1,Sum(Cost_Y2__c) ModuleY2,Sum(Cost_Y3__c) ModuleY3,Sum(Cost_Y4__c) ModuleY4 From Product__c WHERE Grant_Intervention__r.Implementation_Period__c =: strGIPId AND Health_Products_and_Costs__r.Budget_Framework_ID__c =: BudgetId.id Group By Grant_Intervention__r.Module__r.Name,Grant_Intervention__r.Name];
    System.debug('-lstBudgetLineHPCGroupByModule-'+lstHPCModule);
    moduleWrapMap = new map<String, BLModule>();
    
    /***************** Cost Grouping ********************/ 
    //Budget grouping by costgrouping from Detailed Budget
    List<AggregateResult> lstBudgetLineCost = [Select Cost_Input__r.Cost_Grouping__c CostGrouping,Sum(Y1_Grant_Amount__c) CostY1,Sum(Y2_Grant_Amount__c) CostY2,Sum(Y3_Grant_Amount__c) CostY3,Sum(Y4_Grant_Amount__c) CostY4 From Budget_Line__c Where Cost_Input__r.Cost_Grouping__c != null And Grant_Intervention__r.Implementation_Period__c =: strGIPId AND Detailed_Budget_Framework__c=: BudgetId.id Group By Cost_Input__r.Cost_Grouping__c Order by Cost_Input__r.Cost_Grouping__c];
    
    //Budget grouping by costgrouping from HPC
    List<AggregateResult> lstBudgetLineCostHPC = [Select Cost_Input__r.Cost_Grouping__c CostGrouping,Sum(Cost_Y1__c) CostY1,Sum(Cost_Y2__c) CostY2,Sum(Cost_Y3__c) CostY3,Sum(Cost_Y4__c) CostY4 From Product__c Where Cost_Input__r.Cost_Grouping__c != null And Grant_Intervention__r.Implementation_Period__c =: strGIPId AND Health_Products_and_Costs__r.Budget_Framework_ID__c =: BudgetId.id Group By Cost_Input__r.Cost_Grouping__c Order by Cost_Input__r.Cost_Grouping__c];
    
    /***************** Recepient Grouping ********************/ 
    //Budget Grouping by recipient
    List<AggregateResult> lstBudgetLinePayee = [Select Payee__c PayeeId,Sum(Y1_Grant_Amount__c) PayeeY1,Sum(Y2_Grant_Amount__c) PayeeY2,Sum(Y3_Grant_Amount__c) PayeeY3,Sum(Y4_Grant_Amount__c) PayeeY4 From Budget_Line__c where Payee__c != null And Grant_Intervention__r.Implementation_Period__c =: strGIPId AND Detailed_Budget_Framework__c=: BudgetId.id Group By Payee__c ];
 
   //HPC Grouping by recipient
    List<AggregateResult> lstHPCPayee = [Select Implementer__c recepientId, Sum(Cost_Y1__c) PayeeY1,Sum(Cost_Y2__c) PayeeY2,Sum(Cost_Y3__c) PayeeY3,Sum(Cost_Y4__c) PayeeY4 From Product__c where Implementer__c != null And Grant_Intervention__r.Implementation_Period__c =: strGIPId AND Health_Products_and_Costs__r.Budget_Framework_ID__c =: BudgetId.id Group By Implementer__c ];
       
    Set<Id> setPayeeIds = new Set<Id>();
    for(AggregateResult objAgg : lstBudgetLinePayee){
        setPayeeIds.add((Id)objAgg.get('PayeeId'));    
    }
    Set<Id> recepientIds = new Set<Id>();
    for(AggregateResult objAgg : lstHPCPayee){
        recepientIds.add((Id)objAgg.get('recepientId'));    
    }
    setPayeeIds.addAll(recepientIds);
    
    
    Map<Id,Implementer__c> mapImplementer = new Map<Id,Implementer__c>([Select Id,Implementer_Name__c From Implementer__c Where Id IN : setPayeeIds]);
    System.debug('--mapImplementer-'+mapImplementer);
     
         
    lstBLModuleByModule = new List<BLModule>();
    lstBLHPCModuleByModule = new List<BLModule>();
    
        
    Map<String, List<BLModule>> BLModuleMap = new Map<String, List<BLModule>>();    
    
    List<BLModule> moduleList = new List<BLModule>();
    for( AggregateResult result : lstBudgetLineModule ){
        
        string moduleName = String.valueof(result.get('Modulename'));
        string interventionName = String.valueof(result.get('Interventionname'));
        if( !BLModuleMap.containsKey(moduleName) ){
            BLModuleMap.put(moduleName, new List<BLModule>());
            moduleList = new List<BLModule>();
            moduleList.add( new BLModule(moduleName,interventionName, false,
                             Integer.valueof(result.get('ModuleY1')), Integer.valueof(result.get('ModuleY2')),
                             Integer.valueof(result.get('ModuleY3')),Integer.valueof(result.get('ModuleY4') ) ));
            BLModuleMap.put(moduleName, moduleList);
        }else{
            moduleList = BLModuleMap.get(moduleName);
            moduleList.add( new BLModule(moduleName,interventionName, false,
                             Integer.valueof(result.get('ModuleY1')), Integer.valueof(result.get('ModuleY2')),
                             Integer.valueof(result.get('ModuleY3')),Integer.valueof(result.get('ModuleY4') ) ));
            BLModuleMap.put(moduleName, moduleList);
        }       
    }
    moduleList = new List<BLModule>();
    for( AggregateResult result : lstHPCModule ){
        
        string moduleName = String.valueof(result.get('Modulename'));
        string interventionName = String.valueof(result.get('Interventionname'));
        if( !BLModuleMap.containsKey(moduleName) ){
            BLModuleMap.put(moduleName, new List<BLModule>());
            moduleList = new List<BLModule>();
            moduleList.add( new BLModule(moduleName,interventionName, false,
                             Integer.valueof(result.get('ModuleY1')), Integer.valueof(result.get('ModuleY3')),
                             Integer.valueof(result.get('ModuleY3')),Integer.valueof(result.get('ModuleY4') ) ));
            BLModuleMap.put(moduleName, moduleList);
        }else{
            moduleList = BLModuleMap.get(moduleName);
            moduleList.add( new BLModule(moduleName,interventionName, false,
                             Integer.valueof(result.get('ModuleY1')), Integer.valueof(result.get('ModuleY2')),
                             Integer.valueof(result.get('ModuleY3')),Integer.valueof(result.get('ModuleY4') ) ));
                         
            BLModuleMap.put(moduleName, moduleList);
        }        
    }       
    System.debug('-- Final Map---'+BLModuleMap);
    lstBLModule = new List<BLModule>();
    
    AllYearModuleTotalCost = 0;
    TotalModuleY1Cost = 0;
    TotalModuleY2Cost =0;
    TotalModuleY3Cost =0;
    TotalModuleY4Cost =0;
    
    for( String keyValue : BLModuleMap.keySet()){
        string moduleName = keyValue;
        Integer totalModuleCost = 0;
        Integer ModuleCostY1 = 0; Integer ModuleCostY2 = 0; Integer ModuleCostY3 = 0; Integer ModuleCostY4 = 0;     
        for(Integer index =0; index < BLModuleMap.get(keyValue).size(); index++ ){
            ModuleCostY1  += BLModuleMap.get(keyValue)[index].ModuleCostY1;
            ModuleCostY2  += BLModuleMap.get(keyValue)[index].ModuleCostY2;
            ModuleCostY3  += BLModuleMap.get(keyValue)[index].ModuleCostY3;
            ModuleCostY4  += BLModuleMap.get(keyValue)[index].ModuleCostY4;
            
            
            if( index != 0){
                BLModuleMap.get(keyValue)[index].moduleName = '';
                lstBLModule.add(BLModuleMap.get(keyValue)[index]);
            }else{
                lstBLModule.add(BLModuleMap.get(keyValue)[index]);
            }
            BLModuleMap.get(keyValue)[index].TotalModuleCost = BLModuleMap.get(keyValue)[index].ModuleCostY1 
                                                            + BLModuleMap.get(keyValue)[index].ModuleCostY2 
                                                            + BLModuleMap.get(keyValue)[index].ModuleCostY3 
                                                            + BLModuleMap.get(keyValue)[index].ModuleCostY4;
        }
        //insert and modulewise row
        BlModule moduleObj = new BLModule(moduleName,'', true,ModuleCostY1, ModuleCostY2, ModuleCostY3 ,ModuleCostY4);
        moduleObj.TotalModuleCost = ModuleCostY1 + ModuleCostY2 + ModuleCostY3 + ModuleCostY4;
        lstBLModule.add(moduleObj);
        TotalModuleY1Cost += ModuleCostY1;  TotalModuleY2Cost += ModuleCostY2; TotalModuleY3Cost += ModuleCostY3; TotalModuleY4Cost += ModuleCostY4;
    }
    AllYearModuleTotalCost = TotalModuleY1Cost + TotalModuleY2Cost + TotalModuleY3Cost + TotalModuleY4Cost;
    
    /************************** Cost Grouping ***********************************/
        
        Map<String, BLCost> blCostMap = new Map<String, BLCost>();  
          //adding list to wrapper list  lstBLCost  
            lstBLCost = new List<BLCost>();
         for(AggregateResult objAgg : lstBudgetLineCost){
            BLCost objwrap = new BLCost();
            objwrap.CostGrouping = String.valueof(objAgg.get('CostGrouping'));
            if(objAgg.get('CostY1') != null) objwrap.CostY1 = Integer.valueof(objAgg.get('CostY1'));
            else objwrap.CostY1 = 0;
            if(objAgg.get('CostY2') != null) objwrap.CostY2 = Integer.valueof(objAgg.get('CostY2'));
            else objwrap.CostY2 = 0;
            if(objAgg.get('CostY3') != null) objwrap.CostY3 = Integer.valueof(objAgg.get('CostY3'));
            else objwrap.CostY3 = 0;
            if(objAgg.get('CostY4') != null) objwrap.CostY4 = Integer.valueof(objAgg.get('CostY4'));
            else objwrap.CostY4= 0;
            objwrap.TotalCost = objwrap.CostY1 + objwrap.CostY2 + objwrap.CostY3 + objwrap.CostY4;
          
            blCostMap.put( objwrap.CostGrouping, objwrap); 
            lstBLCost.add(objwrap);
        }
         
        //update the cost wrapper List for HPC products
        for(AggregateResult objAgg : lstBudgetLineCostHPC){
            BLCost objwrap;
            string grping = String.valueof(objAgg.get('CostGrouping'));
            if( !blCostMap.containsKey(grping)){    
                objwrap = new BLCost();
                objwrap.CostGrouping = String.valueof(objAgg.get('CostGrouping'));
                if(objAgg.get('CostY1') != null) objwrap.CostY1 = Integer.valueof(objAgg.get('CostY1'));
                else objwrap.CostY1 = 0;
                if(objAgg.get('CostY2') != null) objwrap.CostY2 = Integer.valueof(objAgg.get('CostY2'));
                else objwrap.CostY2 = 0;
                if(objAgg.get('CostY3') != null) objwrap.CostY3 = Integer.valueof(objAgg.get('CostY3'));
                else objwrap.CostY3 = 0;
                if(objAgg.get('CostY4') != null) objwrap.CostY4 = Integer.valueof(objAgg.get('CostY4'));
                else objwrap.CostY4= 0;
                objwrap.TotalCost = objwrap.CostY1 + objwrap.CostY2 + objwrap.CostY3 + objwrap.CostY4;
                
                lstBLCost.add(objwrap);
            }else{
                objwrap = blCostMap.get(grping);
                objwrap.CostY1 += Integer.valueof(objAgg.get('CostY1'));    
                objwrap.CostY2 += Integer.valueof(objAgg.get('CostY2'));    
                objwrap.CostY3 += Integer.valueof(objAgg.get('CostY3'));
                objwrap.CostY4 += Integer.valueof(objAgg.get('CostY4'));
                
                objwrap.TotalCost = objwrap.CostY1 + objwrap.CostY2 + objwrap.CostY3 + objwrap.CostY4;
            }
        }
        //update total
        TotalY1Cost = 0;
        TotalY2Cost = 0;
        TotalY3Cost = 0;
        TotalY4Cost = 0;
        AllYearTotalCost = 0;
        for( BLCost objwrap : lstBLCost){
            objwrap.TotalCost = objwrap.CostY1 + objwrap.CostY2 + objwrap.CostY3 + objwrap.CostY4;
            
            if(TotalY1Cost == null || TotalY1Cost == 0 ) TotalY1Cost = objwrap.CostY1;
            else TotalY1Cost += objwrap.CostY1;
            if(TotalY2Cost == null || TotalY2Cost == 0) TotalY2Cost = objwrap.CostY2;
            else TotalY2Cost += objwrap.CostY2;
            if(TotalY3Cost == null || TotalY3Cost == 0) TotalY3Cost = objwrap.CostY3;
            else TotalY3Cost += objwrap.CostY3;
            if(TotalY4Cost == null || TotalY4Cost == 0) TotalY4Cost = objwrap.CostY4;
            else TotalY4Cost += objwrap.CostY4;
            if(AllYearTotalCost == null || AllYearTotalCost == 0) AllYearTotalCost = objwrap.TotalCost;
            else AllYearTotalCost += objwrap.TotalCost;     
        }
        
        /*************************** Recepient List ************************************************************/ 
        //adding list to wrapper payee list
        Map<String, BLPayee> payeeNameMap = new Map<String, BLPayee>();
         lstBLPayee = new List<BLPayee>();
         System.debug('lstBudgetLinePayee===>'+lstBudgetLinePayee);
         System.debug('lstHPCPayee===>'+lstHPCPayee); 
        for(AggregateResult objAgg : lstBudgetLinePayee){
            BLPayee objwrap = new BLPayee();
            objwrap.PayeeName = mapImplementer.get((ID)objAgg.get('PayeeId')).Implementer_Name__c;
            if(objAgg.get('PayeeY1') != null) objwrap.PayeeCostY1 = Integer.valueof(objAgg.get('PayeeY1'));
            else objwrap.PayeeCostY1 = 0;
            if(objAgg.get('PayeeY2') != null) objwrap.PayeeCostY2 = Integer.valueof(objAgg.get('PayeeY2'));
            else objwrap.PayeeCostY2 = 0;
            if(objAgg.get('PayeeY3') != null) objwrap.PayeeCostY3 = Integer.valueof(objAgg.get('PayeeY3'));
            else objwrap.PayeeCostY3 = 0;
            if(objAgg.get('PayeeY4') != null) objwrap.PayeeCostY4 = Integer.valueof(objAgg.get('PayeeY4'));
            else objwrap.PayeeCostY4= 0;
            objwrap.TotalPayeeCost = objwrap.PayeeCostY1 + objwrap.PayeeCostY2 + objwrap.PayeeCostY3 + objwrap.PayeeCostY4;
            
            payeeNameMap.put(objwrap.PayeeName, objwrap);
            lstBLPayee.add(objwrap);
        }

        for(AggregateResult objAgg : lstHPCPayee){
            string PayeeName = mapImplementer.get((ID)objAgg.get('recepientId')).Implementer_Name__c;
            BLPayee objwrap ;
            if( !payeeNameMap.containsKey(PayeeName)){
                objwrap = new BLPayee();
                objwrap.PayeeName =PayeeName;
                if(objAgg.get('PayeeY1') != null) objwrap.PayeeCostY1 = Integer.valueof(objAgg.get('PayeeY1'));
                else objwrap.PayeeCostY1 = 0;
                if(objAgg.get('PayeeY2') != null) objwrap.PayeeCostY2 = Integer.valueof(objAgg.get('PayeeY2'));
                else objwrap.PayeeCostY2 = 0;
                if(objAgg.get('PayeeY3') != null) objwrap.PayeeCostY3 = Integer.valueof(objAgg.get('PayeeY3'));
                else objwrap.PayeeCostY3 = 0;
                if(objAgg.get('PayeeY4') != null) objwrap.PayeeCostY4 = Integer.valueof(objAgg.get('PayeeY4'));
                else objwrap.PayeeCostY4= 0;
                objwrap.TotalPayeeCost = objwrap.PayeeCostY1 + objwrap.PayeeCostY2 + objwrap.PayeeCostY3 + objwrap.PayeeCostY4;
                
                lstBLPayee.add(objwrap);
            }else{
                objwrap = payeeNameMap.get(PayeeName);
                objwrap.PayeeCostY1 += Integer.valueof(objAgg.get('PayeeY1'));
                objwrap.PayeeCostY2 += Integer.valueof(objAgg.get('PayeeY2'));
                objwrap.PayeeCostY3 += Integer.valueof(objAgg.get('PayeeY3'));
                objwrap.PayeeCostY4 += Integer.valueof(objAgg.get('PayeeY4'));
                
                objwrap.TotalPayeeCost = objwrap.PayeeCostY1 + objwrap.PayeeCostY2 + objwrap.PayeeCostY3 + objwrap.PayeeCostY4;
                
                
            }
        }
         for( BLPayee objwrap : lstBLPayee){
            objwrap.TotalPayeeCost = objwrap.PayeeCostY1 + objwrap.PayeeCostY2 + objwrap.PayeeCostY3 + objwrap.PayeeCostY4;
            if(TotalPayeeY1Cost == null) TotalPayeeY1Cost = objwrap.PayeeCostY1;
            else TotalPayeeY1Cost += objwrap.PayeeCostY1;
            if(TotalPayeeY2Cost == null) TotalPayeeY2Cost = objwrap.PayeeCostY2;
            else TotalPayeeY2Cost += objwrap.PayeeCostY2;
            if(TotalPayeeY3Cost == null) TotalPayeeY3Cost = objwrap.PayeeCostY3;
            else TotalPayeeY3Cost += objwrap.PayeeCostY3;
            if(TotalPayeeY4Cost == null) TotalPayeeY4Cost = objwrap.PayeeCostY4;
            else TotalPayeeY4Cost += objwrap.PayeeCostY4;
            if(AllYearPayeeTotalCost == null) AllYearPayeeTotalCost = objwrap.TotalPayeeCost;
            else AllYearPayeeTotalCost += objwrap.TotalPayeeCost;
        }               
        checkProfile();
        /*}catch(Exception e){
            system.debug('---Exception--'+e.getStackTraceString());
        }*/
        
            
    }
    
     
    public void checkProfile(){
        List<String> PermissionSets = new List<String>();
        List<PermissionSetAssignment> standalonePermSets = [ SELECT PermissionSet.Label FROM PermissionSetAssignment WHERE Assignee.Username =: UserInfo.getUserName()];
        if(standalonePermSets.size()>0){
        for(PermissionSetAssignment PermSets : standalonePermSets){
            PermissionSets.add(PermSets.PermissionSet.Label);
            system.debug('Name '+PermSets.PermissionSet.Label);

        }}
        
        Id profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        List<Profile_Access_Setting__c> checkpage = [Select Salesforce_Item__c, Status__c from Profile_Access_Setting__c where Page_Name__c ='GMbudgetTable' and (Profile_Name__c =: profilename OR Permission_Sets__c IN: PermissionSets)];
        system.debug(checkpage);
        for (Profile_Access_Setting__c check : checkpage){
            if (check.Salesforce_Item__c == 'External Profile') blnExternalPro = true;
       
        }
        
    }
    public Class BLModule{
        Public Integer ModuleCostY1 {get;set;}
        Public Integer ModuleCostY2 {get;set;}
        Public Integer ModuleCostY3 {get;set;}
        Public Integer ModuleCostY4 {get;set;}
        Public Integer TotalModuleCost {get;set;}
        Public String ModuleName {get;set;}
        Public String InterventionName {get;set;}
        Public Boolean blnbckgrnd {get; set;}
        public BLModule(string ModuleName,string InterventionName ,Boolean blnbckgrnd, integer val1,integer val2,integer val3,integer val4 ){
            this.ModuleName = ModuleName;
            this.InterventionName = InterventionName;
            this.blnbckgrnd = blnbckgrnd;
            this.ModuleCostY1 = val1;
            this.ModuleCostY2 = val2;
            this.ModuleCostY3 = val3;
            this.ModuleCostY4 = val4;
            this.TotalModuleCost = 0;
        }
        public BLModule(){
            
        }
    }
    
    public Class BLCost{
        Public Integer CostY1 {get;set;}
        Public Integer CostY2 {get;set;}
        Public Integer CostY3 {get;set;}
        Public Integer CostY4 {get;set;}
        Public Integer TotalCost {get;set;}
        Public String CostGrouping {get;set;}
    }
    
     public Class BLPayee{
        Public Integer PayeeCostY1 {get;set;}
        Public Integer PayeeCostY2 {get;set;}
        Public Integer PayeeCostY3 {get;set;}
        Public Integer PayeeCostY4 {get;set;}
        Public Integer TotalPayeeCost {get;set;}
        Public String PayeeName {get;set;}
    }

}