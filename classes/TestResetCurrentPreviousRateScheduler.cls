/*********************************************************************************
* Test Class: {TestResetCurrentPreviousRateScheduler}
*  DateCreated : 07/10/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of ResetCurrentPreviousRateScheduler.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      07/10/2014      INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
private class TestResetCurrentPreviousRateScheduler{
    static testmethod void TestResetCurrentPreviousRateScheduler(){
        ResetCurrentPreviousRateScheduler objResetCurrentPreviousRateScheduler  = new ResetCurrentPreviousRateScheduler();
        objResetCurrentPreviousRateScheduler.execute(null);
    }    
}