/**
 * This class contains unit tests for validating the behavior of Apex classes
 * BankAccountInformationExt
 */
@isTest
private class BankAccountInformationExt_Test {

    static testMethod void myUnitTest() {
        
        Implementation_Period__c ip = createData();
        ApexPages.StandardController controller = new ApexPages.StandardController(ip);
        BankAccountInformationExt ext = new BankAccountInformationExt(controller);
        ext.updateBankAccount();
        ext.getCountries();
        test.startTest();
        Bank__c bankAcc = [Select Id FROM Bank__c WHERE SWIFT_BIC_Code__c = 'bankParent' LIMIT 1];
        ext.bankAccount.Selected_Bank_Id__c = bankAcc.Id;
        Country__c objCountry = [SELECT Id FROM Country__c LIMIT 1];
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.getBranches();
        ext.getBanks();
        ext.attachfile();
        List<Attachment> attachments = [select id, name from Attachment where parent.id=:ip.Principal_Recipient__c];
        ext.attachmentIdToDelete = attachments[0].Id;
        ext.removeRow();
        ext.cancel();
        
        ext.changeToViewMode();
        test.stopTest();
        ext.edit();
        ext.cancelRecord();
        ext.updateAccountApprovalStatus();
        ext.bankAccount.Selected_Country_Id__c = bankAcc.Id;
        ext.saveRecord();
        ext.submittForApproval();
        ext.saveChangesAndRedirect();
        
       
        ext.bankAccount.Selected_Country_Id__c = 'none';
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.New_Bank_Name__c = NULL;
        ext.bankAccount.Branch_Type__c = 'none';
        ext.bankAccount.ABA_Code__c= Null;
        ext.bankAccount.SWIFT_BIC_Code__c = NULL;
        ext.saveRecord();
    }
    
    static testMethod void myUnitTest2() {
        PageReference page = new PageReference('');
        Test.setCurrentPage(page);
        page.getParameters().put('mode','view');
        Implementation_Period__c ip = createData();
        ip.Bank_Account__c = null;
        update ip;
        ApexPages.StandardController controller = new ApexPages.StandardController(ip);
        BankAccountInformationExt ext = new BankAccountInformationExt(controller);
        
    }
    static testMethod void myUnitTest3() {
        PageReference page = new PageReference('');
        Test.setCurrentPage(page);
        page.getParameters().put('mode','view');
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        //objCountry.CT_Public_Group_ID__c = objGroup.Id;
        insert objCountry;
        
        Account objAccount = new Account();
        objAccount.Name = 'TestAccount';
        objAccount.Country__c = objCountry.Id;
        objAccount.Approval_Status__c = 'LFA verification';
        objAccount.Boolean_Duplicate__c = true;
        insert objAccount;
        
        Bank__c bankParent = new Bank__c(Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'bankParent', 
                                         Name__c = 'bankParent', ABA_Code__c = 'bankParent');
        insert bankParent;
        
        Bank__c bank1 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test2',
                                         Name__c = 'Test2', ABA_Code__c = 'Test2');
        insert bank1;
        
        Bank_Account__c bankAccParent = new Bank_Account__c(Account__c = objAccount.Id);
        insert bankAccParent;
        
        Bank_Account__c bankAcc = new Bank_Account__c(Account__c = objAccount.Id);
        bankAcc.Bank__c = bank1.Id;
        bankAcc.Approval_Status__c = 'Update Information';
        bankAcc.Selected_Country_Id__c = objCountry.Id;
        bankAcc.Selected_Bank_Id__c = 'New';
        bankAcc.Bank_Address_line_1__c = null;
        bankAcc.Postal_Code_Zip__c = null;
        bankAcc.Intermediary_Bank_ABA_Routing_No__c = 'ABCD';
        insert bankAcc;
       
 
        
        Implementation_Period__c ip = createData();
        ip.Bank_Account__c = bankAcc.Id;
        update ip;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ip);
        BankAccountInformationExt ext = new BankAccountInformationExt(controller);
        bankAcc.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        bankAcc.Please_Explain_Rationale__c = 'test';
        bankAcc.Separate_bank_account_in_local_currency__c = 'No';
        bankAcc.Local_Currency_Bank_Account_Details__c = 'rtfd';
        
        bankAcc.Bank_Account_Name_Beneficiary__c = '';
        ext.varBankAccountNumber ='';
        ext.saveRecord();
        
        bankAcc.City__c = 'Test';
        ext.bankAccount.Bank_Address_line_1__c = null;
        ext.saveRecord();
        
        
        
        ext.clearBankAccount();
        
        test.startTest();
        Bank__c bank2 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test2',
                                         Name__c = 'Test2', ABA_Code__c = 'Test2');
        
        Bank_Account__c bankAcc1 = new Bank_Account__c(Account__c = objAccount.Id);
        bankAcc1.Bank__c = bank2.Id;
        bankAcc1.Approval_Status__c = 'Approved';
        insert bankAcc1;
        
        Implementation_Period__c ip1 = createData();
        ip1.Bank_Account__c = bankAcc1.Id;
        update ip1;
        
        ApexPages.StandardController controller1 = new ApexPages.StandardController(ip1);
        BankAccountInformationExt ext1 = new BankAccountInformationExt(controller1);
        ext1.changeToViewMode();
        test.stopTest();
        
    }
    static testMethod void myUnitTest4() {
        PageReference page = new PageReference('');
        Test.setCurrentPage(page);
        page.getParameters().put('mode','view');
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        //objCountry.CT_Public_Group_ID__c = objGroup.Id;
        insert objCountry;
        
        Account objAccount = new Account();
        objAccount.Name = 'TestAccount';
        objAccount.Country__c = objCountry.Id;
        objAccount.Approval_Status__c = 'LFA verification';
        objAccount.Boolean_Duplicate__c = true;
        insert objAccount;
        
        Bank__c bankParent = new Bank__c(Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'bankParent', 
                                         Name__c = 'bankParent', ABA_Code__c = 'bankParent');
        insert bankParent;
        
        Bank__c bank1 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test2',
                                         Name__c = 'Test2', ABA_Code__c = 'Test2');
        insert bank1;
        
        Bank_Account__c bankAccParent = new Bank_Account__c(Account__c = objAccount.Id);
        insert bankAccParent;
        
        Bank_Account__c bankAcc = new Bank_Account__c(Account__c = objAccount.Id);
        bankAcc.Bank__c = bank1.Id;
        bankAcc.Selected_Country_Id__c = 'none';
        bankAcc.Approval_Status__c = 'Finance Officer Verification';
        bankAcc.Approved_in_GFS__c = false;
        insert bankAcc;
        
        
       Implementation_Period__c ip= new Implementation_Period__c(); 
        ip.Principal_Recipient__c = objAccount.Id;
        ip.Bank_Account__c = bankAcc.Id;
        insert ip;
        
        Org_Bank_Account_Junc__c objOAJunct1 = new Org_Bank_Account_Junc__c ();
        objOAJunct1.Bank_Account__c = bankAcc.Id;
        objOAJunct1.Account__c = objAccount.Id;
        objOAJunct1.Grant_Implementation_Period__c = ip.Id;
        objOAJunct1.Approval_Status__c = 'Finance Officer verification';
        objOAJunct1.Current_Status__c = true;
        insert objOAJunct1;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ip);
        BankAccountInformationExt ext = new BankAccountInformationExt(controller);
        ext.lock = false;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = 'none';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = NULL;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = NULL;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Selected_Bank_Id__c = bank1.Id;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'none';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = NULL;
        ext.saveRecord();
        ext.clearBankAccount();
        
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'New';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = bank1.Id;
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = NULL;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = null;
        ext.bankAccount.SWIFT_BIC_Code__c= null;
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = NULL;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = 'adddf';
        ext.bankAccount.SWIFT_BIC_Code__c= null;
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = NULL;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = NULL;
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = null;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = null ;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = 'aaaa';
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = null;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = null;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = null;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = null;
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.getBankAccount();
        ext.bankAccount = bankAcc;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = Null;
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'No';
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.getBankAccount();
        ext.bankAccount = bankAcc;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = 'New';
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = 'New';
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'No';
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.getBankAccount();
        ext.bankAccount = bankAcc;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = bank1.Id;
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'No';
        ext.saveRecord();
        ext.clearBankAccount();
        
        test.startTest();
        Bank__c bank2 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test2',
                                         Name__c = 'Test2', ABA_Code__c = 'Test2');
        
        Bank_Account__c bankAcc1 = new Bank_Account__c(Account__c = objAccount.Id);
        bankAcc1.Bank__c = bank2.Id;
        bankAcc1.Approval_Status__c = 'Approved';
        bankAcc1.Account__c = objAccount.Id;
        insert bankAcc1;
        
        Implementation_Period__c implementationPeriod = createData();
        implementationPeriod.Bank_Account__c = bankAcc1.Id;
        update implementationPeriod;
        
        ApexPages.StandardController controller1 = new ApexPages.StandardController(implementationPeriod);
        BankAccountInformationExt ext1 = new BankAccountInformationExt(controller1);
        ext.updateAccountApprovalStatus();
        ext1.changeToViewMode();
        ext1.clearSelectedBankAccount();
        test.stopTest();
        
    }
    static testMethod void myUnitTest5() {
        PageReference page = new PageReference('');
        Test.setCurrentPage(page);
        page.getParameters().put('mode','view');
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        //objCountry.CT_Public_Group_ID__c = objGroup.Id;
        insert objCountry;
        
        Account objAccount = new Account();
        objAccount.Name = 'TestAccount';
        objAccount.Country__c = objCountry.Id;
        objAccount.Approval_Status__c = 'LFA verification';
        objAccount.Boolean_Duplicate__c = true;
        insert objAccount;
        
        Bank__c bankParent = new Bank__c(Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'bankParent', 
                                         Name__c = 'bankParent', ABA_Code__c = 'bankParent');
        insert bankParent;
        
        Bank__c bank1 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test2',
                                         Name__c = 'Test2', ABA_Code__c = 'Test2');
        insert bank1;
        
        Bank_Account__c bankAccParent = new Bank_Account__c(Account__c = objAccount.Id);
        insert bankAccParent;
        
        Bank_Account__c bankAcc = new Bank_Account__c(Account__c = objAccount.Id);
        bankAcc.Bank__c = bank1.Id;
        bankAcc.Selected_Country_Id__c = 'none';
        bankAcc.Approval_Status__c = 'Finance Officer Verification';
        bankAcc.Approved_in_GFS__c = false;
        insert bankAcc;
        
        
       Implementation_Period__c ip= new Implementation_Period__c(); 
        ip.Principal_Recipient__c = objAccount.Id;
        ip.Bank_Account__c = bankAcc.Id;
        insert ip;
        
        Org_Bank_Account_Junc__c objOAJunct1 = new Org_Bank_Account_Junc__c ();
        objOAJunct1.Bank_Account__c = bankAcc.Id;
        objOAJunct1.Account__c = objAccount.Id;
        objOAJunct1.Grant_Implementation_Period__c = ip.Id;
        objOAJunct1.Approval_Status__c = 'Finance Officer verification';
        objOAJunct1.Current_Status__c = true;
        insert objOAJunct1;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(ip);
        BankAccountInformationExt ext = new BankAccountInformationExt(controller);
        ext.getBankAccount();
        ext.bankAccount = bankAcc;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = bank1.Id;
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = null;
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'No';
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.getBankAccount();
        ext.bankAccount = bankAcc;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = bank1.Id;
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'test';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'Yes';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = null;
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = 'tatr';
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'No';
        ext.saveRecord();
        ext.clearBankAccount();
        
        ext.getBankAccount();
        ext.bankAccount = bankAcc;
        ext.bankAccount.Bank__c = bank1.Id;
        ext.bankAccount.Selected_Bank_Id__c = bank1.Id;
        ext.bankAccount.Selected_Country_Id__c = objCountry.Id;
        ext.bankAccount.Branch_Type__c = 'SWIFT';
        ext.bankAccount.New_Bank_Name__c = 'Test';
        ext.bankAccount.Selected_Branch_Id__c = bank1.Id;
        ext.bankAccount.ABA_Code__c = '1212121';
        ext.bankAccount.Bank_Address_line_1__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'Yes';
        ext.bankAccount.Please_Explain_Rationale__c = 'tt';
        ext.bankAccount.Separate_bank_account_in_local_currency__c = 'No';
        ext.bankAccount.Local_Currency_Bank_Account_Details__c = 'rtfd';
        ext.bankAccount.Bank_Account_Name_Beneficiary__c = null;
        ext.bankAccount.City__c = 'Test';
        ext.bankAccount.Postal_Code_Zip__c = '123456';
        ext.varBankAccountNumber = '12356';
        ext.bankAccount.Intermediary_Bank_ABA_Routing_No__c = '123';
        ext.bankAccount.Intermediary_Bank__c = 'Yes';
        ext.bankAccount.Intermediary_Bank_Name__c = 'Testing';
        ext.bankAccount.Intermediary_Bank_Swift_BIC_code__c = '1212121';
        ext.bankAccount.Intermediary_Bank_Country__c = 'Test';
        ext.bankAccount.Bank_Account_in_the_Grant_Currency__c = 'No';
        ext.saveRecord();
        ext.clearBankAccount();
        
        }
    public static Implementation_Period__c createData() {
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        //objCountry.CT_Public_Group_ID__c = objGroup.Id;
        insert objCountry;
        
        Account objAccount = new Account();
        objAccount.Name = 'TestAccount';
        objAccount.Country__c = objCountry.Id;
        objAccount.Approval_Status__c = 'LFA verification';
        objAccount.Boolean_Duplicate__c = true;
        insert objAccount;
        Grant__c grant = new Grant__c(Name = 'Grant',Principal_Recipient__c = objAccount.id);
        insert grant;
        Bank__c bankParent = new Bank__c(Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'bankParent', 
                                         Name__c = 'bankParent', ABA_Code__c = 'bankParent');
        insert bankParent;
        Bank__c bank1 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test1',
                                         Name__c = 'Test1', ABA_Code__c = 'Test1');
        insert bank1;
        Bank__c bank2 = new Bank__c(Bank__c = bankParent.id,Country__c = objCountry.Id , SWIFT_BIC_Code__c = 'Test2',
                                         Name__c = 'Test2', ABA_Code__c = 'Test2');
        insert bank2;
        Bank_Account__c bankAccParent = new Bank_Account__c(Account__c = objAccount.Id);
        insert bankAccParent;
        Bank_Account__c bankAcc = new Bank_Account__c(Account__c = objAccount.Id);
        bankAcc.Bank__c = bank1.Id;
        bankAcc.Approval_Status__c = 'Update information';
        
        insert bankAcc;
        
        
        
        Implementation_Period__c implementationPeriod = new Implementation_Period__c(); 
        implementationPeriod.Principal_Recipient__c = objAccount.Id;
        implementationPeriod.Bank_Account__c = bankAcc.Id;
        insert implementationPeriod;
        
        Attachment attach=new Attachment();     
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = objAccount.id;
        insert attach;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(implementationPeriod);
        objPF.PF_Status__c = 'Accepted';
        insert objPF;
        
        HPC_Framework__c objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = implementationPeriod.Id;
        objHPC.HPC_Status__c = 'Accepted';
        insert objHPC;
        
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Implementation_Period__c =  implementationPeriod.Id;
        objDB.Budget_Status__c = 'Accepted';
        insert objDB;
        
        
        
        return implementationPeriod;
        
        
    }
}