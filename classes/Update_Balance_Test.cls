/**
 * This class contains unit tests for validating the behavior of the Update_Balance trigger.
 */
@isTest
private class Update_Balance_Test {

    static testMethod void validate_Update_Balance() {

        Project_Overview__c po = new Project_Overview__c( Name = 'Test Project', Project_Id__c = 'TT1', Customer_Name__c = 'Test Account'); 
        insert po; 

        // Create first burn report, last week
        Burn_Report__c br1 = new Burn_Report__c( Project_Overview__c = po.Id, Resource__c = 'Dummy Resource', Role__c = 'Technical Architect', Rate_hr__c = 10, Forecast_Budget_Hrs__c = 50, Budget_Hrs_Wk__c = 40, Week_Ending__c = date.today().addDays(-7));
        insert br1;
    
        // Create second burn report, this week
        Burn_Report__c br2 = new Burn_Report__c( Previous_Burn_Report__c = br1.Id, Project_Overview__c = po.Id, Resource__c = 'Dummy Resource', Role__c = 'Technical Architect', Rate_hr__c = 10, Forecast_Budget_Hrs__c = 50, Budget_Hrs_Wk__c = 40, Week_Ending__c = date.today());
        insert br2;
        
        // Create third burn report, next week
        Burn_Report__c br3 = new Burn_Report__c( Previous_Burn_Report__c = br2.Id, Project_Overview__c = po.Id, Resource__c = 'Dummy Resource', Role__c = 'Technical Architect', Rate_hr__c = 10, Forecast_Budget_Hrs__c = 50, Budget_Hrs_Wk__c = 40, Week_Ending__c = date.today().addDays(7));
        insert br3;

        // Check the balance on the new burn report 
        br3 = [SELECT Balance_Budget_Hrs__c FROM Burn_Report__c WHERE Id =:br3.Id];
        System.debug('br3 Balance Budget before trigger fired: ' + br3.Balance_Budget_Hrs__c);

        // Update last week
        br1.Actual_Hrs_Wk__c = 40.0; 
            update br1;
                
        // Test that the trigger correctly updated the balance this week
        br2 = [SELECT Balance_Budget_Hrs__c FROM Burn_Report__c WHERE Id =:br2.Id];
        system.assertEquals (40.0, br2.Balance_Budget_Hrs__c);

        // Test that the trigger correctly updated the balance next week
        br3 = [SELECT Balance_Budget_Hrs__c FROM Burn_Report__c WHERE Id =:br3.Id];
        system.assertEquals (40.0, br3.Balance_Budget_Hrs__c);
 
        // Update this week
        br2.Actual_Hrs_Wk__c = 20.0; 
            update br2;
                
        // Test that the trigger correctly updated the balance this week
        br2 = [SELECT Balance_Budget_Hrs__c FROM Burn_Report__c WHERE Id =:br2.Id];
        system.assertEquals (20.0, br2.Balance_Budget_Hrs__c);

        // Test that the trigger correctly updated the balance next week
        br3 = [SELECT Balance_Budget_Hrs__c FROM Burn_Report__c WHERE Id =:br3.Id];
        system.assertEquals (20.0, br3.Balance_Budget_Hrs__c);
          
    }
}