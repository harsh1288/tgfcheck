/*********************************************************************************
* Test Class: {TestctrlWPNegotiation}
*  DateCreated : 07/16/2013
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of ctrlctrlWPNegotiation.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      07/16/2013      INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
Public Class TestctrlWPNegotiation{
    Public static testMethod Void ctrlWPNegotiation(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        
        insert objWp;
        
        test.starttest();
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Cancelled';   
        insert objService;
        
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);
        
        objCls.ClearFilterMainGrid();
        objCls.ClearFilterMainGrid();
        objCls.CollepseAllServiceType();
        objCls.ShowAllServiceTypeWithNoLoE();
        objCls.HideAllServiceTypeWithNoLoE();
        objCls.ExapndAllServiceType();
        objCls.ShowCancelled();
        objCls.HideCancelled();
        
        objCls.FillSelectedStatus('Not Started');
        Apexpages.currentpage().getparameters().put('AddIndex','1');
        objCls.Addservice();
        
        Apexpages.currentpage().getparameters().put('SaveServiceIndex','0');
        Apexpages.currentpage().getparameters().put('SaveServiceTypeIndex','1');
        objCls.ProfileName = 'LFA Portal User';
        
        update objService;
        test.stoptest();
        ctrlWPNegotiation.wrapResource objwrap3 = new ctrlWPNegotiation.wrapResource();
        objwrap3.objResource = objResource;
        
        
         
        ctrlWPNegotiation.wrapServices objwrap1 = new ctrlWPNegotiation.wrapServices();
        objwrap1.objService = objService;
        objwrap1.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
       
        ctrlWPNegotiation.wrapServiceType objwrap2 = new ctrlWPNegotiation.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        
       
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
        objCls.lstwrapServicesType.add(objwrap2);
        
        objCls.SaveServiceAndAddResources();
        
        objCls.strSelectedstatus = 'LFA Change Request';
        objCls.ApproveService();
        objCls.strSelectedstatus = 'Completed';
        objCls.ApproveService();
        objCls.strSelectedstatus = 'Cancelled';
    
        objCls.LFAServiceCompletionApprovalsProcess();
        ctrlWPNegotiation.getWorkItemId(objservice.id);
        
     
        
    }
    Public static testMethod Void ctrlWPNegotiation1(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='TGF Counter-Proposal';   
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        
        Apexpages.currentpage().getparameters().put('Sid',objService.id);
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);

        //objCls.EditableResourcePopup();
        objCls.FillSelectedStatus('Not Started');
        Apexpages.currentpage().getparameters().put('AddIndex','1');
        objCls.Addservice();
        Apexpages.currentpage().getparameters().put('SaveServiceIndex','0');
        Apexpages.currentpage().getparameters().put('SaveServiceTypeIndex','1');
        
        //objCls.SaveServiceAndAddResources();
        objCls.ProfileName = 'LFA Portal User';
        //objCls.RejectService();
        
                
        
        ctrlWPNegotiation.wrapResource objwrap3 = new ctrlWPNegotiation.wrapResource();
        objwrap3.objResource = objResource;
        
        
         
        ctrlWPNegotiation.wrapServices objwrap1 = new ctrlWPNegotiation.wrapServices();
        objwrap1.objService = objService;
        objwrap1.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
       
        ctrlWPNegotiation.wrapServiceType objwrap2 = new ctrlWPNegotiation.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        
       
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
        Apexpages.currentpage().getparameters().put('newServiceId',objService.id);
        objCls.AddResource();
        objCls.SaveAndReturnResource();
        
        objCls.lstwrapServicesType.add(objwrap2);
        Apexpages.currentpage().getparameters().put('RIndex','0');
        //objCls.SubmitChangeRequest();
        
        ctrlWPNegotiation.wrapServices objwrap32 = new ctrlWPNegotiation.wrapServices();
        objwrap32.objService = objService;
        objwrap32.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap32.lstwrapResource.add(objwrap3);
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap32);
        objCls.lstwrapServicesType.add(objwrap2);
        //objCls.SubmitCounterProposal();
        
        objCls.ProfileName = 'LFA Portal User';
        //objCls.SubmitCounterProposal();
        
        //objCls.TGFCounterProposal();
        //objCls.TGFCounterProposalSubmit();
        //objCls.TGFCounterProposalCancel();
        
        objCls.EditableResourcePopup();
               
        //objCls.AnalyseMainGrid();
        
        objCls.ClearFilterSearchGrid();
       
    }
    Public static testMethod Void ctrlWPNegotiation2(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Cancelled';   
        insert objService;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.name = 'Test Role';
        insert objRole;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.LFA_Role__c = objRole.id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        
        
        Apexpages.currentpage().getparameters().put('Sid',objService.id);
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);
        ctrlWPNegotiation.wrapResource objwrap3 = new ctrlWPNegotiation.wrapResource();
        objwrap3.objResource = objResource;
        ctrlWPNegotiation.wrapServices objwrap1 = new ctrlWPNegotiation.wrapServices();
        objwrap1.objService = objService;
        objwrap1.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
        //objCls.lstwrapResource.add(objwrap3);

       
        ctrlWPNegotiation.wrapServiceType objwrap2 = new ctrlWPNegotiation.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
       
        
        objCls.lstwrapServicesType.add(objwrap2);
        objCls.lstwrapServicesType[0].lstwrapServices.add(objwrap1);
        
        
        Apexpages.currentpage().getparameters().put('newServiceId',objService.id);

        objCls.AddResource();
        objCls.SaveResources();
        objCls.SaveResourcesOther();
        List<LFA_Resource__c>  lstRes=objCls.getResorcesForPDF();
        objCls.CancelResources();
        objCls.CancelEditablePopup();
        objCls.BackToWorkPlan();
        objCls.PostPdf();
        objCls.PostAgreedXLS();
        objCls.PostXLSAcceptTGFOffer();
        objCls.BackToWorkPlanAndSaveStage();
       
        objCls.setSortDirectionMainGrid('');
        string str = objCls.getSortDirectionMainGrid();
        objCls.sortDataMainGrid();
        
        
        
    }
    Public static testMethod Void ctrlWPNegotiation3(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Cancelled';   
        insert objService;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role1';
        insert objRole;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.Contact__c = objCon.Id;
        objResource.LFA_Role__c = objRole.Id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        //objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        objRate.Gf_approved__c = 'Approved';
        insert objRate;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole.Id;
        //objRate.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        objRate1.Gf_approved__c = 'Approved';
        insert objRate1;
        
        
        Apexpages.currentpage().getparameters().put('Sid',objService.id);
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);
        
        ctrlWPNegotiation.wrapResource objwrap3 = new ctrlWPNegotiation.wrapResource();
        objwrap3.objResource = objResource;
        ctrlWPNegotiation.wrapServices objwrap1 = new ctrlWPNegotiation.wrapServices();
        objwrap1.objService = objService;
        objwrap1.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
        //objCls.lstwrapResource.add(objwrap3);

       
        ctrlWPNegotiation.wrapServiceType objwrap2 = new ctrlWPNegotiation.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
       
        
        objCls.lstwrapServicesType.add(objwrap2);
        objCls.lstwrapServicesType[0].lstwrapServices.add(objwrap1);
        
        Apexpages.currentpage().getparameters().put('ResourceIndex','0');
        objCls.FillContactsOnChangeRole();
        
        Apexpages.currentpage().getparameters().put('ResourceIndexForRate','0');
        Test.startTest();
        objCls.FillRateOnChangeContact();
        
        objCls.CancelChangeRequest();
        objCls.TGFCounterProposalCancel();
        objCls.ModifyService();
        
        objCls.AcceptTGFApprovalProcess();
        objCls.CancelServiceAddOnTGFCP();
        Test.stopTest();
        
    }
    Public static testMethod Void ctrlWPNegotiation4(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Cancelled';   
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        Apexpages.currentpage().getparameters().put('Sid',objService.id);
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);
        ctrlWPNegotiation.wrapResource objwrap3 = new ctrlWPNegotiation.wrapResource();
        objwrap3.objResource = objResource;
        ctrlWPNegotiation.wrapServices objwrap1 = new ctrlWPNegotiation.wrapServices();
        objwrap1.objService = objService;
        objwrap1.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
        //objCls.lstwrapResource.add(objwrap3);

       
        ctrlWPNegotiation.wrapServiceType objwrap2 = new ctrlWPNegotiation.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
        objCls.lstwrapServicesType.add(objwrap2);
        objCls.lstwrapServicesType[0].lstwrapServices.add(objwrap1);
       
        List<LFA_Service__c> lstServiceForSearchTemp = new List<LFA_Service__c>();
        lstServiceForSearchTemp.add(objService);
        
        
        ctrlWPNegotiation.wrapSearchService objwrapSearch = new ctrlWPNegotiation.wrapSearchService();
        objwrapSearch.objSearchService= objService;
        objwrapSearch.IsChecked= true;
        objwrapSearch.IsChecked= true;
        objCls.lstServiceForSearch.add(objwrapSearch);
        
        
        Apexpages.currentpage().getparameters().put('DeleteResourceIndex','0');
        
        Apexpages.currentpage().getparameters().put('DeleteSTIndex','0');
        Apexpages.currentpage().getparameters().put('DeleteIndex','0');
        
        objCls.DeleteResource();
        objCls.DeleteService();
        
       
        
    }
    Public static testMethod Void ctrlWPNegotiation5(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2015';
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Cancelled';   
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        Apexpages.currentpage().getparameters().put('Sid',objService.id);
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);
        
        Apexpages.currentpage().getparameters().put('DuplicateIndex','0');
        Apexpages.currentpage().getparameters().put('DuplicateSTIndex','0');
        
        Apexpages.currentpage().getparameters().put('ServiceId',objService.id);
        objCls.DuplicateService();
        objCls.CancelServiceAddition();
        
    }
    Public static testMethod Void ctrlWPNegotiation6(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Not Started';   
        insert objService;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role1';
        insert objRole;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.LFA_Service__c = objService.id;
        objResource.Contact__c = objCon.Id;
        objResource.LFA_Role__c = objRole.Id;
        objResource.Rate__c = 1;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        //objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        objRate.Gf_approved__c = 'Approved';
        insert objRate;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole.Id;
        objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        objRate1.Gf_approved__c = 'Approved';
        insert objRate1;
        
        
        Apexpages.currentpage().getparameters().put('Sid',objService.id);
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlWPNegotiation objCls = new ctrlWPNegotiation(sc);
        
        ctrlWPNegotiation.wrapResource objwrap3 = new ctrlWPNegotiation.wrapResource();
        objwrap3.objResource = objResource;
        ctrlWPNegotiation.wrapServices objwrap1 = new ctrlWPNegotiation.wrapServices();
        objwrap1.objService = objService;
        objwrap1.lstwrapResource = new List<ctrlWPNegotiation.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
        //objCls.lstwrapResource.add(objwrap3);

       
        ctrlWPNegotiation.wrapServiceType objwrap2 = new ctrlWPNegotiation.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        objwrap2.lstwrapServices = new List<ctrlWPNegotiation.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
       
        
        objCls.lstwrapServicesType.add(objwrap2);
        objCls.lstwrapServicesType[0].lstwrapServices.add(objwrap1);
        
        //Apexpages.currentpage().getparameters().put('RIndex','0');
        Apexpages.currentpage().getparameters().put('ResourceIndex','0');
        objCls.FillContactsOnChangeRole();
        
        Apexpages.currentpage().getparameters().put('ResourceIndexForRate','0');
        objCls.FillRateOnChangeContact();
        objCls.ModifyService();
        
    }
}