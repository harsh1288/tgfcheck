/*********************************************************************************
* {Test} Class: {ctrlGrantCoverageHistory_Test}
* Purpose/Methods:
    - Used to cover all methods of ctrlGrantCoverageHistory.* 

*********************************************************************************/
@isTest
Public Class ctrlGrantCoverageHistory_Test{
    Public static testMethod void TestFieldHistory1(){    
        
        Grant_Indicator__c objIndicator = TestClassHelper.insertGrantIndicator();
        Period__c objPeriod = testClassHelper.insertPeriod();
        Result__c objResult = new Result__c ();
        objResult.Period__C = objPeriod.Id;
        objResult.Indicator__c = objIndicator.Id;
        insert objResult;
         
        objResult.Target__c = 10;
        update objResult;
        
        objResult.Target__c = 20;
        update objResult;
        
            
        ctrlGrantCoverageHistory objGCH = new ctrlGrantCoverageHistory();
        objGCH.strObjectName = 'Result__c';
        objGCH.strRecordId = objResult.Id;
        objGCH.getRetrieveFieldHistory();
        
        objGCH.strRecordId = objIndicator.Id;
        objGCH.getRetrieveResultHistory();
        
    }
}