/*********************************************************************************
* {Test} Class: {TestLFAInvoiceTrigger}
* Created by {DeveloperName},  {DateCreated 05/16/2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of LFAInvoiceTrigger Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
   1.0                        05/16/2014
*********************************************************************************/

@isTest
Public class TestLFAInvoiceTrigger{
    Public static testMethod Void TestLFAInvoiceTrigger(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        insert objWp;
        
        LFA_Invoice__c objLFAInvoice = new  LFA_Invoice__c();
        objLFAInvoice.LFA_Work_Plan__c = objWp.id;
        insert objLFAInvoice;
        
        LFA_Service__c objLFAService = new LFA_Service__c();
        objLFAService.LFA_Work_Plan__c = objWp.id;
        insert objLFAService;
        
        Service_Invoice__c objServiceInvoice = new Service_Invoice__c();
        objServiceInvoice.LFA_Service__c= objLFAService.id;
        objServiceInvoice.LFA_Invoice__c = objLFAInvoice.id;
        insert objServiceInvoice ;
        
        objLFAInvoice.Status__c = 'Payment Approved';
        update objLFAInvoice;
        
    }
}