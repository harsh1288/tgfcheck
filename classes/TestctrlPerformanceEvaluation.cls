/*********************************************************************************
* Test Class: {TestctrlPerformanceEvaluation}
*  
----------------------------------------------------------------------------------

*********************************************************************************/
@isTest (SeeAllData=true)
Public Class TestctrlPerformanceEvaluation{
    Public static testMethod Void TestctrlPerformanceEvaluation(){
        List<RecordType> lstRecordTypeAccount = [Select Id,DeveloperName from RecordType Where SObjectType = 'Account' AND DeveloperName= 'LFA' limit 1];
        Account objAcc= new Account();
        objAcc.RecordTypeId = lstRecordTypeAccount[0].Id;
        objAcc.Name = 'Test1';
        insert objAcc;
        Account objAccount2= new Account();
        objAccount2.RecordTypeId = lstRecordTypeAccount[0].Id;
        objAccount2.Name = 'Test2';
        objAccount2.ParentID = objAcc.ID;
        insert objAccount2;
        List<Contact> contactsToInsert = new List<Contact>();
        Contact objContact = new Contact();
        objContact.LastName = 'TestLast';
        objContact.AccountID = objAccount2.ID;
        objContact.Receive_PET_Alerts__c = true;
        contactsToInsert.add(objContact);
        
        Contact objContact1 = new Contact();
        objContact1.LastName = 'TestLast';
        objContact1.AccountID = objAcc.ID;
        objContact1.Receive_PET_Alerts__c = true;
        contactsToInsert.add(objContact1);
        insert contactsToInsert;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',ContactID = objContact.ID);
        
        
        List<Country__c> countriesToInsert = new List<Country__c>();
        Country__c objCountry = new Country__c();
        objCountry.Name ='India';
        //objCountry.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        countriesToInsert.add(objCountry );
       
        Country__c objCountry1 = new Country__c();
        objCountry1.Name ='India1';
        //objCountry1.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        countriesToInsert.add(objCountry1) ;
        insert countriesToInsert;
        List<LFA_Work_Plan__c> lfpToInsert = new List<LFA_Work_Plan__c>();
        LFA_Work_Plan__c lfwp = new LFA_Work_Plan__c();
        lfwp.Name = 'Test';
        lfwp.LFA_Access__c = 'Read';
        lfwp.LFA__c = objAccount2.Id;
        lfwp.Country__c = objCountry.ID;
        
        lfpToInsert.add(lfwp);
                
        LFA_Work_Plan__c lfwp1 = new LFA_Work_Plan__c();
        lfwp1.Name = 'Test';
        lfwp1.LFA_Access__c = 'Read';
        lfwp1.LFA__c = objAccount2.Id;
        lfwp1.Country__c = objCountry1.ID;
        lfpToInsert.add(lfwp1);
        insert lfpToInsert;       
        
        PET_Access__c objPETAccess1 = new PET_Access__c();
        objPETAccess1.LFA_Work_Plan__c = lfwp1.id;
        objPETAccess1.PET_Access__c = 'Read';
        objPETAccess1.User__c = u.id;
        objPETAccess1.Receive_PET_Alerts__c = true;
        insert objPETAccess1 ; 
        
        List<LFA_Work_Plan__c> wpsToInsert = new List<LFA_Work_Plan__c>();
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
       wpsToInsert.add(objWP);
          LFA_Work_Plan__c objWp1 = new LFA_Work_Plan__c();
        objWp1.Name = 'Test Wp';
        objWp1.LFA__c = objAcc.Id;
        objWp1.Country__c = objCountry.id;
        objWp1.End_Date__c = system.today();
        wpsToInsert.add(objWP1);
        insert wpsToInsert;
       
        List<LFA_Service__c> servicesToInsert = new List<LFA_Service__c>();
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        servicesToInsert.add(objService);
        
         LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        servicesToInsert.add(objService1);
        
            LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
       servicesToInsert.add(objService2);
        
        LFA_Service__c objService4 = new LFA_Service__c();
        objService4.Name = 'PUDR';
        objService4.LFA_Work_Plan__c = objWp1.id;
        objService4.Service_Type__c = 'Key Services';
        objService4.Service_Sub_Type__c = 'PUDR';
         servicesToInsert.add(objService4);
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        servicesToInsert.add(objService3);
        
            insert servicesToInsert;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        
       
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        insert objResource1;
        
        
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService2.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        insert objResource2;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        insert objRate;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        
    
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role1';
        insert objRole1;
        
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole1.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        insert objResource3;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        insert objRate1;
        
      
        
        
        
        LFA_Resource__c objResource4 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource4.LFA_Service__c = objService4.id;
        objResource4.Rate__c = 1;
        objResource4.LFA_Role__c = objRole1.Id;
        objResource4.LOE__c = 1;
        objResource4.CT_Planned_Cost__c = 1;
        objResource4.CT_Planned_LOE__c = 1;
        objResource4.TGF_Proposed_Cost__c = 1;
        objResource4.TGF_Proposed_LOE__c = 1;
        insert objResource4;
        
        LFA_Service__c objService5 = new LFA_Service__c();
        objService5.Name = 'PUDR';
        objService5.LFA_Work_Plan__c = objWp1.id;
        objService5.Service_Type__c = 'Key Services';
        objService5.Service_Sub_Type__c = 'PUDR';
        insert objService5;
        
        LFA_Resource__c objResource5  = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource5.LFA_Service__c = objService5.id;
        objResource5.Rate__c = 1;
        objResource5.LFA_Role__c = objRole1.Id;
        objResource5.LOE__c = 1;
        objResource5.CT_Planned_Cost__c = 1;
        objResource5.CT_Planned_LOE__c = 1;
        objResource5.TGF_Proposed_Cost__c = 1;
        objResource5.TGF_Proposed_LOE__c = 1;
        insert objResource5;
        
        String strRecordTypeId = [Select Id From RecordType Where sobjectType = 'Performance_Evaluation__c'  And DeveloperName = 'Period'].Id;
        
        Performance_Evaluation__c perf = new Performance_Evaluation__c();
        //perf.Q1_Rating__c='1';
        perf.RecordTypeId = strRecordTypeId;
        perf.LFA_Work_Plan__c = objWp.Id;
        insert perf;
                    
       
        Test.setCurrentPage(Page.PerformanceEvaluation);
        Apexpages.currentpage().getparameters().put('id',perf.id);
        Apexpages.currentpage().getparameters().put('RecordType',strRecordTypeId);
     
        ApexPages.StandardController sc = new ApexPages.StandardController(perf);
        ctrlPerformanceEvaluation objCls = new ctrlPerformanceEvaluation(sc);
        

        test.startTest();
        objCls.strRecordTypeName = 'Period';  
        //objCls.strRecordTypeId = '012g0000000Cp6Y';
        
        objCls.SavePE();
        objCls.coverall();
        objCls.coverall2();
        objCls.coverall3();
        
        objCls.ClearFilterMainGrid();
        objCls.ExapndAllServiceType();
        objCls.CollepseAllServiceType();
        //objCls.FillGrant();
        /*objCls.FillRole();
        objCls.FillCountry();
        objCls.FillYear();
        objCls.FillRegion();
        objCls.FillAllRisksOptions();
        objCls.FillLFA();*/
        
        Apexpages.currentpage().getparameters().put('AddIndex','0');
        //
        objCls.RetriveResourceValues();
        Apexpages.currentpage().getparameters().put('SaveServiceIndex','3');
        Apexpages.currentpage().getparameters().put('SaveServiceTypeIndex','0');
        //objCls.SaveServiceAndAddResources();
        
        Apexpages.currentpage().getparameters().put('newServiceId',objService.id);
        //objCls.AddResource();
        
        //objCls.CancelResources();
        
        Apexpages.currentpage().getparameters().put('DuplicateIndex','0');
        Apexpages.currentpage().getparameters().put('DuplicateSTIndex','0');
        Apexpages.currentpage().getparameters().put('ServiceId',objService.id);
        //objCls.DuplicateService();
        
        objCls.rerenderaction();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','1');
        Apexpages.currentpage().getparameters().put('DeleteSTIndex','0');
        
        objCls.CancelReadOnlyResources();
        
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        
       // objCls.EditService();
        Apexpages.currentpage().getparameters().put('ResourceIndex','0');
       // objCls.FillContactsOnChangeRole();
        Apexpages.currentpage().getparameters().put('ResourceIndexForRate','0');
       // objCls.FillRateOnChangeContact();
        Apexpages.currentpage().getparameters().put('intIndexST','0');
        Apexpages.currentpage().getparameters().put('intIndex','0');
      //  objCls.SaveResources();
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','0');
     //   objCls.EditService();
        Apexpages.currentpage().getparameters().put('DeleteResourceIndex','0');
     //   
        
        objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
       // objCls.AnalyseSearch();
        
        objCls.sortExpression = 'Country__c';
        objCls.sortDirection = 'ASC';
        objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
        objCls.getSortDirection();
        objCls.setSortDirection('ASC');
        //objCls.AnalyseSearch();
        //objCls.AddServiceToWorkPlan();
                       
        objCls.SelectedServiceTypeSearching = 'PUDR';
        objCls.SelectedYear = '2013';
        objCls.SelectedFinancialRisk = 'Low';
        objCls.SelectedHealthRisk = 'High';
        objCls.SelectedProgramaticRisk = 'Medium';
        objCls.SelectedLFA = objAcc.Id;
        //objCls.AnalyseSearch();
        objCls.ClearFilterSearchGrid();
        //objCls.BackToWorkPlan();
        //objCls.getResorcesForPDF();
        
        objCls.WorkPlanChanged();
        test.stopTest();
        
         //objCls.AddServiceToWorkPlan();
        
        /*Performance_Evaluation__c perf1 = new Performance_Evaluation__c();
        perf.Q1_Rating__c='1';
        perf.LFA_Work_Plan__c = objWp1.Id;
        insert perf1;        
        
        Apexpages.currentpage().getparameters().put('id',perf1.id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(perf1);
        ctrlPerformanceEvaluation objCls1 = new ctrlPerformanceEvaluation(sc1);
        
        
        Account objAcc1 = new Account();
        objAcc1.Name = 'Test Acc1';
        insert objAcc1;
        
        Contact objCon1 = new Contact();
        objCon1.LastName = 'Test LName';
        objCon1.AccountId = objAcc1.Id;
        insert objCon1;
        
        
        
        LFA_Work_Plan__c objWp2 = new LFA_Work_Plan__c();
        objWp2.Name = 'Test Wp';
        objWp2.LFA__c = objAcc1.Id;
        objWp2.Country__c = objCountry1.id;
        insert objWp2;
        
        LFA_Service__c objService6 = new LFA_Service__c();
        objService6.Name = 'PUDR1';
        objService6.LFA_Work_Plan__c = objWp2.id;
        objService6.Service_Type__c = 'Key Services';
        objService6.Service_Sub_Type__c = 'PUDR1';
        insert objService6;
        
        LFA_Role__c objRole2 = new LFA_Role__c();
        objRole2.Name = 'Test Role3';
        insert objRole2;
        
        LFA_Resource__c objResource6 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource6.LFA_Service__c = objService6.id;
        objResource6.Rate__c = 1;
        objResource6.LFA_Role__c = objRole2.Id;
        objResource6.LOE__c = 1;
        objResource6.CT_Planned_Cost__c = 1;
        objResource6.CT_Planned_LOE__c = 1;
        objResource6.TGF_Proposed_Cost__c = 1;
        objResource6.TGF_Proposed_LOE__c = 1;
        insert objResource6;
        
        Rate__c objRate2 = new Rate__c();
        objRate2.LFA_Role__c = objRole2.Id;
        objRate2.Contact__c = objCon1.Id;
        objRate2.Country__c = objCountry1.Id;
        objRate2.Rate__c = 1;
        objRate2.Active__c = true;
        insert objRate2;
        
        Apexpages.currentpage().getparameters().put('id',objWp2.id);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(objWp2);
        ctrlAddEditWorkPlan objCls2 = new ctrlAddEditWorkPlan(sc2);
        
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        objCls2.EditService();
        Apexpages.currentpage().getparameters().put('intIndexST','0');
        Apexpages.currentpage().getparameters().put('intIndex','0');
        
        objCls2.lstwrapServicesType[0].lstwrapServices[0].lstwrapResource[0].objResource  = objResource6;
        objCls2.SaveResources();
        objCls.AddServiceToWorkPlan();
       
        objCls.DeleteResource();*/
        
    }
}