public with sharing class Help_Guidance_Grant_Indicator{
    public String strLanguage {get; set;}
    public Guidance__c guidance_impact {get; set;}
    public Guidance__c guidance_outcome {get; set;}
    public Guidance__c guidance_coverage {get; set;}
    
    public Help_Guidance_Grant_Indicator(){
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        Grant_Indicator();
    }
    
    public void Grant_Indicator(){
        guidance_impact = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'GM indicator related list'];
                    
        /*guidance_outcome = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'GM Objectives & Outcome Indicators'];
                    
        guidance_coverage = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'GM Coverage & Output Indicators'];*/
    }
}