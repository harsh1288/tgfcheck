public without sharing class DynamicChangeProfile{

public User currentUser {get;private set;}
public static final string PUBLIC_URL = 'http://gmsprint6-theglobalfund1.cs20.force.com/loopback' ;
public String selModule{get; set;}
public String strAccessLevel{get; set;}
//public Map<String, String> mapAccessProfile{get; set;}
public List<npe5__Affiliation__c> lstAccounts{get; set;}
public Map<String, npe5__Affiliation__c> mapAccountAccessLevel {get; set;}
public String userAccessLevel;
public Id accountId;
private Map<string,Affiliation_to_User_Profile_Mapping__c> mapAccess;

public DynamicChangeProfile(){

    selModule = '';
    userAccessLevel = '';
    
    mapAccess = Affiliation_to_User_Profile_Mapping__c.getAll();
    
    System.debug('mapAccess '+mapAccess);
    
    /*mapAccessProfile = new Map<String, String>();
    mapAccessProfile.put('Admin','Applicant/CM Admin');
    mapAccessProfile.put('Read/Write', 'Applicant/CM Read Write');
    mapAccessProfile.put('Read','Applicant/CM Read-Only');
    mapAccessProfile.put('PAdmin','PR Admin');
    mapAccessProfile.put('PR Read Write','PR Read Write Edit');
    mapAccessProfile.put('PR Read Only','PR Read Only');*/

    }

    
    public PageReference initialize(){
        System.debug('first name'+userInfo.getFirstName());
        
        String profileId = ApexPages.currentPage().getParameters().get('profileId');
        //return profileId == null?initializePage():initializeUpdate();
         return profileId == null?initializeView():initializeUpdate(); 
    }
    
    /*public PageReference initializePage(){
    
        lstAccounts = new List<npe5__Affiliation__c>();
        mapAccountAccessLevel = new Map<String, npe5__Affiliation__c>();
        
       currentUser = [Select Id, contactId, Name, firstname, profile.Id, profile.name, Profile.UserLicense.Id from User where Id=:userInfo.getUserId() limit 1];    
       return null;
    }*/

    public PageReference initializeView(){
        
        currentUser = [Select Id, contactId, Name, firstname, profile.Id, profile.name, Profile.UserLicense.Id from User where Id=:userInfo.getUserId() limit 1];    
        
        List<npe5__Affiliation__c> relatedAffiliation = new List<npe5__Affiliation__c>();
        lstAccounts = new List<npe5__Affiliation__c>();
        mapAccountAccessLevel = new Map<String, npe5__Affiliation__c>();   

        
        if(selModule == 'ConceptNote'){
            relatedAffiliation = [SELECT Id, npe5__Contact__c, npe5__Organization__c,npe5__Organization__r.Name, npe5__Organization__r.Id, Access_Level__c FROM npe5__Affiliation__c WHERE npe5__Contact__c=:currentUser.contactId AND npe5__status__c = 'Current' AND RecordTypeId =: label.CM_Affiliation_RT order by createddate desc];
        }
        else if (selModule == 'Grant-Making'){
            relatedAffiliation = [SELECT Id, npe5__Contact__c, npe5__Organization__c, npe5__Organization__r.Name, npe5__Organization__r.Id, Access_Level__c FROM npe5__Affiliation__c WHERE npe5__Contact__c=:currentUser.contactId AND npe5__status__c = 'Current'  AND RecordTypeId =: label.PR_Affiliation_RT order by createddate desc];    
        }
        System.debug('relatedAffiliation '+relatedAffiliation);
        
        for(npe5__Affiliation__c aff :relatedAffiliation){
            mapAccountAccessLevel.put(aff.npe5__Organization__r.Id, aff);
        }
        
        for(Id affs:mapAccountAccessLevel.keyset()){
            lstAccounts.add(mapAccountAccessLevel.get(affs));
        }
    
        return null;
    }
    
    public PageReference initializeUpdate()
    {
      PageReference ref = new PageReference('');
    
        try{
    
        String  UserId = ApexPages.currentPage().getParameters().get('userId');
        String profileId= ApexPages.currentPage().getParameters().get('profileId');
        
        User userToUpd = new User (Id = UserId , profileId =ProfileId );
        update userToUpd ;
       
        return null;    
        }
        catch(Exception e){
   
           System.debug(e+'Ids not found');
        }
    return null;
    }
    
    public PageReference selectProfile(){
        
        String affId = ApexPages.currentPage().getParameters().get('affId'); // Get selected Affiliation
        npe5__Affiliation__c objAff = [Select access_level__c, id, npe5__Organization__r.Id, RecordType.Name from npe5__Affiliation__c where id =:  affId AND npe5__Status__c = 'Current'];
        
        accountId = objAff.npe5__Organization__r.Id;
        String profName = '';
            
            // Get profile Name according to the Access Level defined in Affiliation
            
            String accessLevelRecordType = '';
            accessLevelRecordType = objAff.Access_Level__c+objAff.RecordType.Name;
            System.debug('accessLevelRecordType '+accessLevelRecordType);
            profName = mapAccess.get(accessLevelRecordType).Profile_ID__c;
            System.debug(' profName '+profName+ 'accessLevelRecordType '+accessLevelRecordType);
             
            //Profile objprof= [Select Id, Name From Profile where UserLicense.Id= :currentUser.Profile.UserLicense.Id AND Name = :profName];  
                        
            //String remoteUrl = PUBLIC_URL +'/apex/DynamicChangeProfile?userId=' + Userinfo.getUserId() + '&profileId=' + objprof.Id + '&accountId='+accountId + '&selModule='+selModule;
            String remoteUrl = PUBLIC_URL +'/apex/DynamicChangeProfile?userId=' + Userinfo.getUserId() + '&profileId=' + profName;
            
            if(!Test.isRunningtest() && currentUser.profile.Id!=profName){
            
                HttpRequest httpRequest = new HttpRequest();
                httpRequest.setMethod('GET');
                httpRequest.setEndPoint(remoteUrl);        
                HttpResponse httpResponse = new Http().send(httpRequest);
            }
            return redirect(selModule);
        }  
        
    public PageReference redirect(String module){
        
        PageReference ref = new PageReference('');
        
        if(module == 'ConceptNote'){
        ref = new PageReference('/apex/OpenConceptNotesH_Clone?accountId='+accountId);
        }
        else if(module == 'Grant-Making'){
        ref = new PageReference('/apex/GrantMakingHome_Clone?accountId='+accountId);
        }
        return ref;
    }  
}