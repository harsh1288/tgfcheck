/*
* $Author:      $ TCS - Vaishali Mankar
* $Description: $ Class for controlling new HPC Framework create event redirection
* $Date:        $ 21-Nov-2014 
* $Revision:    $ 
*/
public class saveAndNewPFController{

    private Performance_Framework__c PF;
    private Map<String, String> keyPrefixMap;
    public String backUrl{get;set;}
    public saveAndNewPFController(ApexPages.StandardController stdController){
        this.PF = (Performance_Framework__c)stdController.getRecord();
        backUrl = '';         
    }
    
     public PageReference createNewPFUrl()
     {
            boolean statusPF = saveandNewHandler.pfTrue('Performance_Framework__c');
            keyPrefixMap =  saveandNewHandler.schemaMap();
            String baseUrl;
            string userTypeStr = UserInfo.getUserType();
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == 'Standard') 
            {
                baseUrl = baseUrl+'/';
            }
            else
            { 
                baseUrl = baseUrl+'/GM/';
            }
           if(PF.Implementation_Period__c!= null)
           {
             // Creating url according to user type when Save and New is clicked while editing a record
                List<Implementation_Period__c> s = new List<Implementation_Period__c>([Select Name from Implementation_Period__c where Id  =:PF.Implementation_Period__c]);
                baseUrl += String.valueOf(keyPrefixMap.get('Implementation_Period__c'));
                baseUrl += '/e?nooverride=1';
                if(s.size()>0)
                {
                    baseUrl += '&';
                    baseUrl += Label.HPC_GIP_Lkup;
                    baseUrl += '=';
                    baseUrl += s[0].Name;            
                    baseUrl += '&retURL=';
                    baseUrl += PF.Implementation_Period__c;
                }
                PageReference saveNewUrl = new PageReference(baseUrl);
                saveNewUrl.setRedirect(true);                
                return saveNewUrl;            
           }
           else
           {
            // Creating url according to user type when Save and New is clicked while creating new record
               if(userTypeStr == 'Standard') 
                {                    
                }
                else
                { 
                    if(ApexPages.currentPage().getParameters().get('retUrl').indexOf('GM') > -1)
                    {
                    
                    }
                    else
                    {
                        backUrl += +'/GM';
                    }
                }
               backUrl += ApexPages.currentPage().getParameters().get('retUrl');
/*               if(backUrl.contains('?'))
               {
                   backUrl += '&nooverride=1';
               }
               else
               {
                   backUrl += '?nooverride=1';
               }
*/               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>'));                           
               return null;
           }         
     }
}