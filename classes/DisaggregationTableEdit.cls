public class DisaggregationTableEdit{

    public string targetRecord{get;set;}
    public string pfStatus{get;set;}
    public string dataType{get;set;}
    public string decimalPlaces{get;set;}
    public string pfid{get;set;}
    public string indtitle{get;set;}
    public string indtype;
    public DisaggregationTableEdit(ApexPages.StandardSetController controller)
    {
        targetRecord=ApexPages.currentPage().getParameters().get('par1');
        pfStatus=ApexPages.currentPage().getParameters().get('par2');
        dataType=ApexPages.currentPage().getParameters().get('par3');
        decimalPlaces=ApexPages.currentPage().getParameters().get('par4');
        pfid=ApexPages.currentPage().getParameters().get('par5');
        indtype = ApexPages.currentPage().getParameters().get('par6');
        indtitle= ApexPages.currentPage().getParameters().get('par7');
    }
    public PageReference ReturnToInd()
    {
        if(indtype == 'Coverage/Output' ){
               PageReference pageRef = new PageReference('/apex/IndicatorInlineCovOutput?id='+pfid);
               pageRef.setRedirect(true);
               return pageRef;
            }
        else{
        PageReference pageRef = new PageReference('/apex/IndicatorsInlineEditPage?id='+pfid);
        pageRef.setRedirect(true);
        return pageRef;
        }
    }
}