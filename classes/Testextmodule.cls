/*********************************************************************************
* Test class:   Testextmodule
  Class: extmodule 
*  DateCreated : 20/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To test extmodule class 
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           20/11/2014      INITIAL DEVELOPMENT
*********************************************************************************/ 

@isTest
public class Testextmodule {

   public static testMethod void Testextmodule() {
       
       Catalog_Module__c objCatMod = TestClassHelper.createCatalogModule(); 
       insert objCatMod;
       
       Catalog_Intervention__c objCatInt = TestClassHelper.createCatalogIntervention(); 
       objCatInt.Catalog_Module__c = objCatMod.Id;
       insert objCatInt;
       
       Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention(); 
       objCI.Name = 'Please specify';
       objCI.Catalog_Module__c = objCatMod.Id;
       insert objCI;
       
       test.startTest();
       Account objAccount = TestClassHelper.createAccount();
       insert objAccount;
       
       Grant__c objGrant = TestClassHelper.insertGrant(objAccount);
       
       Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant, objAccount);
       insert objIP;
       
       Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
       insert objPF;
       
       Module__c objModule =  TestClassHelper.createModule();
       objModule.Catalog_Module__c = objCatMod.Id;
       objModule.Implementation_Period__c = objIP.Id;
       objModule.Performance_Framework__c = objPF.Id;
       insert objModule;
       
       
       
       Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
       objGI.Module__c = objModule.Id;
       objGI.Catalog_Intervention__c = objCatInt.Id;
       objGI.Performance_Framework__c = objPF.Id;
       insert objGI;
       
      
        
       test.stopTest();

       //Add CN to Grant Interventions and Module
       Apexpages.currentpage().getparameters().put('id',objModule.Id);       
       ApexPages.StandardController sc = new ApexPages.StandardController(objModule);
       extmodule objextmodule = new extmodule(sc);
       objextmodule.intrvntnname = '';
       objextmodule.wlstintrvntn[0].selected = true;
       objextmodule.wlstintrvntn[0].catintrvntn.Is_Other_Intervrntion__c = true;
    
       objextmodule.save();
       
       objextmodule.intrvntnname = objGI.id;
       objextmodule.mcustintrnametogint.put('Please specify',objGI);
        objextmodule.save();
       objextmodule.otherontervention();
       
       
    }
    public static testMethod void TestextmoduleNoIP() {
       
       Catalog_Module__c objCatModNoIP = TestClassHelper.createCatalogModule(); 
       insert objCatModNoIP;
       
       Catalog_Intervention__c objCITest = TestClassHelper.createCatalogIntervention(); 
       objCITest.Name = 'Test Records';
       objCITest.Catalog_Module__c = objCatModNoIP.Id;
       insert objCITest;
        
       Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention(); 
       objCI.Name = 'Please specify';
       objCI.Catalog_Module__c = objCatModNoIP.Id;
       insert objCI;
       
      
       Account objAccount = TestClassHelper.createAccount();
       objAccount.Name = 'PR';
       insert objAccount;
       
       Grant__c objGrant = TestClassHelper.insertGrant(objAccount);
       
       Concept_Note__c objCN = TestClassHelper.createCN();
       insert objCN;
       
       test.startTest();
       
       Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAccount);
       objIP.Concept_Note__c = objCN.Id;
       insert objIP;
       
       Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
       insert objPF;
        
        Module__c objModNoIP =  TestClassHelper.createModule();
       objModNoIP.Catalog_Module__c = objCatModNoIP.Id;
       objModNoIP.Concept_Note__c = objCN.Id;
       objModNoIP.Performance_Framework__c = objPF.Id;
       insert objModNoIP;
    
       Grant_Intervention__c objGINoIP = TestClassHelper.createGrantIntervention(objIP);
       objGINoIP.Module__c = objModNoIP.Id;
       objGINoIP.Concept_Note__c = objCN.Id;
       objGINoIP.Catalog_Intervention__c = objCITest.Id;
       objGINoIP.Performance_Framework__c = objPF.Id;
       objGINoIP.PR1__c = 'Test';
       insert objGINoIP;
       
        test.stopTest();
        
        Apexpages.currentpage().getparameters().put('id',objModNoIP.Id);       
       ApexPages.StandardController scSelected = new ApexPages.StandardController(objModNoIP);
       extmodule objextmod = new extmodule(scSelected);
       objextmod.intrvntnname = objCI.id;
      objextmod.mcustintrnametogint.put('test',objGINoIP);
      objextmod.cnid = objCN.id;
       System.debug(objextmod.wlstintrvntn+'Retrieved Cats');
       objextmod.strnintrnvntnname = objCI.id;
       objextmod.selectpr = new List<String>();
       objextmod.selectpr.add(objAccount.Id);
       //objextmod.selectpr.add('PR');
       objextmod.save();
       
       
            
    }
}