public with sharing class ctrlPETSurveys {

public List<PET_REsponse__c> lstResp {get;set;}
public Performance_Evaluation__c perf;
public Performance_Evaluation__c perform {get;set;}
public ApexPages.StandardController stdController;

    public ctrlPETSurveys(ApexPages.StandardController controller) {
    
    perf = (Performance_Evaluation__c)Controller.getRecord();
    this.stdController = controller;
    lstResp = new List<PET_REsponse__c>();
    
  
    if(perf != null)
    for(PET_REsponse__c pr : [Select Id,Comments__c,Comments_to_LFA__c,Rating__c,Rating_to_LFA__c,Type__c from PET_REsponse__c  where Performance_Evaluation__c = :perf.Id])
     lstResp.add(pr);
    }

        
    public List<SelectOption> getRating()
    {
         
         List<SelectOption> optionList = new List<SelectOption>();
         optionList.add(new SelectOption('','-Select-'));
         
         for (Integer i =1; i <= 6;i++) //
         {
           if(i == 6)
            optionList.add (new SelectOption('0', 'N/A'));
           else 
            optionList.add (new SelectOption(String.ValueOf(i), String.ValueOf(i)));
           
         }
      
           return optionList;
            
     }
     
     public pageReference Save()
     {
      if(!lstResp.isEmpty())
       update lstResp;
       
       PageReference ret = stdController.save();
       
       return ret;
       
      
     }

}