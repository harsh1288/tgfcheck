public with sharing class Edit_Indicator_Controller {
    public Grant_Indicator__c objGI;
    public id recordId;
    public Id profileId;
    public String profileName;
    public boolean page_redirect;
    public boolean impact_ind;
    public String moduleUrl;
    public String getBackUrl {get; set;}

    public Edit_Indicator_Controller(ApexPages.StandardController controller) {
        getBackUrl = null;
        page_redirect = false;
        moduleUrl = null;
        impact_ind = false;
        recordId = ApexPages.currentPage().getParameters().get('id');
        profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug(profileName);
        if(profileName == 'PR Admin' || profileName == 'PR Read Write Edit'){
        	system.debug(recordId);
            objGI = [SELECT id, Name, Indicator_Type__c,Grant_Status__c FROM Grant_Indicator__c WHERE id =: recordId LIMIT 1];
            if(objGI.Indicator_Type__c == 'Impact' && (objGI.Grant_Status__c == 'Accepted' || objGI.Grant_Status__c == 'Submitted to MEPH Specialist'))
                impact_ind = true;
            else
                impact_ind = false;
            system.debug('objGI'+objGI);
        }
        else
            impact_ind = false;
            system.debug('impact_ind:'+impact_ind);
    }
    
    public PageReference reDirect() { 
        //apex/overrideindicatordetail?id=a02b000000DRAqv&sfdc.override=1
        PageReference editAction;
        String baseUrl;
        string userTypeStr = UserInfo.getUserType();
        System.debug('>>>user type'+userTypeStr);
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        if(userTypeStr == 'Standard') {
            baseUrl = baseUrl+'/';
        }
        else 
            baseUrl = baseUrl+'/GM/';
        if(impact_ind){
            system.debug('list size is greater than zero');
            getBackUrl = baseUrl+recordId;
            System.debug('Base URL: ' + getBackUrl);       
            //error = 'You are not allowed to edit the record';            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, 'You are not allowed to edit the record!');
            ApexPages.addMessage(errorMsg);
            page_redirect = false;
            //return null;
        }
        else{
            if(profileName == 'PR Admin' || profileName == 'PR Read Write Edit'){
                system.debug('I am in inner IF');
                //String moduleUrl = baseUrl+recordId+'/e?nooverride=1&retURL=%2F'+recordId+'&saveURL=%2F'+recordId;
                //https://theglobalfund--support--c.cs20.visual.force.com/apex/overideedit?id=a02m0000001NsVC&retURL=%2Fa1pm00000000ioS&sfdc.override=1 
                //String moduleUrl = baseUrl+'/apex/overideedit?id='+recordId+'&retURL=%2F'+recordId+'&sfdc.override=1';
                String moduleUrl = baseUrl+recordId+'/e?nooverride=1&retURL=%2F'+recordId+'&saveURL=%2F'+recordId;
                editAction = new PageReference(moduleUrl);
                editAction.setRedirect(true);
                page_redirect = true;    
            }
            else{
                system.debug('I am in inner else');
                //String moduleUrl = baseUrl+recordId+'/e?nooverride=1&retURL=%2F'+recordId+'&saveURL=%2F'+recordId;
                //https://theglobalfund--support--c.cs20.visual.force.com/apex/overideedit?id=a02m0000001NsVC&retURL=%2Fa1pm00000000ioS&sfdc.override=1 
                String moduleUrl = baseUrl+'/apex/overideedit?id='+recordId+'&retURL=%2F'+recordId+'&sfdc.override=1';
                //String moduleUrl = baseUrl+recordId+'/e?nooverride=1&retURL=%2F'+recordId+'&saveURL=%2F'+recordId;
                editAction = new PageReference(moduleUrl);
                editAction.setRedirect(true);
                page_redirect = true; 
            }  
        }
        if(page_redirect)
            return editAction;
        else 
            return null;
    }

}