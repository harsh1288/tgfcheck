@istest
public class TestgrantMultiCountriesExt{

public static testMethod void TestgrntMultiCountry(){
    
Account objAcc = TestClassHelper.insertAccount();
 
Country__c objCountry = TestClassHelper.insertCountry();
Country__c objCountry1 = TestClassHelper.insertCountry();
Country__c objCountry2 = TestClassHelper.insertCountry();
Country__c objCountry3 = TestClassHelper.insertCountry();
Country__c objCountry4 = TestClassHelper.insertCountry();
Country__c objCountry5 = TestClassHelper.insertCountry();
Country__c objCountry6 = TestClassHelper.insertCountry();
Country__c objCountry7 = TestClassHelper.insertCountry();
Country__c objCountry8 = TestClassHelper.insertCountry();
Country__c objCountry9 = TestClassHelper.insertCountry();
Country__c objCountry10 = TestClassHelper.insertCountry();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Grant_Multi_Country__c objGrntMC = TestClassHelper.createGrntMultiCountry(objCountry.Id,objIP.Id );
insert objGrntMC;

ApexPages.currentPage().getParameters().put('gid', objIP.Id);
ApexPages.StandardController sc = new ApexPages.StandardController(objGrntMC);
grantMultiCountriesExt objEC = new grantMultiCountriesExt(sc);

objEC.save();

objEC.country = 'Testcountry';
objEC.save();


objEC.cancel();
objEC.openlookupwindow();
objEC.closepopup();
//objEC.previousPage();
//objEC.nextPage();
}
public static testMethod void TestgrntMultiCountry2(){
    
Account objAcc = TestClassHelper.insertAccount();
 
Country__c objCountry = TestClassHelper.insertCountry();
Country__c objCountry1 = TestClassHelper.insertCountry();
Country__c objCountry2 = TestClassHelper.insertCountry();
Country__c objCountry3 = TestClassHelper.insertCountry();
Country__c objCountry4 = TestClassHelper.insertCountry();
Country__c objCountry5 = TestClassHelper.insertCountry();
Country__c objCountry6 = TestClassHelper.insertCountry();
Country__c objCountry7 = TestClassHelper.insertCountry();
Country__c objCountry8 = TestClassHelper.insertCountry();
Country__c objCountry9 = TestClassHelper.insertCountry();
Country__c objCountry10 = TestClassHelper.insertCountry();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Grant_Multi_Country__c objGrntMC = TestClassHelper.createGrntMultiCountry(objCountry.Id,objIP.Id );
insert objGrntMC;

ApexPages.currentPage().getParameters().put('gid', objIP.Id);
ApexPages.StandardController sc = new ApexPages.StandardController(objGrntMC);
grantMultiCountriesExt objEC = new grantMultiCountriesExt(sc);

objEC.save();
objEC.openlookupwindow();
objEC.nextPage();
objEC.previousPage();

objEC.country = 'Testcountry';
objEC.save();

objEC.country = '123456';
objEC.save();

objEC.cancel();
objEC.openlookupwindow();
objEC.closepopup();


}
}