@isTest
public class Reopen_test {    
    public static testMethod void reopenFramework(){
       Country__c objcountry =  TestClassHelper.createCountry();
       objcountry.CT_Public_Group_ID__c = '00Gb0000000tjuUEAQ';
       insert objcountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.Country__c = objcountry.Id;
       insert  objAcc;      
       
       Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
       insert objGrant;
       
       Implementation_Period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
       objimp.Approval_Status__c = 'Approved';
       insert objimp;
       
       Implementation_Period__c objimp1 = [select Approval_Status__c from  Implementation_Period__c where id=:objimp.Id ];
       objimp1.Approval_Status__c = 'Update information';
       update objimp1;
      
    }
    
 }