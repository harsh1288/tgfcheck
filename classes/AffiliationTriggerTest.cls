@isTest(seealldata=false)
public class AffiliationTriggerTest{
    
    public static User portalAccountOwner1;
    
    public static void createPortalUser(){
        //Create portal account owner
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
    }
    
    public static void setup(){
        CrmTestUtil.createAffiliationtoUserProfileMapping('AdminCM Affiliation', '00eb00000010MhQ', 'Admin', 'CM Affiliation', 1, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('AdminPR Affiliation', '00eb00000010Mh6', 'Admin', 'PR Affiliation',4, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('Read/WriteCM Affiliation', '00eb00000010MhV', 'Read/Write', 'CM Affiliation',2, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('Read/WritePR Affiliation', '00eb00000010MhG', 'Read/Write', 'PR Affiliation',5, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('ReadCM Affiliation', '00eb00000010Mha', 'Read', 'CM Affiliation',3, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('ReadPR Affiliation', '00eb00000010MhL', 'Read', 'PR Affiliation',6, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('NoAccessCM Affiliation', '', 'No Access', 'CM Affiliation',7, true);
        CrmTestUtil.createAffiliationtoUserProfileMapping('NoAccessPR Affiliation', '', 'No Access', 'PR Affiliation',8, true);
        CrmTestUtil.createAffiliationBasedSharingMatrix('AdminCM Affiliation', 'Admin', 'CM Affiliation','Edit', 'Read', 'Read', 'Edit', true);
        CrmTestUtil.createAffiliationBasedSharingMatrix('Read/WriteCM Affiliation', 'Read/Write', 'CM Affiliation','Edit', 'Read', 'Read', 'Edit', true);
        CrmTestUtil.createAffiliationBasedSharingMatrix('ReadCM Affiliation', 'Read', 'CM Affiliation','Read', 'Read', 'Read', 'Edit', true);
        CrmTestUtil.createAffiliationBasedSharingMatrix('AdminPR Affiliation', 'Admin', 'PR Affiliation','Read', 'Edit', 'Read', 'Edit', true);
        CrmTestUtil.createAffiliationBasedSharingMatrix('Read/WritePR Affiliation', 'Read/Write', 'PR Affiliation','Read', 'Edit', 'Read', 'Edit', true);
        CrmTestUtil.createAffiliationBasedSharingMatrix('ReadPR Affiliation', 'Read', 'PR Affiliation','Read', 'Read', 'Read', 'Edit', true);
        
    } 
    
    public static testMethod void AffiliationApexSharingTest1(){
        createPortalUser();
        system.runAs(portalAccountOwner1){
            setup();
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
            List<Account> testAccountsToBeInserted = new List<Account>();
            Account objAcc = CrmTestUtil.insertAccount('Test1',false);
            Account objAcc1 = CrmTestUtil.insertAccount('Test 2',false);
            Account a = new Account();
            a.Name = 'Partner Organisation';
            a.Short_Name__c = 'Name';
            testAccountsToBeInserted.add(objAcc);
            testAccountsToBeInserted.add(objAcc1);
            testAccountsToBeInserted.add(a);
            insert testAccountsToBeInserted;
            
            Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
            insert objGrant;
            List<Concept_Note__c> CNsToInsert = new List<Concept_Note__c>();
            List<Implementation_Period__c> IPsToInsert = new List<Implementation_Period__c>();
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = objAcc.id;
            CNsToInsert.add(objCN);
            
            Concept_Note__c objCN1 = TestClassHelper.createCN();
            objCN1.CCM_New__c = objAcc.id;
            CNsToInsert.add(objCN1);
            insert CNsToInsert;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc1);
            objIP.Concept_Note__c = objCN.Id;
            IPsToInsert.add(objIP);
            Implementation_Period__c objIP1 = TestClassHelper.createIP(objGrant,objAcc1);
            objIP.Concept_Note__c = objCN.Id;
            IPsToInsert.add(objIP1);
            insert IPsToInsert;
            Contact Con = new Contact();
            con.FirstName = 'Test Contact';
            con.External_User__c=true;
            con.LastName = 'Testing';
            Con.email = 'standarduser@testorg.com';
            con.accountid= objAcc.id;
            insert con;
            //List<npe5__Affiliation__c> objAfflist =[select id from npe5__Affiliation__c where npe5__Contact__c=:con.Id and npe5__Organization__c  =: objAcc1.Id and Access_Level__c = 'No Access'];
            npe5__Affiliation__c objAff = new npe5__Affiliation__c();
            objAff.Is_Inactive_c__c = False;
            objAff.Access_Level__c = 'No Access';
            objAff.Recordtypeid =label.CM_Affiliation_RT;
            objAff.npe5__Organization__c  = objAcc1.Id;
            objAff.npe5__Contact__c  = con.Id;
            AffiliationServices.run = 0;
            insert objAff;
            // npe5__Affiliation__c objAff = objAfflist[0];
            Test.StartTest();
            objAff.Access_Level__c = 'Read';
            objAff.Manage_Contacts__c= true;
            objAff.npe5__status__c= 'Current';
            AffiliationServices.run = 0;
            update objAff;
            objAff.npe5__status__c= 'Former';
            objAff.Access_Level__c = 'No Access';
            AffiliationServices.run = 0;
            update objAff;
            
            Test.stopTest();
        }
    }
    public static testMethod void AffiliationApexSharingTest2(){
        createPortalUser();
        system.runAs(portalAccountOwner1){
            setup();
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
            List<Account> testAccountsToBeInserted = new List<Account>();
            
            Account objAcc = CrmTestUtil.insertAccount('Test 3',false);
            Account objAcc1 = CrmTestUtil.insertAccount('Test 4',false);
            Account a = new Account();
            a.Name = 'Partner Organisation';
            a.Short_Name__c = 'Name'; 
            testAccountsToBeInserted.add(objAcc);
            testAccountsToBeInserted.add(objAcc1);
            testAccountsToBeInserted.add(a);
            
            insert testAccountsToBeInserted;
            
            Grant__c objGrant  = TestClassHelper.createGrant(objAcc1);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = objAcc1.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc1);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            
            Contact Con = new Contact();
            con.FirstName = 'Test Contact';
            con.External_User__c=true;
            con.LastName = 'Testing';
            Con.email = 'standarduser@testorg.com';
            con.accountid= objAcc.id;
            insert con;
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                              contactid=con.id,UserName='s@testorg.com');
            insert u;
            Test.StartTest();
            npe5__Affiliation__c objAff = new npe5__Affiliation__c();
            objAff.Is_Inactive_c__c = False;
            objAff.Access_Level__c = 'Admin';
            objAff.Recordtypeid =label.PR_Affiliation_RT;
            objAff.npe5__Organization__c  = objAcc1.Id;
            objAff.npe5__Contact__c  = con.Id;
            AffiliationServices.run = 0;
            insert objAff;
            
            Concept_Note__c objCN1 = TestClassHelper.createCN();
            objCN1.CCM_New__c = objAcc1.id;
            insert objCN1;
            Implementation_Period__c objIP1 = TestClassHelper.createIP(objGrant,objAcc1);
            objIP1.Concept_Note__c = objCN1.Id;
            insert objIP1;
            Test.stopTest();
        }
    }
    public static testMethod void insertPRAffiliationWithContactHavingUser(){
        setup();
        createPortalUser();
        system.runAs(portalAccountOwner1){
            Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
            Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
            affiliatedContact.recordTypeId = label.PR_Contact_RT ;
            insert affiliatedContact;
            Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
            
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
            Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = affiliatedAccount.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,affiliatedAccount);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                              contactid=affiliatedContact.id,UserName='s@testorg.com');
            insert u;
            Test.StartTest();
            AffiliationServices.run = 0;
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.PR_Affiliation_RT , 'Admin',true);
            Test.stopTest();
        }
    }
    public static testMethod void insertPRAffiliationWithLFAContactHavingUser(){
        setup();
        createPortalUser();
        
        system.runAs(portalAccountOwner1){
            Recordtype r = [Select id from recordtype where DeveloperName=: 'LFA' AND SobjectType =: 'Account'];
            Account parentAccount = CrmTestUtil.insertAccount('Test 1',false);
            parentAccount.recordTypeId =r.id;
            insert parentAccount;
            Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
            affiliatedContact.recordTypeId = label.LFA_Contact_RT ;
            insert affiliatedContact;
            Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
            
            
            Profile p = [SELECT Id FROM Profile WHERE Name='LFA Portal User'];             
            Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = affiliatedAccount.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,affiliatedAccount);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            
            user u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                              contactid=affiliatedContact.id,UserName='s@testorg.com');
            insert u;
             Test.StartTest();
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name =: 'LFA_Portal_User_Work_Plans'];
            PermissionSet ps1 = [SELECT Id FROM PermissionSet WHERE Name =: 'LFA_Portal_User_PET'];
            PermissionSet ps2 = [SELECT Id FROM PermissionSet WHERE Name =: 'Make_GM_app_visible_to_LFA_user_PermissionSet'];
            
            npe5__Affiliation__c LFAAffiliation = [Select id,PET_Access__c,LFA_Work_Plans__c from npe5__Affiliation__c where npe5__Contact__c=:affiliatedContact.id AND npe5__Organization__c=: parentAccount.id];
            LFAAffiliation.LFA_Work_Plans__c = true;
            update LFAAffiliation;
            
            PermissionSetAssignment pSet2 = new PermissionSetAssignment();
            pSet2.PermissionSetId = ps.id;
            pSet2.AssigneeId = u.id;
            insert pSet2;
            
             system.runAs(u){
                ctrlLFADefaultTab objcom = new ctrlLFADefaultTab();
                objcom.pageRedirect();
            }
           
            AffiliationServices.run = 0;
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.PR_Affiliation_RT , 'Read',true);
            
            PermissionSetAssignment pSet1 = new PermissionSetAssignment();
            pSet1.PermissionSetId = ps2.id;
            pSet1.AssigneeId = u.id;
            insert pSet1;
            
            system.runAs(u){
                ctrlLFADefaultTab objcom = new ctrlLFADefaultTab();
                objcom.pageRedirect();
            }
            
            
            LFAAffiliation.PET_Access__c = true;
            update LFAAffiliation;
            
            PermissionSetAssignment pSet = new PermissionSetAssignment();
            pSet.PermissionSetId = ps1.id;
            pSet.AssigneeId = u.id;
            insert pSet;
            
            system.runAs(u){
                ctrlLFADefaultTab objcom = new ctrlLFADefaultTab();
                objcom.pageRedirect();
            }
            
            delete pSet2;
            
            system.runAs(u){
                ctrlLFADefaultTab objcom = new ctrlLFADefaultTab();
                objcom.pageRedirect();
            }
            
            delete pSet;
            system.runAs(u){
                ctrlLFADefaultTab objcom = new ctrlLFADefaultTab();
                objcom.pageRedirect();
            }
            
            Test.stopTest();
        }

    }
    public static testMethod void insertCMAffiliationWithContactHavingUser(){
        setup();
        createPortalUser();
        system.runAs(portalAccountOwner1){
            Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
            Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
            affiliatedContact.recordTypeId = label.CM_Contact_RT ;
            insert affiliatedContact;
            Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
            Account aAccount = CrmTestUtil.insertAccount('Test 3',true);
            
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
            Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = affiliatedAccount.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,aAccount);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                              contactid=affiliatedContact.id,UserName='s@testorg.com');
            insert u;
            Test.StartTest();
            AffiliationServices.run = 0;
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.CM_Affiliation_RT , 'Admin',true);
            Test.stopTest();
        }
    }
    public static testMethod void insertCMAffiliationWithManageContactsTrue(){
        setup();
        createPortalUser();
        system.runAs(portalAccountOwner1){
            Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
            Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
            affiliatedContact.recordTypeId = label.CM_Contact_RT ;
            insert affiliatedContact;
            Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
            Account aAccount = CrmTestUtil.insertAccount('Test 3',true);
            
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
            Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = affiliatedAccount.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,aAccount);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                              contactid=affiliatedContact.id,UserName='s@testorg.com');
            insert u;
            Test.StartTest();
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.CM_Affiliation_RT , 'Admin',false);
            testAffiliation.Manage_Contacts__c = true;
            AffiliationServices.run = 0;
            insert testAffiliation;
            Test.stopTest();
        }
    }
    /*
    public static testMethod void createSharesExplicit(){        
        
        Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
        Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
        Account aAccount = CrmTestUtil.insertAccount('Test 3',true);
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
        Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = affiliatedAccount.id;
        insert objCN;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,aAccount);
        objIP.Concept_Note__c = objCN.Id;
        insert objIP;
        Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
            affiliatedContact.recordTypeId = label.CM_Contact_RT ;
            insert affiliatedContact;
            
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                          contactid=affiliatedContact.id,UserName='s@testorg.com');
        insert u;
        setup();
        createPortalUser();
        system.runAs(portalAccountOwner1){
            
            AffiliationServices.run = 0;
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.CM_Affiliation_RT , 'Admin',true);
            testAffiliation.Manage_Contacts__c = true;
            testAffiliation.Access_Level__c = 'Read';
            AffiliationServices.run = 0;
            update testAffiliation;
            Test.StartTest();
            AffiliationServices.run = 0;
            AffiliationServices.createSharesBasedOnAffiliation(new List<npe5__Affiliation__c>{testAffiliation}, new Map<Id, npe5__Affiliation__c>());
            Test.stopTest();
        }
    }
    */
    public static testMethod void insertAffiliationWithContactWithoutHavingUser(){
        setup();
        createPortalUser();
        system.runAs(portalAccountOwner1){
            Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
            Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
            affiliatedContact.recordTypeId = label.PR_Contact_RT ;
            insert affiliatedContact;
            Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
            
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
            Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = affiliatedAccount.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,affiliatedAccount);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            Test.StartTest();
            AffiliationServices.run = 0;
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.PR_Affiliation_RT , 'Admin',true);
            Test.stopTest();
        }
    }
    public static testMethod void updateAffiliationAccessLevel(){
        setup();
        createPortalUser();
        system.runAs(portalAccountOwner1){
            Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
            Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,true);
            Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
            
            
            Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
            Grant__c objGrant  = TestClassHelper.createGrant(affiliatedAccount);
            insert objGrant;
            
            Concept_Note__c objCN = TestClassHelper.createCN();
            objCN.CCM_New__c = affiliatedAccount.id;
            insert objCN;
            
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,affiliatedAccount);
            objIP.Concept_Note__c = objCN.Id;
            insert objIP;
            
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
                              contactid=affiliatedContact.id,UserName='s@testorg.com');
            insert u;
            AffiliationServices.run = 0;
            npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.PR_Affiliation_RT , 'Admin',true);
            testAffiliation.Access_Level__c = 'Read/Write';
            Test.StartTest();
            AffiliationServices.run = 0;
            update testAffiliation;
            Test.stopTest();
        }
    }
    public static testMethod void createUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin'];             
        Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
        Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,false);
        affiliatedContact.LastName = 'Test Contact';
        insert affiliatedContact;
        Test.StartTest();            
        
        AffiliationServices.createNewUserFromContact(affiliatedContact ,p.Id ,'Af-125646123456');
        Test.stopTest();
    }
}